// lex_analyzer.cpp : main project file.
/*
 * scanner.c
 *
 *  Created on: 29. 9. 2013
 *      Author: Marek
 */

#include "scanner.h"

/* Vestavene funkce
	param-> buffer-----retezec k porovnani se jmeny vestavenych funkci
*/
int CheckEmbedded(char *buffer, int count)
{
	if(count > MAX_LENGTH_EM)
		return LEX_FUN_ID;

	static char *embedded[] =
	{
		[0] = "boolval",
		[1] = "doubleval",
		[2] = "intval",
		[3] = "strval",
		[4] = "get_string",
		[5] = "put_string",
		[6] = "strlen",
		[7] = "get_substring",
		[8] = "find_string",
		[9] = "sort_string"
	};

	static int emlen[] =
	{
		[0] = 7,
		[1] = 9,
		[2] = 6,
		[3] = 6,
		[4] = 10,
		[5] = 10,
		[6] = 6,
		[7] = 13,
		[8] = 11,
		[9] = 11,
	};

	for(int i = 0; i < 10; i++)
	{
		if(strncmp(embedded[i], buffer, count) == 0 && emlen[i] == count) 
			return LEX_FUN_ID_EM;                             

	}

	return LEX_FUN_ID;
}

/* Klicova slova
	param-> buffer-----retezec k porovnani s klicovymi slovy
*/
int CheckKeyword(char *buffer, int count)
{
	if(count > MAX_LENGTH_KW)
		return -1;

	static char *keyword[] =
	{
		[0] = "if",
		[1] = "else",
		[2] = "function",
		[3] = "while",
		[4] = "return",
		[5] = "true",
		[6] = "false",
		[7] = "for",
		[8] = "elseif",
		[9] = "continue",
		[10] = "break",
		[11] = "null"
	};

	static int keylen[] =
	{
		[0] = 2,
		[1] = 4,
		[2] = 8,
		[3] = 5,
		[4] = 6,
		[5] = 4,
		[6] = 5,
		[7] = 3,
		[8] = 6,
		[9] = 8,
		[10] = 5,
		[11] = 4
	};
   
	for(int i=LEX_IF; i <= LEX_NULL; i++)
	{

		if(memcmp(buffer, keyword[i - LEX_IF], count) == 0 && keylen[i - LEX_IF] == count)
			return i;
	}
	return -1;
}

/* MemIncrease
	param-> memory----pamet k realokaci
	param-> memSize---nova zadouci velikost
*/
int MemIncrease(char **memory, int memSize)
{
	*memory = (char*) realloc(*memory, (memSize * 2 * sizeof(char)));

	if(*memory != NULL)
		return memSize*2;

	return -1;
}

int HexToDec(int high, int low)
{
	if(isdigit(high))
	{
		high = high - '0';
	}else{
		high = (high - 'a') + 10;
	}

	if(isdigit(low))
	{
		low = low - '0';
	}else{
		low = (low - 'a') + 10;
	}

	return high * 16 + low;
}

int GetNextToken(FILE * pFile, TToken *token, char **bufferPtr, int *lenght)
{	
//	TItem *item = NULL;
	int character = 0;
	int state = S_START;
	int result = -1;
	static int memSize = MAX_BUFFER;
	int count = 0;
	char *buffer = *bufferPtr;

	while(character != EOF)
	{
		character = getc(pFile);

		//pristi inkrementace count bude mimo buffer
		if(count >= memSize-1)
		{
			if((memSize = MemIncrease(bufferPtr, memSize)) == -1)
			//	return ERROR_INTERPRET;
      return 1;

			buffer = *bufferPtr;
		}

		*lenght = count;

		switch(state)
		{
			case S_START:
			{
				if(character == '\n' || !isspace(character))
				{
					if(isalpha(character) || (character == '_')){ /* Identifikator */
						buffer[count++] = character;
						state = S_KEYW_ID;
					} else if(isdigit(character)) { /* Integer/Double */
						buffer[count++] = character;
						state = S_INT;
					} else if(character == '"'){ /* String */
						state = S_STRING;
					} else if(character == '$'){ /* Promena */
          printf("here");
						buffer[count++] = character;
						state = S_VAR;
					} else if(character == '='){ /* Prirazeni/Rovnost */
						state = S_OP_EQ;
					} else if(character == '!'){ /* Nerovnost */
						state = S_OP_NE;
					} else if(character == '<'){ /* Mensi */
						state = S_OP_L;
					} else if(character == '>'){ /* Vetsi */
						state = S_OP_G;
					} else if(character == '+'){ /* Scitani */
						token->type=LEX_PLUS;
						return RESULT_OK;
					} else if(character == '-'){ /* Odcitani */
						token->type=LEX_MINUS;
						return RESULT_OK;
					} else if(character == '*'){ /* Hvezda(Nasobeni) */
						token->type = LEX_STAR;
						return RESULT_OK;
					} else if(character == '('){ /* Leva zavorka */
						token->type=LEX_LBRACKET;
						return RESULT_OK;
					} else if(character == ')'){ /* Prava zavorka */
						token->type=LEX_RBRACKET;
						return RESULT_OK;
					} else if(character == '{'){ /* Leva slozena zavorka */
						token->type=LEX_CLBRACKET;
						return RESULT_OK;
					} else if(character == '}'){ /* Prava slozena zavorka */
						token->type=LEX_CRBRACKET;
						return RESULT_OK;
					} else if(character == ','){ /* Carka */
						token->type=LEX_COMMA;
						return RESULT_OK;
					} else if(character == '/'){ /* Lomintko(Deleni/Komentar) */
						state = S_SLASH;
					} else if(character == ';'){ /* Strednik */
						token->type = LEX_SEMI;
						return RESULT_OK;
					} else if(character == '.'){ /* Tecka */
						token->type = LEX_DOT;
						return RESULT_OK;
					} else if(character == EOF){ /* EOF */
						token->type = LEX_EOF;
						return RESULT_OK;
					}
				}
			break;
			}

			case S_OP_L:{     /* <= = */
				switch(character){
					case '=':
						token->type=LEX_LESS_EQ;
						return RESULT_OK;
					break;
					case ' ':
						token->type=LEX_LESS;
						return RESULT_OK;
					break;
					case '?':
						state = S_PHP;
					break;
					default:
						ungetc (character, pFile);
						token->type=LEX_LESS;
						return RESULT_OK;
					break;
				}
			break;
			}
			case S_OP_G:{  /* >= = */
				switch(character){
					case '=':
						token->type=LEX_GREATER_EQ;
						return RESULT_OK;
					break;
					case ' ':
						token->type=LEX_GREATER;
						return RESULT_OK;
					break;
					default:
						ungetc (character, pFile);
						token->type=LEX_GREATER;
						return RESULT_OK;
					break;
				}
			break;
			}	
			case S_SLASH:{			/* Lomintko/Komentar */
				switch(character){
					case '*':
						state = S_COMMENT;
					break;
					case '/':
						state = S_SINGLE_COMMENT;
					break;
					case ' ':
						token->type=LEX_SLASH;
						return RESULT_OK;
					break;
					default:
						ungetc (character, pFile);
						token->type=LEX_SLASH;
						return RESULT_OK;
					break;
				}
			break;
			}
			case S_OP_EQ:{			/* === */
				switch(character){
					case '=':
						state = S_OP_EQ_2;
					break;
					case ' ':
						token->type=LEX_ASSIGN;
						return RESULT_OK;
					break;
					default:
						ungetc (character, pFile);
						token->type=LEX_ASSIGN;
						return RESULT_OK;
					break;
				}
			break;
			}
			case S_OP_EQ_2:{		/* === */
				switch(character){
					case '=':
						token->type = LEX_EQUAL;
						return RESULT_OK;
					break;
					default:
						return ERROR_LEX;
					break;
				}
			break;
			}
			case S_OP_NE:{		/* !== */
				switch(character){
					case '=':
						state = S_OP_NE_2;
					break;
					default:
						return ERROR_LEX;
					break;
				}
			break;
			}
			case S_OP_NE_2:{		/* !== */
				switch(character){
					case '=':
						token->type = LEX_NOTEQUAL;
						return RESULT_OK;
					break;
					default:
						return ERROR_LEX;
					break;
				}
			break;
			}
			case S_COMMENT:{		/* Komentar */
				if(character == '*'){
					character = getc(pFile);
					if(character == '/'){
						state = S_START;
					} else {
						ungetc (character, pFile);
					}
				}
			break;
			}
			case S_SINGLE_COMMENT:

				while((character = getc(pFile)) != '\n' && character != EOF)
					;

				if(character == EOF)
				{
					token->type = LEX_EOF;
					return RESULT_OK;
				}

				state = S_START;
				break;

			case S_KEYW_ID:
			{
				/* Identifikator */

				if((isalpha(character)) || (character == '_') || (isdigit(character)))
				{
					buffer[count++] = character;
				} else if(isspace(character)){
					result = CheckKeyword(buffer, count);

					if(result == -1)
					{
					//	int type = CheckEmbedded(buffer, count);
				//		item = HtableInsert(type, buffer, count, tableSymbols);

						token->type = LEX_FUN_ID;
					//	token->attr = item;
					} else if((result == LEX_TRUE) || (result == LEX_FALSE)) {
				//		TItem* item = HtableInsert(LEX_BOOL, buffer, count, tableSymbols);
						token->type = LEX_CON;
					//	token->attr = item;
						return RESULT_OK;
					} else if(result == LEX_NULL) {
			//			TItem* item = HtableInsert(LEX_NULL, buffer, count, tableSymbols);
						token->type = LEX_CON;
				//		token->attr = item;
						return RESULT_OK;
					} else {
						token->type = result;
					}
					return RESULT_OK;

				} else {

					ungetc (character, pFile);
					result = CheckKeyword(buffer, count);

					if (result == -1)
					{
						//int type = CheckEmbedded(buffer, count);
				//		item = HtableInsert(type, buffer, count, tableSymbols);
					
						token->type = LEX_FUN_ID;
				//		token->attr = item;
					} else if((result == LEX_TRUE) || (result == LEX_FALSE)) {
				//		TItem* item = HtableInsert(LEX_BOOL, buffer, count, tableSymbols);
						token->type = LEX_CON;
				//		token->attr = item;
						return RESULT_OK;
					} else if(result == LEX_NULL) {
				//		TItem* item = HtableInsert(LEX_NULL, buffer, count, tableSymbols);
						token->type = LEX_CON;
			//			token->attr = item;
						return RESULT_OK;
					} else {
						token->type = result;
					}
					return RESULT_OK;
				}
				break;
			}
			case S_INT:
			{
				/* Integer */
				if (isdigit(character))
				{
					buffer[count++] = character;
					/* Dalsi cislo */
				} else if(character == '.'){
					buffer[count++] = character;
					state = S_CHECK_D;
				} else if((character == 'E') || (character == 'e')){
					buffer[count++] = character;
					state = S_EXP_DOUBLE;
				} else if(character == ' '){
					buffer[count]='\0';
					//predani do TS
		//			TItem* item = HtableInsert(LEX_INT, buffer, count, tableSymbols);
					token->type = LEX_CON;
			//		token->attr = item;
					return RESULT_OK;
				} else { 
					ungetc (character, pFile);
					buffer[count]='\0';

				//	TItem* item = HtableInsert(LEX_INT, buffer, count, tableSymbols);
					token->type = LEX_CON;
			//		token->attr = item;
					return RESULT_OK;
				}
				break;
			}

			case S_CHECK_D:
			{
				/* Kontrola formatu double */
				if(isdigit(character)){
					buffer[count++] = character;
					state = S_DOUBLE;
				} else {
					return ERROR_LEX;
				}
				break;
			}

			case S_DOUBLE:
			{
				/* Double */
				if(isdigit(character))
				{
					buffer[count++] = character;
					/* Dalsi cislo */
				} else if(character == ' '){
					//predani do TS
			//		TItem* item = HtableInsert(LEX_DOUBLE, buffer, count, tableSymbols);
					token->type = LEX_CON;
			//		token->attr = item;
					return RESULT_OK;
				} else if((character == 'E') || (character == 'e')){
					buffer[count++] = character;
					state = S_EXP_DOUBLE;
				} else {
					ungetc (character, pFile);
					//predani do TS
			//		TItem* item = HtableInsert(LEX_DOUBLE, buffer, count, tableSymbols);
					token->type = LEX_CON;
			//		token->attr = item;
					return RESULT_OK;
				}
				break;
			}

			case S_EXP_DOUBLE:
			{
				/* Exponent */
				if(isdigit(character))
				{
					buffer[count++] = character;
					state = S_MANTIS;
				} else if((character == '+') || (character == '-')){
					buffer[count++] = character;
					state = S_SIGN;
				} else {
					return ERROR_LEX;
				}
				break;
			}

			case S_MANTIS:
			{
				/* Mantisa exponentu */
				if(isdigit(character))
				{
					buffer[count++] = character;
				} else if(character == ' '){
					//predani do TS
			//		TItem* item = HtableInsert(LEX_DOUBLE, buffer, count, tableSymbols);
					token->type = LEX_CON;
				//	token->attr = item;
					return RESULT_OK;
				} else {
					ungetc (character, pFile);
					//predani do TS
		//			TItem* item = HtableInsert(LEX_DOUBLE, buffer, count, tableSymbols);
					token->type = LEX_CON;
		//			token->attr = item;
					return RESULT_OK;
				}
				break;
			}

			case S_SIGN:
			{	/* Znamenko exponentu */
					if(isdigit(character)){
						buffer[count++] = character;
						state = S_MANTIS;
					} else {
						return ERROR_LEX;
					}
			break;
			}

			case S_STRING:
			{		/* Retezec */
					if(character == EOF){
						return ERROR_LEX;
					} else if(character == '\\'){
						buffer[count++] = character;
						state = S_ESCAPE;
					} else if(character == '"'){
						//predani do TS
			//			TItem* item = HtableInsert(LEX_STRING, buffer, count, tableSymbols);
						token->type = LEX_CON;
			//			token->attr = item;
						return RESULT_OK;
					} else {
						buffer[count++] = character;
						/* Dalsi znak */
					}
			break;
			}
			case S_ESCAPE:
			{
				/* Escape sekvence */
				if(character == EOF)
				{
					return ERROR_LEX;
				}
				switch (character)
				{
					case ('t'):
						buffer[count - 1] = '\t';
						break;
					case ('n'):
						buffer[count - 1] = '\n';
						break;
					case ('$'):
						buffer[count - 1] = '$';
						break;
					case ('"'):
						buffer[count - 1] = '"';
						break;
					case ('\\'):
						break;
					case ('x'):
					{
						int charA = 0;
						int charB = 0;

						if((charA = getc(pFile)) != EOF && (charB = getc(pFile)) != EOF)
						{
							if(isxdigit(charA) && isxdigit(charB))
							{
								buffer[count - 1] = HexToDec(tolower(charA), tolower(charB));
							}else{
								buffer[count++] = 'x';

								if(count == memSize-1)
								{
									if((memSize = MemIncrease(bufferPtr, memSize)) == -1)
										return ERROR_INTERPRET;

									buffer = *bufferPtr;
								}

								buffer[count++] = charA;
								ungetc(charB, pFile);
								break;
							}
						}else{
							token->type = LEX_EOF;
							return RESULT_OK;
						}

						break;
					}
					default:
						buffer[count++] = character;
				}

				state = S_STRING;
				break;
			}
			case S_VAR:
			{
				/* Promenna */
				if((isalpha(character)) || (character == '_'))
				{
					buffer[count++] = character;
					state = S_VAR_2;
				} else {
					return ERROR_LEX;
				}
				break;
			}
			case S_VAR_2:
			{
				/* Promenna */
				if((isalpha(character)) || (character == '_') || (isdigit(character))){
					buffer[count++] = character;
					/* Dalsi znak */
				} else if(character == ' '){
					result = CheckKeyword(buffer, count);
					if (result == -1)
					{
				//		item = HtableFindItem(LEX_ID, buffer, count, tableSymbols);
						token->type = LEX_ID;
			//			token->attr = item;
					} else if((result == LEX_TRUE) || (result == LEX_FALSE)) {
				//		TItem* item = HtableInsert(LEX_BOOL, buffer, count, tableSymbols);
						token->type = LEX_CON;
				//		token->attr = item;
						return RESULT_OK;
					} else if(result == LEX_NULL) {
				//		TItem* item = HtableInsert(LEX_NULL, buffer, count, tableSymbols);
						token->type = LEX_CON;
				//		token->attr = item;
						return RESULT_OK;
					} else {
						token->type = result;
					}
					return RESULT_OK;
				} else {
					ungetc (character, pFile);
					result = CheckKeyword(buffer, count);
					if (result == -1)
					{
				//		item = HtableFindItem(LEX_ID, buffer, count, tableSymbols);
						token->type = LEX_ID;
				//		token->attr = item;
					} else if((result == LEX_TRUE) || (result == LEX_FALSE)) {
				//		TItem* item = HtableInsert(LEX_BOOL, buffer, count, tableSymbols);
						token->type = LEX_CON;
				//		token->attr = item;
						return RESULT_OK;
					} else if(result == LEX_NULL) {
				//		TItem* item = HtableInsert(LEX_NULL, buffer, count, tableSymbols);
						token->type = LEX_CON;
				//		token->attr = item;
						return RESULT_OK;
					} else {
						token->type = result;
					}
					return RESULT_OK;
				}
				break;
			}
			case S_PHP:
			{
					if(character == 'p')
					{
						state = S_PHP_2;
						break;
					} else {
						return ERROR_LEX;
					}
			}
			case S_PHP_2:
			{
					if(character == 'h')
					{
						state = S_PHP_3;
						break;
					} else {
						return ERROR_LEX;
					}
			}
			case S_PHP_3:
			{
					if(character == 'p')
					{
						state = S_PHP_4;
						break;
					} else {
						return ERROR_LEX;
					}
			}
			case S_PHP_4:
			{
					if(isspace(character))
					{
						token->type = LEX_START_MARK;
						return RESULT_OK;
					} else {
						return ERROR_LEX;
					}
			}
		}
	}
  fclose(pFile);
    return ERROR_LEX;
}
