/*
 * scanner.h
 *
 *  Created on: 15. 10. 2013
 *      Author: Michal
 */

#include <stdbool.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "error.h"
//#include "ial.h"

#ifndef SCANNER_H_
#define SCANNER_H_

#define MAX_BUFFER 2    // definujeme maximalni velikost slova
#define MAX_LENGTH_EM 13 //nejdelsi delka identifikatoru vestavene funkce
#define MAX_LENGTH_KW 8 //nejdelsi delka klicoveho slova

//TODO: POZOR hodnoty konstant operatoru nemenit!

//ZACATEK NEMENIT HODNOTY!
#define	LEX_PLUS		0	// +
#define LEX_MINUS		1	// -
#define LEX_STAR		2	// *
#define LEX_SLASH		3	// /
#define LEX_DOT			4	// .
#define LEX_LESS		5	// <
#define LEX_GREATER		6	// >
#define LEX_LESS_EQ		7	// <=
#define LEX_GREATER_EQ	8	// >=
#define LEX_EQUAL		9	// ===
#define LEX_NOTEQUAL	10	// !==
#define LEX_LBRACKET	11	// (
#define LEX_RBRACKET	12	// )

#define SA_END_EXP		13 	// Pomocny ukoncujici znak pro SA
#define LEX_CON			14  // Konstanta napr. int, double...

#define LEX_ID			15
//KONEC NEMENIT HODNOTY!

#define INTR_VAR_INT	10
#define INTR_VAR_DOUBLE	11
#define INTR_VAR_STRING	12
#define INTR_VAR_BOOL	13
#define INTR_VAR_NULL	14

#define LEX_FUN_ID		21
#define LEX_FUN_ID_EM	22

#define LEX_ASSIGN		30	// =
#define LEX_CLBRACKET	31	// {
#define LEX_CRBRACKET	32	// }
#define LEX_COMMA		33	// ,
#define LEX_SEMI		34	// ;

#define LEX_INT			35
#define LEX_DOUBLE		36
#define LEX_STRING		37
#define LEX_BOOL		38

#define LEX_IF			53
#define LEX_ELSE		54
#define LEX_FUNC		55
#define LEX_WHILE		56
#define LEX_RETURN		57

#define LEX_TRUE		58
#define LEX_FALSE		59
#define LEX_FOR			60
#define LEX_ELSIF		61
#define LEX_CONTINUE	62
#define LEX_BREAK		63
#define LEX_NULL		64		// NEPOSOUVAT - MASKOVANI
#define LEX_START_MARK	65
#define LEX_EOF			66

//Specialne pro syntakticky analyzator
#define SA_EPSILON		67

//TODO: POZOR! hodnota nesmi prekrocit 90 a vyse jsou definovany neterminaly

#define S_START 			0           /**< Pocatecni stav */
#define S_KEYW_ID			1			/**< Klicove slovo */
#define S_KONST_ID			2      		/**< Konstanta */
#define S_VAR				3    		/**< PromenNa */
#define	S_VAR_2				4
#define	S_OP_L				5			/**< Operator Less */
#define	S_OP_G				6			/**< Operator Greater */
#define	S_OP_EQ				7			/**< Operator rovna se */
#define	S_OP_EQ_2			8
#define	S_OP_NE				9			/**< operator nerovna se */
#define	S_OP_NE_2			10
#define	S_INT				11			/**< Integer */
#define	S_CHECK_D			12			/**< Kontrola doublu */
#define	S_EXP_DOUBLE		13			/**< Double s exponentem */
#define	S_MANTIS			14			/**< Mantisa exponentu */
#define	S_SIGN				15			/**< Znamenko exponentu */
#define	S_DOUBLE			16			/**< Desetinne cislo */
#define	S_STRING			17			/**< Retezec */
#define	S_ESCAPE			18			/**< Escape sekvence */
#define	S_STAR				19			/**< Operator nasobeni */
#define	S_SLASH				20			/**< Operator deleni/komentar */
#define	S_SINGLE_COMMENT	21			/**< Radkovy komentar */
#define	S_COMMENT			22			/**< Blokovy komentar */
#define S_PHP			    23
#define S_PHP_2				24
#define S_PHP_3				25
#define S_PHP_4				26

typedef struct token
{
	int type;
//	TItem *attr;
}TToken;

int GetNextToken(FILE * pFile, TToken *token, char **bufferPtr, int *lenght);

int CheckEmbedded(char *buffer, int count);
int CheckKeyword(char *buffer, int count);
int MemIncrease(char **memory, int memSize);
int HexToDec(int high, int low);


#endif /* SCANNER_H_ */
