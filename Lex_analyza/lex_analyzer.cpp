// lex_analyzer.cpp : main project file.
/*
 * scanner.c
 *
 *  Created on: 29. 9. 2013
 *      Author: Marek
 */

#include "stdafx.h"
#include <stdio.h>
#include <ctype.h>
#include "scanner.h"
#include "error.h"
using namespace System;

#define MAX 255          // definujeme maximalni velikost slova
TToken token;
FILE * pFile;


int main()
{	
	int character;
	int state = S_START;
	int count = -1;
	char buffer[MAX + 2] = {0};

	pFile = fopen("TextFile1.txt", "r");

	while(true){
		character = getc(pFile);
		switch(state){ 
			printf("HERE"); 
			case S_START:{
				if(character == '/n' || !isspace(character)){
					if(isalpha(character) || (character == '_')){ /* Identifikator */
						count++;
						buffer[count] = character;
						state = S_KEYW_ID;
					} else if(isdigit(character)) { /* Integer/Double */
						count++;
						buffer[count] = character;
						state = S_INT;
					} else if(character == '"'){ /* String */
						state = S_STRING;
					} else if(character == '$'){ /* Promena */
						count++;
						buffer[count] = character;
						state = S_VAR;
					} else if(character == '='){ /* Prirazeni/Rovnost */
						state = S_OP_EQ;
					} else if(character == '!'){ /* Nerovnost */
						state = S_OP_NE;
					} else if(character == '<'){ /* Mensi */
						state = S_OP_L;
					} else if(character == '>'){ /* Vetsi */
						state = S_OP_G;
					} else if(character == '+'){ /* Scitani */
						token.type=LEX_PLUS;
						return RESULT_OK;
					} else if(character == '-'){ /* Odcitani */
						token.type=LEX_MINUS;
						return RESULT_OK;
					} else if(character == '*'){ /* Hvezda(Nasobeni) */
						token.type = LEX_STAR;
						return RESULT_OK;
					} else if(character == '('){ /* Leva zavorka */
						token.type=LEX_LBRACKET;
						return RESULT_OK;
					} else if(character == ')'){ /* Prava zavorka */
						token.type=LEX_RBRACKET;
						return RESULT_OK;
					} else if(character == '{'){ /* Leva slozena zavorka */
						token.type=LEX_CLBRACKET;
						return RESULT_OK;
					} else if(character == '}'){ /* Prava slozena zavorka */
						token.type=LEX_CRBRACKET;
						return RESULT_OK;
					} else if(character == ','){ /* Carka */
						token.type=LEX_COMMA;
						return RESULT_OK;
					} else if(character == '/'){ /* Lomintko(Deleni/Komentar) */
						state = S_SLASH;
					} else if(character == ';'){ /* Strednik */
						token.type = LEX_SEMI;
					} else if(character == '.'){ /* Tecka */
						token.type = LEX_DOT;
					}
				}
			break;
			}

			case S_OP_L:{     /* <= = */
				switch(character){					
					case '=':
						token.type=LEX_LESS_EQ;
						return RESULT_OK;
					break;
					case ' ':
						token.type=LEX_LESS;
						return RESULT_OK;
					break;
					case '?':
						state = S_PHP;
					break;
					default:
						return ERROR_LEX;
					break;
				}
			break;
			}
			case S_OP_G:{  /* >= = */
				switch(character){
					case '=':
						token.type=LEX_GREATER_EQ;
						return RESULT_OK;
					break;
					case ' ':
						token.type=LEX_GREATER;
						return RESULT_OK;
					break;
				}
			break;
			}	
			case S_SLASH:{			/* Lomintko/Komentar */
				switch(character){
					case '*':
						state = S_COMMENT;
					break;
					case '/':
						state = S_SINGLE_COMMENT;
					break;
					case ' ':
						token.type=LEX_SLASH;
						return RESULT_OK;
					break;
				}
			break;
			}
			case S_OP_EQ:{			/* === */
				switch(character){
					case '=':
						state = S_OP_EQ_2;
					break;
					case ' ':
						token.type=LEX_ASSIGN;
						return RESULT_OK;
					break;
				}
			break;
			}
			case S_OP_EQ_2:{		/* === */
				switch(character){
					case '=':
						token.type = LEX_EQUAL;
						return RESULT_OK;
					break;
					default:
						return ERROR_LEX;
					break;
				}
			break;
			}
			case S_OP_NE:{		/* !== */
				switch(character){
					case '=':
						state = S_OP_NE_2;
					break;
					case ' ':
						return ERROR_LEX;
					break;
				}
			break;
			}
			case S_OP_NE_2:{		/* !== */
				switch(character){
					case '=':
						token.type = LEX_NOTEQUAL;
						return RESULT_OK;
					break;
					default:
						return ERROR_LEX;
					break;
				}
			break;
			}
			case S_COMMENT:{		/* Komentar */
				character = getc(pFile);
				if(character == '*'){
					character = getc(pFile);
					if(character == '/'){
						state = S_START;
					} else {
						ungetc (character, pFile);
					}
				}
			break;
			}
			case S_KEYW_ID:{		/* Identifikator */
					
					if((isalpha(character)) || (character == '_') || (isdigit(character))){
						count++;		/* Dalsi znak */
						buffer[count] = character;
					} else if(isspace(character)){
						token.type = LEX_FUN_ID;
						return RESULT_OK;
					} else {
						ungetc (character, pFile);
						token.type = LEX_FUN_ID;
						return RESULT_OK;
					}
			break;
			}
			case S_INT:{		/* Integer */
					if (isdigit(character)){
						count++;
						buffer[count] = character;
						/* Dalsi cislo */
					} else if(character == '.'){
						count++;
						buffer[count] = character;
						state = S_CHECK_D;
					} else if(character == ' '){
						token.type = LEX_INT;
						return RESULT_OK;
					} else {
						ungetc (character, pFile);
						token.type = LEX_INT;
						return RESULT_OK;
					}
			break;
			}
			case S_CHECK_D:{	/* Kontrola formatu double */
					if(isdigit(character)){
						count++;
						buffer[count] = character;
						state = LEX_DOUBLE;
					} else {
						return ERROR_LEX;
					}
			break;
			}
			case S_DOUBLE:{			/* Double */						
					if(isdigit(character)){
						count++;
						buffer[count] = character;
						/* Dalsi cislo */
					} else if(character == ' '){
						token.type = LEX_DOUBLE;
						return RESULT_OK;
					} else if((character == 'E') || (character == 'e')){
						count++;
						buffer[count] = character;
						state = S_EXP_DOUBLE;
					} else {
						ungetc (character, pFile);
						token.type = LEX_DOUBLE;
						return RESULT_OK;
					}
			break;
			}
			case S_EXP_DOUBLE:{		/* Exponent */
					if(isdigit(character)){
						count++;		/* Dalsi cislo */
						buffer[count] = character;
						state = S_MANTIS;
					} else if((character == '+') || (character == '-')){
						count++;		
						buffer[count] = character;
						state = S_SIGN;
					} else {
						return ERROR_LEX;
					}
			break;
			}
			case S_MANTIS:{  /* Mantisa exponentu */
					if(isdigit(character)){
						count++;		
						buffer[count] = character;
						/* Dalsi cislo */
					} else if(character == ' '){
						token.type = LEX_DOUBLE;
						return RESULT_OK;
					} else {
						ungetc (character, pFile);
						token.type = LEX_DOUBLE;
						return RESULT_OK;
					}
			break;
			}
			case S_SIGN:{	/* Znamenko exponentu */
					if(isdigit(character)){
						count++;		
						buffer[count] = character;
						state = S_MANTIS;
					} else {
						return ERROR_LEX;
					}
			break;
			}
			case S_STRING:{		/* Retezec */
					if(character == 'EOF'){
						return ERROR_LEX;
					} else if(character == '\\'){
						state = S_ESCAPE;
					} else if(character == '"'){
						token.type = LEX_STRING;
						return RESULT_OK;
					} else {
						count++;		
						buffer[count] = character;
						/* Dalsi znak */
					}
			break;
			}
			case S_ESCAPE:{			/* Escape sekvence */
					if(character == 'EOF'){
						return ERROR_LEX;
					} 
					state = S_STRING;
			break;
			}
			case S_VAR:{		/* Promenna */
					if((isalpha(character)) || (character = '_')){
						count++;		
						buffer[count] = character;
						state = S_VAR_2;
					} else {
						return ERROR_LEX;
					}
			break;
			}
			case S_VAR_2:{		/* Promenna */
					if((isalpha(character)) || (character = '_') || (isdigit(character))){
						count++;		
						buffer[count] = character;
						/* Dalsi znak */
					} else if(character == ' '){
						token.type = LEX_ID;
						return RESULT_OK;
					} else {
						ungetc (character, pFile);
						token.type = LEX_ID;
						return RESULT_OK;
					}
			break;
			}
			case S_PHP:{
					if(character == 'p')
					{
						state = S_PHP_2;
					} else {
						return ERROR_LEX;
					}
			}
			case S_PHP_2:{
					if(character == 'h')
					{
						state = S_PHP_3;
					} else {
						return ERROR_LEX;
					}
			}
			case S_PHP_3:{
					if(character == 'p')
					{
						state = S_PHP_4;
					} else {
						return ERROR_LEX;
					}
			}
			case S_PHP_4:{
					if(character == ' ')
					{
						token.type = LEX_PHP;
						return RESULT_OK;	
					} else {
						return ERROR_LEX;
					}
			}
		}	
	} 
	fclose(pFile);
    return 0;
}
