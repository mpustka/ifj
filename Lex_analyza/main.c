#include "scanner.h"
#include "error.h"



static char *tokens[] =
	{
		[0] = "LEX_PLUS",
		[1] = "LEX_MINUS",
		[2] = "LEX_STAR",
		[3] = "LEX_SLASH",
		[4] = "LEX_DOT",
		[5] = "LEX_LESS",
		[6] = "LEX_GREATER",
		[7] = "LEX_LESS_EQ",
    [8] = "LEX_GREATER_EQ",
    [9] = "LEX_EQUAL",
    [10] = "LEX_NOTEQUAL",
    [11] = "LEX_LBRACKET",
    [12] = "LEX_RBRACKET",
    [14] = "LEX_CON",  // constant
    [15] = "LEX_ID",   //ID
    [21] = "LEX_FUN_ID", //FUN ID
    [22] = "LEX_FUN_ID_EM", // FUNID_EM
    [30] = "LEX_ASSIGN",
    [31] = "LEX_CLBRACKET",
    [32] = "LEX_CRBRACKET",
    [33] = "LEX_COMMA",
    [34] = "LEX_SEMI",
    [35] = "LEX_INT",
    [36] = "LEX_DOUBLE",
    [37] = "LEX_STRING",  //String
    [38] = "LEX_BOOL",
    [53] = "LEX_IF",
    [54] = "LEX_ELSE",
    [55] = "LEX_FUNC",
    [56] = "LEX_WHILE",
    [57] = "LEX_RETURN",
    [58] = "LEX_TRUE",
    [59] = "LEX_FALSE",
    [60] = "LEX_FOR",
    [61] = "LEX_ELSIF",
    [62] = "LEX_CONTINUE",
    [63] = "LEX_BREAK",
    [64] = "LEX_NULL",
    [65] = "LEX_START_MARK",
    [66] = "LEX_EOF",
    
    
	};

int main(int argc, char *argv[])
{
    FILE *file = NULL;
    TToken token;
    char *buffer = NULL; //buffer pro lexikalni analyzator
    int length = 0; //velikost slova v bufferu LA
    int x = 0;
    argc =argc;
    if((buffer = malloc(sizeof(char)*MAX_BUFFER)) == NULL)
		return 1;
    
    if ((file = fopen(argv[1], "r")) != NULL){
        while((x != 1) && (token.type != 66)){
        x = GetNextToken(file, &token, &buffer, &length);
            if(x == 0){
                printf("%s", tokens[token.type]);
               
                if((token.type == 14) || (token.type == 15) || (token.type == 21) || (token.type == 22) || (token.type == 37)){
                    printf("----%s", buffer);
                }
                printf("\n");    
            } else {
                printf("Lex_chyba\n");
            }   
        }       
     }
}