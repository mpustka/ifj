/*
 * error.h
 *
 *  Created on: 15. 10. 2013
 *      Author: Harry
 */

#define RESULT_OK		0

#define ERROR_LEX		1
#define ERROR_SA		2
#define ERROR_SEM_NFUN	3
#define ERROR_SEM_MISS	4
#define ERROR_SEM_NVAR	5
#define ERROR_SEM_ZERO	10
#define ERROR_SEM_CONV	11
#define ERROR_SEM_ARRE	12
#define ERROR_SEM_RUNT	13
#define ERROR_INTERPRET 99


