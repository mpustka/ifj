/*
 * Soubor:  htable_printrecord.c
 * Datum:   20.04.2013 21:26
 * Autor:   Martin Roncka, xronck00 - FIT
 * Projekt: IJC-DU2
 */

#include "htable.h"
#include <stdio.h>

#include <string.h>

void htable_printrecord(const char *key, int value)
{
  printf("%s\t%d\n", key, value);
}

/* konec souboru */
