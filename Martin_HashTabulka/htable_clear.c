/*
 * Soubor:  htable_clear.c
 * Datum:   20.04.2013 21:26
 * Autor:   Martin Roncka, xronck00 - FIT
 * Projekt: IJC-DU2
 */

#include "htable.h"
#include <stdlib.h>

/* 
 * Funkce uvolni vsechny seznamy v hashovaci tabulce a odkazy nastavi na NULL
 */
void htable_clear(Thtable *hash)
{
  Trecord *tmp, *prev;

  for (int i = 0; i < hash->size; i++)
  {
    tmp = hash->array[i];

    while (tmp != NULL)
    {
      prev = tmp;
    
      tmp = tmp->next;

      free(prev->key);

      free(prev);
    }

    hash->array[i] = NULL;
  }
}

/* konec souboru */
