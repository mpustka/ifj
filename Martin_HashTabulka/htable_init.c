/*
 * Soubor:  htable_init.c
 * Datum:   20.04.2013 21:26
 * Autor:   Martin Roncka, xronck00 - FIT
 * Projekt: IJC-DU2
 */

#include "htable.h"
#include <stdlib.h>

Thtable * htable_init(unsigned int size)
{
  Thtable *tmp;

  unsigned int toalloc = sizeof(Thtable) + size * sizeof(Tlist);
  if ((tmp = malloc(toalloc)) == NULL)
    return NULL;

  tmp->size = size;

  for (int i = 0; i < size; i++)
    tmp->array[i] = NULL;

  return tmp;
}

/* konec souboru */
