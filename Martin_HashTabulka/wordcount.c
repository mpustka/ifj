/*
 * Soubor:  wordcount.c
 * Datum:   18.04.2013 12:24
 * Autor:   Martin Roncka, xronck00 - FIT
 * Projekt: IJC-DU2
 * Popis:   Program uklada slova privedena na stdin do zaznamu v pameti pomoci
            hashovaci tabulky a umoznuje tak jejich snadne vyhledavani a vypis.
 */


//////////////////////////////////////////////
//                                          //
//  Pripojeni hlavickovych souboru a makra  ////////////////////////////////////
//                                          //
//////////////////////////////////////////////

/* pripojeni hlavickovych souboru */
#include <stdio.h>       // vstup/vystupni funkce, konstant..
#include <string.h>      // pro praci s retezci
#include <stdlib.h>      // pro funkce prace s pameti
#include "htable.h"      // rozhrani funkci pro praci s hash tabulkou
#include "io.h"          // pro funkci fgetword

/* definice maker a konstant */
#define MAX 255          // definujeme maximalni velikost slova

/*
 * Vhodnym zvolenim velikosti hashovaci tabulky ovlivnujeme delku seznamu ktere
   budou vznikat pri pridavani zaznamu. Jelikoz nechceme aby seznamy byly prilis
   dlouhe - protoze jejich prochazeni spotrebovava velkou miru strojoveho casu
   snazime se HSIZE drzet na rozumne mire. Tezko lze ale odhadnout s jakym
   poctem slov se bude program vyporadavat a proto mi prijde rozumne nastavit
   velikost tabulky na vysokou hodnotu okolo 10-100 tisic pri ktere sice bude
   tabulka mit velky rozsah a bude zabirat vetsi mnozstvi pameti, na druhou 
   stranu se vsak velmi urychli vykon programu pri praci s eelkymi soubory o
   velkem poctu slov pro ktere ma hashovaci funkce stejnou indexovaci hodnotu
   jako napriklad v /usr/share/dict/words.
 */
#define HSIZE 10000


//////////////////////////////
//                          //
//  Hlavni funkce programu  ////////////////////////////////////////////////////
//                          //
//////////////////////////////

int main(int argc, char *argv[])
{
  // kontrola argumentu pro pripadny vypis napovedy
  if (argc == 2 && strcmp(argv[1], "-help") == 0)
  {
    printf("Program ocekava na vstupu text ktery je po jednotlivych slovech "
           "ukladan do pameti. Program vypise cetnost danych slov na stdout\n");
    return EXIT_SUCCESS;
  }

  Thtable *hash;          // ukazatel na hash tabulku se kterou budeme pracovat

  // volame funkci pro inicializaci hash tabulky a kontrolujeme uspesnost
  if ((hash = htable_init(HSIZE)) == NULL)
  {
    printf("Nepodarilo se vytvorit hash tabulku\n");
    return EXIT_FAILURE;
  }

  FILE *fr = stdin;       // nastavime soubor pro cteni na stdin
  Trecord *tmp = NULL;    // tmp pro pristup k jednotlivym zaznamum v htable
  
  int state = 0;          // stav funkce indikujici prekorceni rozashu pro slovo
  char buffer[MAX + 2] = {0}; // pomocny buffer pro ukladani slov
  char *ptr = NULL;       // pomocny ukazatel pro alokaci pameti pro slova
  int length = MAX;       // pro uchovani navratove hodnoty fgetword

  // v cyklu nacitame slova ze souboru dokud jej cely neprecteme
  while ((length = fgetword(buffer, MAX + 2, fr)) != EOF)
  {
    if (length == 0)      // pokud bylo nacteno 0 znaku preskakujeme cyklus
      continue;

    // v pripade ze bylo nacteno slovo o delce MAX, vypiseme hlaseni
    if (length == (MAX + 1) && state == 0)
    {
      state = 1;          // nastavim state na 1  aby se hlaseni vypsalo pouze 1
      fprintf(stderr, "Nektera ze slov jsou prilis dlouha "
                      "a mohou byt z kracena!\n"); 
    }

    // hledame nactene slovo v hash tabulce 
    if ((tmp = htable_lookup(hash, buffer)) == NULL)
    {
      fprintf(stderr, "Chyba pri vyhledavani zaznamu\n");
      htable_free(hash); // v pripade chyby znicim htable a koncime program
      return EXIT_FAILURE;
    }
    
    // z duvodu hlaseni chyby valgrindem v pripade ponechani hodnoty z fgetword
    length = MAX;        // nastavime max na MAX velikost slova pro alokaci

    if (tmp->data == 0)  // pokud dany zaznam jeste nebyl inicializovan
    {
      // alokujeme pamet pro jeho klic
      if ((ptr = malloc(length + 1)) == NULL)
      {
        htable_free(hash); // v pripade chyby znicim htable a koncime program
        return EXIT_FAILURE;
      }

      // zkopirujeme nactene slovo z bufferu do alokovane pameti
      strncpy(ptr, buffer, length);
      tmp->key = ptr;    // nastavime ukazatel v zaznamu na tuto pamet
    }
    /*
    if ((tmp = htable_lookup(hash, buffer)) == NULL)
    {
      fprintf(stderr, "Nepodarilo se alokovat pamet pro zaznam\n");
      htable_free(hash);
      return EXIT_FAILURE;
    }*/

    tmp->data++;         // inkrementujeme cetnost slova v zaznamu 
  }

 // htable_remove(hash, "spreading"); // odstranime zaznam s klicem "spreading"

  htable_foreach(hash, (*htable_printrecord)); // vsechny zaznamy vytiskneme

 // htable_statistics(hash);  // vypiseme statistiky hashovaci tabulky

  htable_free(hash);     // hashovaci tabulku uklidime

  return EXIT_SUCCESS;   // uspesne ukoncime program
}

/* konec souboru wordcount.c */
