/*
 * Soubor:  htable_lookup.c
 * Datum:   20.04.2013 21:26
 * Autor:   Martin Roncka, xronck00 - FIT
 * Projekt: IJC-DU2
 */

#include "htable.h"
#include <stdlib.h>
#include <string.h>

Trecord * htable_lookup(Thtable *hash, const char *key)
{
  int state = 0;
  unsigned int h;
  Trecord *tmp, *prev;

  h = hash_function(key, hash->size);
  tmp = hash->array[h];
 
  while(1)
  {
    if (tmp == NULL)
    {
      if ((tmp = malloc(sizeof(Trecord))) == NULL)
      {
        return NULL;
      }
      if (state == 0)
        hash->array[h] = tmp;
      else
        prev->next = tmp;
     
      tmp->data = 0;
      tmp->next = NULL;

      return tmp;
    }
    else if (strcmp(tmp->key, key) == 0)
    {
      return tmp;
    }
    state++;
    prev = tmp;
    tmp = tmp->next; 
  }
}

/* konec souboru */
