/*
 * Soubor:  htable_statistics.c
 * Datum:   20.04.2013 21:26
 * Autor:   Martin Roncka, xronck00 - FIT
 * Projekt: IJC-DU2
 */

#include "htable.h"
#include <stdio.h>

void htable_statistics(Thtable *hash)
{
  unsigned int count = 0;
  unsigned int t = 0;
  unsigned int max = 0;
  unsigned int min = 0 - 1;
  float dia = 0;
  Trecord *tmp;


  for (int i = 0; i < hash->size; i++)
  {
    tmp = hash->array[i];

    t = 0;
    while (tmp != NULL) 
    {
      t++;
      if (t > max)
        max = t;
        
      if (t < min)
        min = t;

      tmp = tmp->next;
      count++;
    } 
  }

  dia = count / (float) hash->size;

  printf("  _________________________ \n"
         " |                         |\n"
         " |  Hash table statistics  |\n"
         " |_________________________|\n"
         " |                         |\n"
         " |  minimum: %2.d\t\t   |\n"
         " |  maximum: %2.d\t\t   |\n"
         " |  prumer:  %2.2f \t   |\n"
         " |_________________________|\n\n", min,max,dia);
}

/* konec souboru */
