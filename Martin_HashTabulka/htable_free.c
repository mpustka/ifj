/*
 * Soubor:  htable_free.c
 * Datum:   20.04.2013 21:26
 * Autor:   Martin Roncka, xronck00 - FIT
 * Projekt: IJC-DU2
 */

#include "htable.h"
#include <stdlib.h>
 
void htable_free(Thtable *hash)
{
  htable_clear(hash);
  free(hash);
}

/* konec souboru */
