/*
 * Soubor:  htable_remove.c
 * Datum:   20.04.2013 21:26
 * Autor:   Martin Roncka, xronck00 - FIT
 * Projekt: IJC-DU2
 */

#include "htable.h"
#include <stdlib.h>
#include <string.h>

/*
 * Funkce vyhleda zaznam a vymaze ze seznamu v hash tabulce

 * return value: 0      v pripade uspesneho nalezeni a vymazani
                 1      v pripade nenalazeni zaznamu
 */
int htable_remove(Thtable *hash, const char *key)
{
  int state = 0;
  unsigned int h;
  Trecord *tmp, *prev;

  h = hash_function(key, hash->size);
  tmp = hash->array[h];
 
  while(tmp != NULL)
  {
    if (strcmp(tmp->key, key) == 0)
    {
      if (state == 0)
        hash->array[h] = 0;
      else 
        prev->next = tmp->next;

      free(tmp->key);
      free(tmp);

      return 0;
    }
    prev = tmp;

    state++;
    tmp = tmp->next;
  }
  return 1;
}

/* konec souboru */
