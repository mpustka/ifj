/*
 * Soubor:  htable.h
 * Datum:   20.04.2013 12:15
 * Autor:   Martin Roncka, xronck00 - FIT
 * Projekt: IJC-DU2 
 * Popis:   rozhrani htable.h pro knihovnu libhtable
 */

typedef struct htable_listitem
{
  char *key;
  int data;
  struct htable_listitem *next;
} Trecord;

typedef Trecord *Tlist;

typedef struct htable
{
  unsigned int size;
  Tlist array[];
} Thtable;

////////////////////////
//                    //
//  Prototypy funkci  //
//                    //
////////////////////////

/* funkce pro vypocet klice do hashovaci tabulky */
unsigned int hash_function(const char *str, unsigned htable_size);

/* funkce na inicializaci hashovaci tabulky */
Thtable * htable_init(unsigned int size);

/* funkce pro vyhledani/pridani zaznamu v hash tabulce */
Trecord * htable_lookup(Thtable *hash, const char *key);

/* funkce pro volani funkce predane pres parametr nad vsemy zaznamy v htable */
void htable_foreach(Thtable *hash, void (*function)(const char *key, int value));

/* funkce vyhleda a zrusi zadanou polozku */
int htable_remove(Thtable *hash, const char *key);

void htable_clear(Thtable *hash);

void htable_free(Thtable *hash);

void htable_statistics(Thtable *hash);

/* funkce pro tisk cetnosti vyskytu slova */
void htable_printrecord(const char *key, int value);

/* konec souboru htable.h */
