/*
 * Soubor:  tail2.cc
 * Datum:   14.04.2013 20:19
 * Autor:   Martin Roncka, FIT - xronck00
 * Projekt: IJC_DU2 
 * Popis:   obdoba programu tail, ktery vypisuje poslednich 10 pripadne n radku
 *          souboru zadaneho parametrem, pripadne standardniho vstupu. 
 */


/* struktura pro ulozeni parametru prikazove radky */
struct params
{
  int n;                 // pocet raduk ktere se budou tisknout
  int state;             // stav podle nehoz se program zachova
  char *name;            // jmeno souboru ze ktereho se ma cist
};

/* prototyp funkce getparams ktera zpracovava argumenty CLI */
int getparams(int argc, char *argv[], struct params *ptr);

/* pripojeni hlavickovych souboru */
#include <iostream>              // pro vstup/vystup 
#include <deque>                 // pro double ended qeue
#include <fstream>               // pro cteni ze souboru
#include <string>                // pro praci s retezci

/* pouzity jmenny prostor */
using namespace std;             // abychom nemuseli psat std::

//ios::sync_with_stdio(false);

//int main(int argc, char *argv[])
int main(int argc, char *argv[])
{
  ios::sync_with_stdio(false);
  ifstream frd;                  // pramen pro cteni ze souboru
  streambuf *cinbuf = cin.rdbuf();  // ulozeni bufferu cin
 
  struct params par;             // struktura pro ulozeni zpracovanych argumentu
  int ret;                       // pro uchovani navratove hodnoty
  
  // volame funkci pro zpravovani argumentu prikazove radky
  if ((ret = getparams(argc, argv, &par)) != 0)
  {
    if (ret == -1)
      fprintf(stderr, "Pro napovedu spustte program s parametrem -help\n");
    else if (ret == -2)
      fprintf(stderr, "Za argumentem -n se ocekava kladne cislo\n");
    else
      fprintf(stderr, "Neplatne cislo argumentu -n\n");
    return EXIT_FAILURE;        // v pripade chyby ukoncime program
  }

  int n = par.n;                 // nahrajeme do n poceta radku 
  string ptr;                    // a prommennou pro nacteni retezce

  if (par.state == 0 && n == 0)  // v pripade vypisu poslednich 0 radku
    return EXIT_SUCCESS;         // uspesne koncime program

  if (par.name != NULL)          // pokud byl zadan soubor parametrem
  {
    frd.open (par.name);         // otevreme jej 
    cin.rdbuf(frd.rdbuf());      // a presmerujeme buffer cin na buffer souboru
  }

  if (par.state == 0)            // pokud byl spusten jako ./tail2 -n cislo
  {
    deque<string> lines;         // vytvorime oboustrannou frontu retezcu

    // cteme z pramene dokud nenarazime na koenc
    for (int i = 0; getline(cin, ptr); i++)
    {
      // pokud mame ulozeno vice radku nez chceme vypsat tak ve fronte cyklime
      if (i >= n)               
        lines.pop_front();       // vyhodime posledni zaznam
      lines.push_back(ptr);      // a nahrajeme novy
    }
  
    for (auto iter: lines)       // iteratorem prochazime nactene radk 
      cout << iter << "\n";      // a vypisujeme na standardni vystup
  }
  else if (par.state == 1)       // pokud byl spusten jako ./tail2 -n +cislo
  {
    for (int i = 1; getline(cin, ptr); i++)  // nacitame radky
      if (i >= n)                // jakmile prekrocime pozadovany pocet
        cout << ptr << "\n";     // zacneme radky vypisovat
  }

  if (par.name != NULL)          // pokud byl otevren soubor
  {
    cin.rdbuf(cinbuf);           // pak obnovime vychozi buffer cin
    frd.close();                 // a soubor zavreme
  }

  return EXIT_SUCCESS;           // uspesne ukoncujeme program
}

/*
 * Funkce pro zpracovani argumentu prikazove radky

 * return value: par             vytvorena struktura
          state: 0               vychozi, vypis poslednich n radku
                 1               vypis od n-teho radku
                -1               v pripade spatneho formatu argumentu
 */
int getparams(int argc, char *argv[], struct params *ptr)
{
  struct params par =
  {
    10,
    0,
    NULL,
  };
  
  // cyklus pro zpracovani vsech argumentu
  for (int i = 1; i < argc; i++)
  {
    // porovnavame zda argument neni shodny s "-n"
    if (*argv[i] == '-' && *(argv[i] + 1) == 'n' && *(argv[i] + 2) == '\0') 
    {
      // pokud ano musi nasledovat dalsi argument ktery udava hodnotu n
      if (argc <= (i + 1)) return -2;

      i++;                            // presun ke zpracovani dalsiho argumentu
      char *pt = argv[i];             // ukazatel pro kontrolu znaku v argumentu

      // kontrolujeme zda je program spusten jako ./tail -n +cislo
      if (*pt == '+')
      {
        par.state = 1;                // nastavime prislusny stav
        pt++;                         // a preskocime znak '+'
      }

      // provedem kontrolu zda je zbytek argumentu ve spravnem foramtu
      while (*pt != '\0')
      {
        // pokud se zde vyskytne znak ktery neni cislo vypiseme hlaseni
        if(!isdigit(*pt)) return -2;

        pt++;                         // posuuneme ukazatel na dalsi znak
      }

      par.n = atoi(argv[i]);          // ukladame do struktury
      if (par.n < 0) return -3;       // overime zda nedoslo k preteceni
    }
    else if (par.name == NULL)
      par.name = argv[i];             // nastavime jmeno souboru pro otevreni
    else // pokud je argumentu vice, nez dava smysl nastavime par.state na -1
      return -1;
  }

  if (ptr != NULL) *ptr = par;        // vratime hodnoty struktury pres odkaz

  return 0;                           // uspesne se vracime s funkce
}

/* konec souboru tail2.cc */
