/*
 * Soubor:  tail.c
 * Datum:   29.03.2013 13:37
 * Autor:   Martin Roncka, FIT - xronck00
 * Projekt: IJC_DU2 
 * Popis:   obdoba programu tail, ktery vypisuje poslednich 10 pripadne n radku
 *          souboru zadaneho parametrem, pripadne standardniho vstupu. 
 */


//////////////////////////////////////////////
//                                          //
//  Pripojeni hlavickovych souboru a makra  ////////////////////////////////////
//                                          //
//////////////////////////////////////////////

/* pripojeni hlavickovych souboru */
#include <stdio.h>       // vstup/vystupni funkce, konstant..
#include <string.h>      // pro praci s retezci
#include <ctype.h>       // pro urceni typu znaku
#include <stdlib.h>      // pro funkce prace s pameti

/* definice maker a konstant */
#define MAX 1024         // definujeme maximalni velikost radku


/////////////////////////
//                     //
//  Definice struktur  /////////////////////////////////////////////////////////
//                     //
/////////////////////////

/* struktura pro ulozeni radku */
struct line
{
  char *ptr;             // ukazatel na misto v pameti kde je radek ulozen
  unsigned int bytes;    // urcuje velikost radku
  unsigned int number;   // urcuje poradi radku v textu
  struct line *next;     // odkaz na nasledujici radek
}; 

/* struktura pro ulozeni parametru prikazove radky */
struct params
{
  int n;                 // pocet raduk ktere se budou tisknout
  int state;             // stav podle nehoz se program zachova
  char *name;            // jmeno souboru ze ktereho se ma cist
};


////////////////////////
//                    //
//  Prototypy funkci  //////////////////////////////////////////////////////////
//                    //
////////////////////////

/* funkce pro vypis napovedy */
void printhelp();
/* funkce getParams pro zpracovani argumentu soubrou */
int getparams(int argc, char *argv[], struct params *ptr);
/* funkce nacteni radku a alokaci pameti */
int getline(FILE *fr, int max, char **ptr);
/* funkce ktera se vola pro vypis od +n radku */
int printfrom(FILE *fr, int max, int n);
/* funkce pro vypis poslednich n radku */
int printlast(FILE *fr, int max, int n);
/* funkce pro uvolneni nactenych radku z pameti */
void freelines(struct line *first);


//////////////////////////////
//                          //
//  Hlavni funkce programu  ////////////////////////////////////////////////////
//                          //
//////////////////////////////

int main(int argc, char *argv[])
{
  // v pripade ze je program spusten s argumentem -help vypiseme napovedu
  if (argc == 2 && strcmp(argv[1], "-help") == 0)
  {
    printhelp();
    return EXIT_SUCCESS;        // ukoncujeme program uspesne
  }

  FILE *fr = stdin;             // pramen ze ktereho se bude cist
  struct params par;            // pro ulozeni argumentu prikazove radky
  int ret;

  // volame funkci pro zpravovani argumentu prikazove radky
  if ((ret = getparams(argc, argv, &par)) != 0)
  {
    if (ret == -1)
      fprintf(stderr, "Pro napovedu spustte program s parametrem -help\n");
    else if (ret == -2)
      fprintf(stderr, "Za argumentem -n se ocekava kladne cislo\n");
    else
      fprintf(stderr, "Neplatne cislo argumentu -n\n");
    return EXIT_FAILURE;        // v pripade chyby ukoncime program
  }
 
  // pokud bylo zadano jmeno v parametru, otevreme soubor s danym jmenem
  if (par.name != NULL)
    if ((fr = fopen(par.name, "r")) == NULL)
    {
      fprintf(stderr, "Nepodarilo se otevrit soubor\n");
      return EXIT_FAILURE;
    }

  int state = par.state;        // pro indikaci stavu programu
  int n = par.n;                // pocet raduk pro tisk

  if (n == 0 && state == 0)     // v pripade vypisu 0 radku
  {
    if (fr != stdin)            // pokud byl otevren nejaky soubor
      fclose(fr);               // zavreme jej
    return EXIT_SUCCESS;        // a uspesne ukoncime program
  }

  if (state == 1)               // vypisujeme od n-teho radku
  {
    printfrom(fr, MAX, n);      
  }
  else if (state == 0)          // vypisuje n poslednich radku
  {
    printlast(fr, MAX, n);
  }

  if (fr != stdin)              // pokud byl otevren nejaky soubor
    fclose(fr);                 // zavreme jej

  return EXIT_SUCCESS;          // a uspesne ukoncime program
}


///////////////////////
//                   //
//  Definice funkci  ///////////////////////////////////////////////////////////
//                   //
///////////////////////

/*
 * Funkce vytiskne napovedu na standardni vstup.
 */
void printhelp()
{
  printf("   __  __ _____ __    ____           \n"
         "  /\\_\\/\\_\\\\____\\\\_\\  /\\___\\ \n"
         "  \\/_____//___/\\/_/__\\/___/        \n"
         "  /\\_____\\\\___\\/\\____\\\\_\\     \n"
         "  \\/_/\\/_//____//____//_/           \n\n");
  printf("Program pro vypis casti souboru. Priklady pouziti:\n\n"
         "\t ./tail             vypise poslednich 10 radku ze stdin na stdout\n"
         "\t ./tail soubor      v pripade uvedeni souboru cte program z nej\n"
         "\t ./tail -n cislo    vypise poslednich <cislo> radku\n"
         "\t ./tail -n +cislo   vypisuje od radku <cislo>\n\n");

  return;
}

/*
 * Funkce pro zpracovani argumentu prikazove radky

 * return value: 0          pri uspesnem nacteni parametru
                 -1         v pripade spatneho formatu argumentu
 */
int getparams(int argc, char *argv[], struct params *ptr)
{
  struct params par =
  {
    .n = 10,
    .state = 0,
    .name = NULL,
  };
  
  // cyklus pro zpracovani vsech argumentu
  for (int i = 1; i < argc; i++)
  {
    // porovnavame zda argument neni shodny s "-n"
    if (strcmp(argv[i], "-n") == 0)
    {
      // pokud ano musi nasledovat dalsi argument ktery udava hodnotu n
      if (argc <= (i + 1)) return -2;

      i++;                            // presun ke zpracovani dalsiho argumentu
      char *pt = argv[i];             // ukazatel pro kontrolu znaku v argumentu

      // kontrolujeme zda je program spusten jako ./tail -n +cislo
      if (*pt == '+')
      {
        par.state = 1;                // nastavime prislusny stav
        pt++;                         // a preskocime znak '+'
      }

      // provedem kontrolu zda je zbytek argumentu ve spravnem foramtu
      while (*pt != '\0')
      {
        // pokud se zde vyskytne znak ktery neni cislo vracime -2
        if(!isdigit(*pt)) return -2;

        pt++;                         // posuuneme ukazatel na dalsi znak
      }

      par.n = atoi(argv[i]);          // ukladame cislo pres *n
      if (par.n < 0) return -3;       // overime zda nedoslo k preteceni
    }
    else if (par.name == NULL)
      par.name = argv[i];             // nastavime jmeno souboru pro otevreni
    else               // pokud je argumentu vice, nez dava smysl, vracime -1
      return -1;
  }

  *ptr = par;                         // zkopirujeme strukturu pres ukazatel
  
  return 0;
}

/*
 * Funkce nacita radek pomoci fgets, zjistuje zda byl nacten cely, alokuje
   pro nej misto v pameti a odkaz vrati pres ukazatel ptr. Funkce vraci
   pocet nactenych radku. V pripade ze je znak moc dlouhy alokuje se max
   velikost radku, kde se ulozi radek ve zkracene podobe.

 * return value: n          pocet nactenych radku
                 max + 1    v pripade preteceni
                 -1         v pripade konce souboru nebo chyby
 */
int getline(FILE *fr, int max, char **ptr)
{ 
  char auxbuff[max + 1];       // pomocny buffer - neni potreba inicializovat
  int bytes = 0;               // pro ulozeni velikosti radku
  *ptr = NULL;                 // nastavime ukazatel na radek na NULL

  if (fgets(auxbuff, (max + 1), fr) != NULL)
  {
    // pokud v nactenem retezci nenalezneme znak '\n' radek nebyl nacten cely
    if (strchr(auxbuff, '\n') == NULL)
    {
      // naalokujeme pamet o velikosti max velikosti radku + 1 pro znak '\0'
      if ((*ptr = malloc(max + 1)) == NULL)
        return -2;
        
      auxbuff[max -1] = '\n';  // dosadime na konec zkraceneho radku znak '\n'
      strcpy(*ptr, auxbuff);   // zkopirujem prvni cast radku

      while (fgets(auxbuff, (max + 1), fr) != NULL)  // budeme cist zbytek radku
        if (strchr(auxbuff, '\n') != NULL) break;    // dokud nenajdeme '\n'

      return max + 1;          // navratova hodnota indikuje ze doslo k preteceni
    }
    else
    {
      bytes = strlen(auxbuff); // zjistime veliksot nacteneho radku

      if ((*ptr = malloc(bytes + 1)) == NULL)  // alokujeme pamet pro radek
        return -2;

      strcpy(*ptr, auxbuff);   // zkopirujeme radek do alokovane pameti

      return bytes;            // vratime pocet celkove nactenych znaku
    }
  }
  else
    return -1;                 // vracime -1 pri docteni nebo chybe 
}

/*
 * Funkce pro vypisovani od urciteho radku 

 * return value: count      pocet vytisknutych radku
                 -count     pokud byl nektery z radku prilis dlouhy
 */
int printfrom(FILE *fr, int max, int n)
{
  int count = 0;             // promenna pro citani od ktereho radku se vypisuje
  int state = 0;             // promenna pro indikaci stavu funkce
  char auxbuff[max + 1];     // auxilary buffer pro ulozeni nacteneho radku

  // postupne prochazime radky az do konce souboru
  while (fgets(auxbuff, (max + 1), fr) != NULL)
  {
    count++;                 // inkrementujeme pocitadlo

    // kontrolujeme zda se nacetl cely radek a pak zda se jiz ma vypisovat
    if (strchr(auxbuff, '\n') == NULL )
    {
      // pri prvnim nacteni prilis dlouheho radku ulozime pozici
      if (state == 0 && count  >= n)
      {
        state = -1;
        fprintf(stderr, "Nektere radky mohou byt vypsany zkracene\n");
      }
        
      auxbuff[max - 1] = '\n';   // dosadime na konec zkraceneho radku znak '\n'

      if (count >= n)                      
        printf("%s", auxbuff);   // vypiseme zkraceny radek

      while (fgets(auxbuff, (max + 1), fr) != NULL)  // budeme cist zbytek radku
        if (strchr(auxbuff, '\n') != NULL) break;    // dokud nenajdeme '\n'
    }
    else if (count >= n)         // pokud jsme na/za n-tym radkem 
      printf("%s", auxbuff);     // zacneme radky opsivovat na stdut
  }

  return count * state;        
}

/*
 * Funkce pro vypis poslednich n radku ze souboru pripadne stdin.

 * return value: number     pocet vytisknutych radku
                 -1         pokud doslo k chybe pri alokaci
 */
int printlast(FILE *fr, int max, int n)
{
  char *ptr;                // pro kontrolu zda doslo k uspesnemu nacteni
  int ret;                  // promenna pro docasne ulozeni velikosti radku
  int number = 0;           // indikuje na kterem radku se nechazime
  int state = 0;            // pro indikaci stavu funkce
  struct line *first, *tmp; // ukazatele na strukturu radku

  // alokujeme pamet pro prvni radek a kontrolujeme uspesnost
  if ((first = malloc(sizeof(struct line))) == NULL)
  {
    fprintf(stderr, "Nepodarilo se alokvoat pamet\n");
    return -1;              // vracime hodnotu indikujici chybu
  }

  first->ptr = NULL;        // ukazatel na textr zatim nastavmie na NULL
  first->next = NULL;       // ukazatel na nasledujici radek zatim na NULL
  tmp = first;              // tmp nastavime na prvni radek
   
  // v nekonecnem cyklu nacitame radky a prubezne alokujeme potrebnou pamet
  while (1)
  {
    number++;               // inkrementujeme pocet radku 

    // volame funkci getline, jejiz navratovou hodnotu ukladame do ret
    ret = getline(fr, max, &ptr);

    // na zaklade navratove hodnoty prochazime vetve funkce
    if (ret == -1)          // pokud jsme na konci souboru
      break;                // ukoncujeme cyklus
    else if (ret == -2)     // pokud doslo k chybe pri alokaci
    {
      freelines(first);     // uvolnime naalokovanou pamet
      return -1;            // a koncime funkci s hodnotou indikujici chybu
    }
    else if (ret > max)     // pokud byl presazen limit delky radku
    {
      state = number;       // nastavime state na cislo radku kde k tomu doslo
      ret--;                // a odecteme 1, viz navratova hodnot getline
    }
   
    if (number > n)         // pokud jsme alokovali dostatecny pocet polozek
    {
      tmp->next = first;    // zacyklime seznam
      tmp = first;          // priradime do tmp prvni polozku
      if (first->next != NULL)
        first = first->next;  // a druhou polozku oznacime jako prvni
      tmp->next = NULL;     // "uzemnime" konec seznamu
    }

    tmp->ptr = ptr;         // priradeime alokovanou pamet do struktury radku
    tmp->bytes = ret;       // nastavime velikost radku
    tmp->number = number;   // a oznacime cislo radku v textu

    if (number < n)
    {
      if ((tmp->next = malloc(sizeof(struct line))) == NULL)
      {
        // pokud se nepodarilo, vypiseme hlaseni a ukoncujeme cteni
        fprintf(stderr, "Nepodarilo se alokovat pamet\n");
        freelines(first);   // uvolnime pamet kterou jsme naalokovali
        return -1;          // navratova hodnota indikuje chybu
      }

      tmp = tmp->next;      // priradime do tmp nove ziskany pointer 
      tmp->ptr = NULL;      // nastavime ukazatel na text radku na NULL
      tmp->next = NULL;     // stejne tak odkaz nanasledujici radek
    }
  }

  tmp = first;              // opet nastavime tmp na ukazatel prvniho radku

  // postupne prochazime seznamem a tiskneme radky
  while (tmp != NULL && tmp->ptr != NULL)
  {
    // pokud doslo pri cteni k preteceni vypiseme hlaseni v danem miste
    if (state == tmp->number)
      fprintf(stderr, "Nektere radky mohou byt vypsany zkracene\n");

    printf("%s", tmp->ptr); // tiskneme ulozeny radek
    tmp = tmp->next;        // posouvame se v seznamu na nasledujici polozku
  }

  freelines(first);         // volame funkci pro uvolneni seznamu
  
  return number;            // vracime pocet nacetnych radku
}

/*
 * Funkce prochazi seznamem nactenych radku a uvolnuje pamet, ktera byla
   alokovana pro retezec i pro samotnou strukturu
 */
void freelines(struct line *first)
{
  struct line *tmp, *des;     // dva ukazatele pro destrukci

  tmp = first;                // tmp nastavime na prvni polozku
  while (tmp != NULL)         // dokud nebude tmp NULL provadime uklid
  {
    des = tmp;                // ulozime si soucasny ukazatel

    if (tmp->ptr != NULL)     // pokd je alokovana pamet pro retezec
      free(tmp->ptr);         // uvolnime ji

    tmp = tmp->next;          // tmp presuneme na dalsi polozku
    free(des);                // a soucasnou uklidime
  }
}

/* konec souboru tail.c */
