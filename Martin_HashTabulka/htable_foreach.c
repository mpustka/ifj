/*
 * Soubor:  htable_foreach.c
 * Datum:   20.04.2013 21:26
 * Autor:   Martin Roncka, xronck00 - FIT
 * Projekt: IJC-DU2
 */

#include "htable.h"
#include <stdlib.h>

void htable_foreach(Thtable *hash, void (*function)(const char *key, int value))
{
  for (int i = 0; i < hash->size; i++)
  {
    Trecord *tmp = hash->array[i];

    while (tmp != NULL) 
    {
      function(tmp->key, tmp->data);
      tmp = tmp->next;
    } 
  }
}

/* konec souboru */
