/*
 * Soubor:  io.c
 * Datum:   20.04.2013 14:38
 * Autor:   xronck00
 * Projekt: 
 * Popis:   
 */

#include "io.h"
#include <stdio.h>
#include <ctype.h>

int fgetword(char *s, int max, FILE *fr)
{
  int c;
  int i = 0;

  while ((c = getc(fr)) != EOF)
  {
    if (isspace(c))
    {
      if (i == 0)
        continue;
      break;
    }

    if (i == max - 1)
      continue;

    s[i] = c;
    i++;
  }

  s[i] = '\0';

  if (c == EOF && i == 0) return EOF;

  return i;
}

/* konec souboru io.c */
