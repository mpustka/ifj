
# novy test by mel fungovat takhle:
# bude testovat kolekci testu, vytvoris si slozku v ni slozky out, in, ret_val.. testy ulozis do dane slozky, vystupy do out, vstupy in a navratove hodnoty taky... test pak projede vsechny soubory ve slozce napr:
# test1.php < test1.php.in 1>out.php
# udela diff  out.php s test1.php.out a pak jeste porovna navratovou hodnotu s test1.php.ret
# soubory muzou byt volitelne... kdyz bude chybet treba test1.php.ret.. bude implicitne porovnavat s hodnotou 0.. nebo nebude test1.php.out tak nebude diffovat
# ve zkratce:
# kazda kolekce testu bude mit vlastni slozku at je v tom poradek... test projede vsechny testy v ni.. bude hledat out, in a ret_val
#
# TLDR: hleda slozky in, out a ret_val, pri nalezeni spousti testy, pouziva vstupy z in (defaultuji na ""), vystupy z out (defaultuji na "") a ukoncovaci hodnoty z ret_val (defaultuji na 0)

# autotest.sh [-c] [-b] [-d] [SLOZKA]
# -c		zapina barevny vystup
# -m		make clean && make pred spustenim testu
# -d		po dobehnuti testu odstranuje soubory *.tmp
# -v		kontrola valgrindem
# SLOZKA	omezeni testu na konkretni slozku

RUN=0 # pocet spustenych testu
OK=0 # pocet uspesnych testu
MAIN=`readlink -f "../src/main"` # cesta k mainu
GREEN=""
RESET=""
RED=""
OPTS=0
CLEANUP=0
VALGRIND=0
VALGRINDCMD="valgrind -v --leak-check=full"

function traverse(){
	THIS=`pwd | sed "s/.*\///"`
	if [ $# -gt $OPTS ]; then
		for arg in "$@"; do
			[ "$arg" == "$THIS" ] && phptest && break
		done
	else
		phptest
	fi
	for each in *; do
	if [ -d "$each" ]; then
			cd "$each"
			traverse "$@"
			cd .. > /dev/null
		fi
	done
}

function phptest(){
#	if [ -d out ] && [ -d in ] && [ -d ret_val ]; then
		> out.out
		SRUN=0
		SOK=0
		SECTION=`pwd | sed "s/.*\///"`
		[ "$SECTION" == "out" ] && return
		[ "$SECTION" == "in" ] && return
		[ "$SECTION" == "ret_val" ] && return
		echo "~ SECTION $SECTION ~"
		for test in *.php; do
			[ "$test" == "*.php" ] && break;
			((RUN++))
			((SRUN++))
			test=`echo "$test" | sed "s/.*\///"`
			if [ -f "ret_val/$test" ]; then
				RET=`cat "ret_val/$test"`
			else
				RET=0
			fi
			echo $test
			[ -d "in" ] || mkdir "in"
			[ -d "out" ] || mkdir "out"
			[ -f "in/$test" ] || touch "in/$test"
			if [ $VALGRIND -eq 1 ]; then
				$VALGRINDCMD "$MAIN" "$test" < "in/$test" > "out/$test.tmp" 2>"out/$test.valgrind"
				END=$?
			else
				"$MAIN" "$test" < "in/$test" > "out/$test.tmp" 2>"out/$test.err"
				END=$?
			fi
			if [ $END == $RET ]; then
				if [ -f "out/$test" ]; then
					diff -N -b "out/$test" "out/$test.tmp" 1>"out/$test.diff" 2>/dev/null
					if [ $? -eq 0 ]; then
						echo -e "$GREEN[ OK ]$RESET"
						echo
						((OK++))
						((SOK++))
					else
						echo -e "$RED[ error ]$RESET"
						echo -e "$RED[->$RESET Ocekavany vystup: \"`cat out/$test 2>/dev/null`\""
						echo -e "$RED[->$RESET Vraceny vystup:   \"`cat out/$test.tmp 2>/dev/null`\""
						echo
					fi
				else
					echo -e "$GREEN[ OK ]$RESET"
					echo
					((OK++))
					((SOK++))
				fi
			else	
				echo -e "$RED[ error ]$RESET" 
				echo -e "$RED[->$RESET ocekavan return: $RET"
				echo -e "$RED[->$RESET ukonceno s:      $END"
				echo
			fi
		done
		echo "V sekci \"$SECTION\" uspesne probehlo $SOK z $SRUN testu."
		echo
		if [ $CLEANUP -eq 1 ]; then
			rm out/*.tmp 2>/dev/null
			rm out/*.valgrind 2>/dev/null
			rm out/*.diff 2>/dev/null 
			rm out/*.err 2>/dev/null
		fi
#	fi
}

while getopts ":cmvd" ARG; do
	case "$ARG" in
		c)
			GREEN="\e[32m"
			RESET="\e[0m"
			RED="\e[35m"
			((OPTS++))
			;;
		m)
			cd `echo $MAIN | sed "s/main//"`
			make clean
			make
			cd - > /dev/null
			((OPTS++))
			;;
		d)
			CLEANUP=1;
			((OPTS++))
			;;
		v)
			VALGRIND=1;
			((OPTS++))
			;;
	esac
done

for each in *; do
    if [ -d "$each" ]; then
	cd "$each"
	traverse "$@"
	cd ..
    fi
done

echo "Celkem uspesne probehlo $OK z $RUN testu."
