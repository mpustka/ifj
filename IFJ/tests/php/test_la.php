<?php

// Test na vypis retezce, plus zpracovani escape sekvence a rozeznani klicovych slov 
// Vystup: Uvozovka " lomitko \ konec radku
// dolar $ tab   vykricnik!
function ifi($x)
{
  $x = put_string("Uvozovka \" lomitko \\ konec radku \n dolar \$  tab \t vykricnik\x21 \n");
}

// Test na blokove kometare
/*Jsem komentar *****/ 
/*Jsem druhy komentar \* tady jeste pokracuju */
// Melo by to pojmout vsechny *

//Test na ciselne promenne - ma vrace true, rozena ID od id?
$id=99.0;
$ID=99.0e0;
if($id===$ID)
{
  $x = put_string("True\n");
}
else
{
 $x = put_string("False\n");
}

//Test porovnani retezce a intu - ma vracet false
// Syntax error: unexpected 'function ...'.
//Syntax error: unexpected 'function ...'.

$name=99;
$nA_me="99";
if($name===$nA_me)
{
  $x = put_string("True\n");
}
else
{
 $x = put_string("False\n");
}

// Volani funkce ifi - pri zkouseni probehla spravne kontrola:
// nedeklarovanych funkci, neinicializovanych promennych, spatny pocet 
// parametru =)
$x="True";
$lk = ifi($x);                                                                                                                           
