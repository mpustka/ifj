#!/usr/local/bin/bash
RUN=0 # pocet spustenych testu
OK=0 # pocet uspesnych testu
MAIN="../src/main" # cesta k mainu
TEST="./php" # slozka s php testy
LOG="./log" # slozka pro logovani

# barvy
ZELENA="\e[32m"
RESET="\e[0m"
CERVENA="\e[35m"

#make
cd ../src
make clean
make
cd ../tests

[ $# -eq 0 ] && ALL=1
for arg in `seq 1 $#`; do
	if [ "$1" == "ALL" ]; then
		ALL=1
	elif [ "$1" == "BASE" ]; then
		BASE=1
	elif [ "$1" == "BOOLVAL" ]; then
		BOOLVAL=1
	elif [ "$1" == "EMBEDDED" ]; then
		BOOLVAL=1
	elif [ "$1" == "HELP" ]; then
		echo "Mozne parametry jsou ALL BOOLVAL EMBEDDED HELP LOG"
		exit 0
	elif [ "$1" == "LOG" ]; then
		LOGENABLE=1
	fi
	shift
done

function log { # funkce pro logovani
	echo "ERR:" > "$LOG/test$RUN.log"
	$MAIN "$TEST/$1" 1>/dev/null 2>> "$LOG/test$RUN.log"
	echo -e "\nLOG:" >> "$LOG/test$RUN.log"
	$MAIN "TEST/$1" 2>/dev/null 1>> "$LOG/test$RUN.log"
}

function rtest { # funkce pro testovani navratove hodnoty programu
	# $1 - popisek testu
	# $2 - nazev souboru testu
	# $3 - ocekavana navratova hodnota
	((RUN++))
	echo "Test $RUN - $1"
	OUTPUT=`$MAIN "$TEST/$2" 2>/dev/null`
	RESULT=$?
	
	if [ $RESULT -eq $3 ]; then
		printf "$ZELENA[ OK ]$RESET\n"
		((OK++))
	else
		printf "$CERVENA[ error ]$RESET - ocekavan return $3, vracen $RESULT\n"
	fi
	[ "$LOGENABLE" == 1 ] && log $2
	echo
}

function otest { # funkce pro testovani vystupu programu
	# $1 - popisek testu
	# $2 - nazev souboru testu
	# $3 - ocekavany vystup
	((RUN++))
	echo "Test $RUN - $1"
	OUTPUT=`$MAIN "$TEST/$2" 2>/dev/null`
	RESULT=$?

	if [ $RESULT -eq 0 ]; then
		if [ "$OUTPUT" == "$3" ]; then
				printf "$ZELENA[ OK ]$RESET\n"
				((OK++))
		else
				printf "$CERVENA[ error ]$RESET - ocekavan vystup $3, vracen $OUTPUT\n"
		fi
	else
			printf "$CERVENA[ error ]$RESET - ocekavan return 0, vracen $RESULT\n"
	fi
	[ "$LOGENABLE" == 1 ] && log $2
	echo
}

function iotest { # funkce pro testovani vystupu programu
	# $1 - popisek testu
	# $2 - nazev souboru testu
	# $3 - standartni vstup
	# $4 - ocekavany vystup
	((RUN++))
	echo "Test $RUN - $1"
	OUTPUT=`$MAIN "$TEST/$2" <$3 2>/dev/null`
	RESULT=$?

	if [ $RESULT -eq 0 ]; then
		if [ "$OUTPUT" == "$4" ]; then
				printf "$ZELENA[ OK ]$RESET\n"
				((OK++))
		else
				printf "$CERVENA[ error ]$RESET - ocekavan vystup $3, vracen $OUTPUT\n"
		fi
	else
			printf "$CERVENA[ error ]$RESET - ocekavan return 0, vracen $RESULT\n"
	fi
	[ "$LOGENABLE" == 1 ] && log $2
	echo
}

[ "$LOGENABLE" == 1 ] && [ -d "$LOG" ] || mkdir -p "$LOG"

if [ "$ALL" == 1 ] || [ "$BASE" == 1 ]; then
	rtest "Spatna pocatecni znacka <?php" spatna_znacka.php 1
	rtest "Znaky pred <?php" bordel.php 2
	rtest "Pouziti nedeklarovanych promennych" nedek1.php 5
	rtest "Pouziti nedeklarovanych promennych" block_test.php 5
	rtest "Pouziti nedeklarovanych promennych" nedek2.php 5
	rtest "Pouziti nedeklarovanych promennych" nedek3.php 0
	otest "Vychozi navratove hodnoty z funkce" defret.php "4"
	rtest "Redefinice/overloadu funkce" redef1.php 3
	rtest "Redefinice/overloadu funkce" redef2.php 3
	rtest "Redefinice/overloadu vestavene funkce" redef3.php 3
	rtest "Nadbytecny poctet paremetru ve funkci" spatny_pocet_par2.php 4
	rtest "Chybejici paremetr ve funkci" spatny_pocet_par.php 4
	rtest "Nadbytecny poctet paremetru ve vestavane funkci" embedded_spatny_pocet_par.php 4
	rtest "Chybejici paremetr ve vestavane funkci" embedded_spatny_pocet_par2.php 4
	otest "Prima rekurze" pow.php "4 27 256 16807 "
	otest "Neprima rekurze" ping.php "ping pong ping pong ping pong ping pong "
	otest "Citlivost na male/velke znaky" case_sensitive1.php "4"
	rtest "Citlivost na male/velke znaky" case_sensitive2.php 0
	rtest "Vychozi navratova hodnota programu" retval.php 0
	rtest "Platnost promenne mimo blok" block1.php 5
	rtest "Platnost promenne mimo blok" block2.php 5
	otest "Nerekurzivni faktorial" pdf_faktorial.php "Vysledek je: 120"
	otest "Rekurzivni faktorial" pdf_faktorial_rek.php "Vysledek je: 120"

fi
if [ "$ALL" == 1 ] || [ "$BOOLVAL" == 1 ]; then
  otest "kontrolva vypisu pri rekurzi" block_test2.php "554535343332313000012342"
	otest "Pretypovani null->bool" embedded_tests/boolval_null.php "false"
	otest "Pretypovani int->bool" embedded_tests/boolval_int-0.php "false"
	otest "Pretypovani int->bool" embedded_tests/boolval_int-1.php "true"
	otest "Pretypovani bool->bool" embedded_tests/boolval_bool-0.php "false"
	otest "Pretypovani bool->bool" embedded_tests/boolval_bool-1.php "true"
	otest "Pretypovani double->bool" embedded_tests/boolval_double-0.php "false"
	otest "Pretypovani double->bool" embedded_tests/boolval_double-1.php "true"
	otest "Pretypovani string->bool" embedded_tests/boolval_string-0.php "false"
	otest "Pretypovani string->bool" embedded_tests/boolval_string-1.php "true"
fi
echo "Spravne probehlo $OK z $RUN testu."
