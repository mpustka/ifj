<?php
// ~DOUBLECHECK
$i=doubleval("z");
$i=doubleval("00.00e0");
$i=doubleval("00.00E0");
$i=doubleval(" \n\t+0e+1");
//$i=doubleval("1e+z");
//$i=doubleval("+z");
$i=doubleval("1z");
//$i=doubleval("1ez");

// ~BOOLVAL
$i=boolval(null);
$i=boolval(true);
$i=boolval(false);
$i=boolval(2);
$i=boolval(0);
$i=boolval(1.0);
$i=boolval(0.0);
$i=boolval("asd");
$i=boolval("");

// ~INTVAL
$i=intval(null);
$i=intval(true);
$i=intval(false);
$i=intval(1);
$i=intval(0);
$i=intval(1.0);
$i=intval(0.0);
$i=intval("asd");
$i=intval("");

// ~DOUBLEVAL
$i=doubleval(null);
$i=doubleval(true);
$i=doubleval(false);
$i=doubleval(1);
$i=doubleval(0);
$i=doubleval(1.0);
$i=doubleval(0.0);
$i=doubleval("asd");
$i=doubleval("");

// ~STRVAL
$i=strval(null);
$i=strval(true);
$i=strval(false);
$i=strval(1);
$i=strval(0);
$i=strval(1.0);
$i=strval(0.0);
$i=strval("asd");
$i=strval("");

// ~PUTSTRING
$i=put_string(null,true,1,1.1,"asd");

// ~GETSUBSTRING
$i=get_substring("asd",1,2);

// ~GETSTRING
$i=get_string();

// ~FINDSUBSTRING
$i=find_string("ahoj karle", "ahoj");

// ~SORTSTRING
$i=sort_string("qwegdge gergafhkawh lagjlif sljfwelrvtghffiu");

/*hextodec*/
$i="\xb3";
$i="\xbb";
$i="\x11";

$i = 3 + 3;
$i = 1 - 1;
$i = 3 * 3;
$i = 4 / 4;

$i = 3+3;
$i=1-1;
$i=3*3;
$i=4/4;
$i="asd" . 3;

if(1 < 1) {} else {}
if(1 > 1) {} else {}
if(1 === 1) {} else {}
if(1 !== 1) {} else {}
$i=(1+1);
if(1 >= 1) {} else {}
if(1 <= 1) {} else {}
if(1<1){}else{}
if(1>1){}else{}
if(1===1){}else{}
if(1!==1){}else{}
$i=(1+1);
if(1>=1){}else{}
if(1<=1){}else{}


