#!/bin/sh
#
# Soubor:  script.sh
# Datum:   27.11.2013 17:21
# Autor:   xronck00
#

cd IFJ/src
make > make.log 2> compile.log
valgrind --tool=memcheck ./main tests/code.php >output.log 2> valgrind.log

# konec souboru script.sh
