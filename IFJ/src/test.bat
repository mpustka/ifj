@ECHO OFF
IF "%1"=="-dir" (
  dir /a:d /b
  exit /b
)

  @ECHO OFF 
  SETLOCAL DisableDelayedExpansion
  SET "r=%__CD__%"
  FOR /R . %%F IN (*) DO (
  SET "p=%%F"
  SETLOCAL EnableDelayedExpansion
  ECHO(!p:%r%=!
  ENDLOCAL
  )
  exit /b

