//
// embedded.c
//
// Vestavene funkce
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	28. 10. 2013
// Autor:		Adam Ruzicka (xruzic43)

#include "embedded.h"
TStack *stringStack = NULL;

/**
 * Funkce odstranuje bile znaky ze vstupniho retezce
 * @param  inputString vstupni retezec
 * @param  errorCode   promenna ve ktere se vraci chybovy kod funkce
 * @return             retezec s odstranenymi bilymi znaky
 */
char *StripWS(char *inputString, int *errorCode) {
    *errorCode = RESULT_OK;
    char *inputStringBackup = inputString; // zalohovani pointeru na zacatek vstupniho retezce
    int whitespaces = 0;
    while (*inputString != '\0') { // pruchod vstupnim retezcem znak po znaku, pocitani bilych znaku
        if ((*inputString == ' ') || (*inputString == '\t')
                || (*inputString == '\n'))
            whitespaces++;
        inputString++;
    }
    char *result = NULL;
    inputString = inputStringBackup;
    if ((result = malloc(
                      (strlen(inputStringBackup) - whitespaces) * sizeof(char)))) { // alokace pameti pro retezec bez bilych znaku
        inputStringBackup = result;
        while (*inputString != '\0') { // odstraneni bilych znaku ze vstupniho retezce
            if ((*inputString != ' ') && (*inputString != '\t')
                    && (*inputString != '\n')) {
                *result = *inputString;
                result++;
            }
            inputString++;
        }
        *result = '\0';
        *errorCode = RESULT_OK;
        return inputStringBackup;
    } else {
        // chyba pri alokaci
        *errorCode = ERROR_INTERPRET;
    }
    return NULL;
}

int DoubleCheck(char *data) {
    char state;
    state=DCS_START;
    while (*data != '\0') {
        switch(state) {
        case DCS_START:
            if((*data >= '0' ) && (*data<='9')) {
                state = DCS_INT;
            } else if (*data == '+') {
                state = DCS_INTS;
            } else if ((*data ==' ') || (*data == '\n') || (*data == '\t')) {
            } else {
                return 0;
            }
            break;
        case DCS_INTS:
            if((*data >= '0') && (*data <= '9')) {
                state = DCS_INT;
            } else {
                return -1;
            }
            break;
        case DCS_INT:
            if((*data >= '0' ) && (*data <= '9')) {
            } else if (*data == '.') {
                state = DCS_DEC0;
            } else if ((*data == 'e') || (*data == 'E' )) {
                state = DCS_EXPS;
            } else {
                return 1;
            }
            break;
        case DCS_DEC0: // desetinna cast (za teckou) [0-9]
            if((*data >= '0') && (*data <= '9')) { //za teckou cifra
                state = DCS_DEC1;
            } else {
                return -1;
            }
            break;
        case DCS_DEC1: // desetinna cast po prvni cislici po tecce [0-9Ee]
            if((*data >= '0') && (*data <= '9')) {
            } else if ((*data == 'e') || (*data == 'E')) {
                state = DCS_EXPS;
            } else {
                return 1;
            }
            break;
        case DCS_EXPS: // exponent pred znamenkem [0-9] + -
            if((*data >= '0') && (*data <= '9'))
            	return 1;
            else if ((*data == '+') || (*data == '-')) {
                state = DCS_EXP;
            } else {
                return -1;
            }
            break;
        case DCS_EXP: // exponent po znamenku [0-9]
            if((*data >= '0') && (*data <= '9')) {
                return 1;
            } else {
                return -1;
            }
            break;
        }
        data++;
    }
    return true;
}

/**
 * Funkce pro pretypovani na bool
 * @param  data pointer na data ktera chceme pretypovat
 * @param  typ  typ dat na ktere pointer ukazuje
 * @return      bool hodnota po prevodu
 */
bool BoolVal(void *data, char typ) {

    switch (typ) {

    case INTR_VAR_NULL:
    case LEX_NULL:
        return false;
    case INTR_VAR_BOOL:
    case LEX_BOOL:
        return (*((int *) data)==1) ? true : false;
        break;
    case INTR_VAR_INT:
    case LEX_INT:
        return ((*((int *)data)) == 0) ? false : true;
        break;
    case INTR_VAR_DOUBLE:
    case LEX_DOUBLE:
        return (*((double *) data) == 0.0) ? false : true;
        break;
    case INTR_VAR_STRING:
    case LEX_STRING:
        return ((strlen((char *) data)) == 0) ? false : true;
        break;
    }
    return 0;
}

/**
 * Funkce pro pretypovani na int
 * @param  data      pointer na data, ktera chceme pretypovat
 * @param  typ       typ dat, na ktere pointer ukazuje
 * @param  errorCode promenna, ve ktere se vraci chybovy kod funkce
 * @return           int hodnota po prevodu
 */
int IntVal(void *data, char typ, int *errorCode) {
    int result = 0;

    *errorCode = RESULT_OK;
    switch (typ) {
    case INTR_VAR_NULL:
    case LEX_NULL:
        return 0;
    case INTR_VAR_BOOL:
    case LEX_BOOL:
        return *(char *) data;
        break;
    case INTR_VAR_INT:
    case LEX_INT:
        return *(int *) data;
        break;
    case INTR_VAR_DOUBLE:
    case LEX_DOUBLE:
        return (int) (*(double *) data);
        break;
    case INTR_VAR_STRING:
    case LEX_STRING:
    	while (isspace((int)*((char *)data)))
    		data = ((char *) data) + 1;

    	if (isdigit((int)*(char *)data))
    	{
    		sscanf(data, "%d", &result);
    		return result;
    	}
    	else
    		return 0;
        break;
    }
    return 0;
}

/**
 * Funkce pro pretypovani na double
 * @param  data      pointer na data, ktera chceme pretypovat
 * @param  typ       typ dat, na ktere pointer ukazuje
 * @param  errorCode promenna, ve ktere se vraci chybovy kod funkce
 * @return           double hodnota po prevodu
 */
double DoubleVal(void *data, char typ, int *errorCode) {
    *errorCode = RESULT_OK;
    double result = 0.0;
	int doubleCheckResult;
    switch (typ) {
    case INTR_VAR_BOOL:
    case LEX_BOOL:
        return ((*(char *) data) == true) ? 1.0 : 0.0;
        break;
    case INTR_VAR_INT:
    case LEX_INT:
        return (double) * (int *) data;
        break;
    case INTR_VAR_DOUBLE:
    case LEX_DOUBLE:
        return *(double *) data;
        break;
    case INTR_VAR_STRING:
    case LEX_STRING:
		doubleCheckResult = DoubleCheck((char *) data);
        if(doubleCheckResult==1) {
            sscanf((char *) data,"%lf", &result);
        } else if (doubleCheckResult==0) {
			return 0.0;
		}
		else {
        	*errorCode=ERROR_SEM_CONV; // ted nevim tu hodnotu
        }
        return result;
        break;
    case INTR_VAR_NULL:
    case LEX_NULL:
        return 0.0;
    }
    return 0.0;
}

/**
 * Funkce pro pretypovani na string
 * @param  data      pointer na data, ktera chceme pretypovat
 * @param  typ       typ dat, na ktere pointer ukazuje
 * @param  errorCode promenna, ve ktere se vraci chybovy kod funkce
 * @return           string hodnota po prevodu
 */
char *StrVal(void *data, char typ, int *errorCode) {
    char *result = NULL;
    *errorCode = RESULT_OK;
    switch (typ) {
//    case INTR_VAR_NULL:
    //  case LEX_NULL:
    /*if (!(result = malloc(1 * sizeof(char)))) {
        *errorCode = ERROR_INTERPRET;
        return NULL;
    }*/

//        result = "";
//        StackPush((void *)&result,&stringStack);
//        return result;
    case INTR_VAR_BOOL:
    case LEX_BOOL:
        if ((*(char *) data) == true) {
            if (!(result = malloc(2 * sizeof(char)))) {
                *errorCode = ERROR_INTERPRET;
                return NULL;
            };
            result = "1";
        } else {
            if (!(result = malloc(1 * sizeof(char)))) {
                *errorCode = ERROR_INTERPRET;
                return NULL;
            };
            result = "";
        }
        StackPush((void *)&result,&stringStack);
        return result;
        break;
    case INTR_VAR_INT:
    case LEX_INT:
        if (!(result = malloc(60 * sizeof(char)))) {
            *errorCode = ERROR_INTERPRET;
            return NULL;
        }
        ;
        sprintf(result, "%d", *(int *) data);
        StackPush((void *)&result,&stringStack);
        return result;
        break;
    case INTR_VAR_DOUBLE:
    case LEX_DOUBLE:
        if (!(result = malloc(60 * sizeof(char)))) {
            *errorCode = ERROR_INTERPRET;
            return NULL;
        }
        ;
        sprintf(result, "%g", *(double *) data);
        StackPush((void *)&result,&stringStack);
        return result;
        break;
    case INTR_VAR_STRING:
    case LEX_STRING:
        return (char *) data;
    default:
        return "";
    }
    return NULL;
}

/**
 * Funkce pro tisknuti retezcu ze zasobniku
 * @param  varStack zasobnik promennych
 * @param  parCount pocet promennych, ktery se ma nacist ze zasobniku
 * @return          pocet vytisknutych retezcu
 */
int PutString(TOperPtrContStack *varStack, int *parCount) {
    int res = 0;
    TOperant *currentOperant;
    while (*parCount > 0) {

        TCSPop(*varStack, currentOperant, TOperPtr);
//        currentOperant = (TOperant *) StackPop(varStack);
        (*parCount)--;

        int type = currentOperant->ptr->type;
        if (type == LEX_INT || type == INTR_VAR_INT)
            printf("%d", currentOperant->ptr->data.iValue);
        else if (type == LEX_DOUBLE || type == INTR_VAR_DOUBLE)
            printf("%g", currentOperant->ptr->data.dValue);
        else if (type == LEX_STRING || type == INTR_VAR_STRING)
            printf("%s", currentOperant->ptr->data.sValue);
        else if (type == LEX_BOOL || type == INTR_VAR_BOOL)
            printf("%s", ((currentOperant->ptr->data.iValue == 1) ? "1" : ""));

        res++;
    }
    return res;
}

/**
 * Funkce pro ziskani podretezce z retezce
 * @param  inputString vstupni retezec
 * @param  start       index znaku na zacatku podretezce
 * @param  end         index znaku na konci podretezce
 * @param  errorCode   promenna, ve ktere se vraci chybovy kod funkce
 * @return             podretezec
 */
char *GetSubString(char *inputString, int start, int end, int *errorCode) {

    *errorCode = RESULT_OK;
    if ((start < 0) || (end < 0) || (start > end)
            || ((unsigned int) start >= strlen(inputString))
            || ((unsigned int) end > strlen(inputString))) {
        // ERROR13
        *errorCode = ERROR_SEM_RUNT;
        return NULL;
    }
    char *result = NULL;
    int size = end - start + 1;

    if ((result = malloc(size * sizeof(char))) == NULL) {
        *errorCode = ERROR_INTERPRET;
    }
    inputString += start;
    strncpy(result, inputString, size);
    result[size - 1] = '\0';
    StackPush((void *)&result,&stringStack);
    return result;
}

/**
 * Funkce pro realokaci retezce
 * @param  inputString vstupni retezec
 * @param  size        velikost pameti alokovane pro vstupni retezec
 * @param  errorCode   promenna, ve ktere se vraci chybovy kod funkce
 * @return             retezec naalokovany do 2x vetsiho prostoru
 */
char *ReallocString(char *inputString, int size, int *errorCode) {
    *errorCode = RESULT_OK;
    char *result = NULL;
    if ((result = malloc((size * 2 + 1) * sizeof(char))) == NULL) {
        *errorCode = ERROR_INTERPRET;
        return inputString;
    }
    strncpy(result, inputString, size);
    free(inputString);
    return result;
}

/* char *TrimStringMem(char *inputString)
 {
 char *result=NULL;
 int i=0;
 if((result=malloc((strlen(inputString)+1)*sizeof(char)))==NULL)
 {
 *errorCode=ERROR_INTERPRET;
 }
 for(i=0;(unsigned int) i<strlen(inputString);i++)
 {
 result[i]=inputString[i];
 }
 free(inputString);
 result[i]='\0';
 return result;
 }
 */

/**
 * Funkce pro nacteni retezce
 * @param  errorCode promenna, ve ktere se vraci chybovy kod funkce
 * @return           nacteny retezec
 */
char *GetString(int *errorCode) {
    *errorCode = RESULT_OK;
    int size = 64;
    char currentCharacter = 0;
    int index = 0;
    char *result = NULL;
    if ((result = malloc((size + 1) * sizeof(char))) == NULL) {
        *errorCode = ERROR_INTERPRET;
    }
    while ((currentCharacter = getchar()) != EOF) {
        if (currentCharacter != '\n') {
            result[index] = currentCharacter;
            if (index == size - 1) { // realokace retezce pokud jsme dosahli konce alokovane pameti
                result = ReallocString(result, size, errorCode);
                if (*errorCode == ERROR_INTERPRET) {
                    free(result);
                    return NULL;
                }
                size = size * 2;
            }
            index++;
        } else {
            break;
        }
    }
    result[index] = '\0';
    // result=TrimStringMem(result);
    StackPush((void *)&result,&stringStack);
    return result;
}

int calculateOffset(char c, char *pattern) {
    int i;
    int patternLength = strlen(pattern);
    for (i = 0; i < patternLength; i++) {
        if (pattern[patternLength - 1 - i] == c) {
            return i;
        }
    }
    return patternLength;
}

/**
 * Boyer-Mooreovo vyhledavani
 * @param  inputString vstupni retezec, ve kterem vyhledavame
 * @param  pattern     vzor, ktery vyhledavame
 * @return             index zacatku nalezeneho podretezce
 */
int FindSubString(char *inputString, char *pattern) {
    int offset;
    unsigned int patternLength = strlen(pattern);
    unsigned int i = 0;

    int patternTable[256];
    offset = (patternLength == 1) ? patternLength : patternLength - 1;
    for (i = 0; i < 256; i++) {
        patternTable[i] = offset;
    }
    offset = 0;
    for (i = 0; i < patternLength; i++) {
        if ((unsigned int) patternTable[(int) pattern[patternLength - 1 - i]] == patternLength-1) {
            patternTable[(int) pattern[patternLength - 1 - i]] = calculateOffset(pattern[patternLength - 1 - i], pattern);
        }
    }
    while (1) {
        if (patternLength + offset > strlen(inputString))
            return -1;
        for (i = 0; i < patternLength; i++) {
            if (inputString[patternLength - 1 + offset - i]
                    != pattern[patternLength - 1 - i]) {
                offset += patternTable[(int) inputString[patternLength - 1  + offset - i]];
                break;
            }
        }
        if(i==patternLength) return offset;
    }
    return 0;
}
