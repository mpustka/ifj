//
// data_structures.h
//
// Datove struktury
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	6. 12. 2013
// Autor:		Martin Roncka (xronck00)

#ifndef _DATA_H_
#define _DATA_H_

typedef union data
{
	int iValue;
	double dValue;
	char *sValue;
}Udata;

typedef struct item
{
	int blok;
	int type;
	Udata data;

	char *id;
}TItem;

typedef struct htableItem
{
	int blok;
	int type;
	Udata data;
	char *id;

	struct htableItem *next;
}THTableItem;

typedef THTableItem *Tlist;

typedef struct operant
{
	int type;
	TItem *ptr;
	unsigned int data;
}TOperant;

typedef struct instr
{
	int instr;
	TOperant operA;
	TOperant operB;
	TOperant operC;
	struct instr *next;
	struct instr *previous;
}TInstr;

typedef struct codeStack
{
	TInstr *stack;
}TCodeStack;

typedef struct tmp
{
	int blok;
	int type;
	Udata data;

//	struct tmp *next;
}TTmp;

typedef struct label
{
	unsigned int label;
	TInstr *ptr;
	char *name;
	struct label *next;
}TLabel;

typedef struct callStack
{
	void *ptr;
} TcallStack;

typedef void * TPtr;

typedef TOperant * TOperPtr;

typedef struct store
{
	TItem *ptr;
	union {
		TTmp val;
		struct backStop{
			TInstr *retInstr;
			int toPop;
		}TBackStop;
	}U;
} TStore;

#endif
