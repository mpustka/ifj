//
// main.c
//
// Hlavni smycka programu
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	30. 9. 2013
// Autor:		Martin Roncka (xronck00) | Michal Pustka (xpustk00)

#include <stdio.h>
#include "parser.h"
#include "stack.h"
#include "interpret.h"
#include "error.h"
#include "ial.h"

/*
 * Funkce pro zachyceni chyby, vypis hlaseni, uzavreni souboru a uvolneni pameti, ukonci program s chybovym kodem error
 */
void ThrowError(char *msg, int error, FILE *fc, THtable *tableSymbols)
{
	fprintf(stderr, msg);

	if(fc != NULL)
		fclose(fc);

	if(tableSymbols != NULL)
		HtableFree(tableSymbols);

	FreeInstructions();
	FreeLabel();
	FreeTemp();
	exit(error);
}

int main(int argc, char *argv[])
{
	FILE *file = NULL;
	THtable *tableSymbols = NULL;
	TStack *stringStack = NULL;

	#ifdef DEBUG_PARSER
		printf("Debug parser mode... \n");
	#endif

	#ifdef DEBUG_INTERPRET
		printf("Debug interpret mode... \n");
	#endif

	if((tableSymbols = HtableInit(100)) == NULL)	//vytvoreni instance tabulky symbolu
		ThrowError("Internal error: Table symbols cannot initialize.\n", ERROR_INTERPRET, NULL, tableSymbols);

	if(argc < 2)
		 ThrowError("User error: Wrong count parameters.\n", ERROR_INTERPRET, NULL, tableSymbols);

	if ((file = fopen(argv[1], "r")) == NULL)				// podminka kontroluje zda byl zadan dostatecny pocet parametru a zda se podarilo otevrit soubor
		ThrowError("Internal error: File cannot open.\n", ERROR_INTERPRET, NULL, tableSymbols);	// v pripade chyby ukoncime program a vratime chybu

	int retVal = 0;													// promenna pro ulozeni navratoveho hodnoty volanych funkci

	if ((retVal = CodeInit()) == 0)
	{
		#if DEBUG_PARSER | DEBUG_INTERPRET
				printf("\nInicializace pameti pro kod probehla vporadku..\n");
		#endif
	}


	if ((retVal = PredectiveSyntaxAnalysis(file, tableSymbols)) == RESULT_OK)
	{
		#if DEBUG_PARSER | DEBUG_INTERPRET
				printf("\nSyntakticka analyza probehla vporadku..\n\n");
		#endif
	}
	else
	{
		if(retVal == ERROR_LEX)
		{
			ThrowError("Lexical error: unexpected token\n", retVal, file, tableSymbols);
		}
		else
		{
			ThrowError("", retVal, file, tableSymbols);
		}
	}

	if ((retVal = Interpret()) == RESULT_OK)
	{
		#if DEBUG_PARSER | DEBUG_INTERPRET
				printf("\nInterpretace probehla vporadku..\n");
		#endif
	}

	if (file != NULL)
		fclose(file);

	if(tableSymbols != NULL)
		HtableFree(tableSymbols);
	
	if(stringStack != NULL) {
		FreeStack(stringStack);
	}
	FreeInstructions();
	FreeLabel();
	FreeTemp();

	return retVal;
}


