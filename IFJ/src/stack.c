//
// stack.c
//
// Datova struktura zasobniku
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	15. 10. 2013
// Autor:		Martin Roncka (xronck00) | Michal Pustka (xpustk00)

#include "stack.h"
#include <stdio.h>

bool StackPush(void **data, TStack **stack)
{
	TStack *item;

	if((item = (TStack*)malloc(sizeof(TStack))) == NULL)
		return false; //alokace selhala

	item->data = data;
	item->next = *stack; //ukazatel na puvodni prvni prvek

	*stack = item;

	return true;
}

void** StackPop(TStack **stack)
{
	if(*stack == NULL)
		return NULL;

	TStack *item = *stack;
	*stack = item->next; //novy vrchol zasovniku

	void **data = item->data;
	free(item); //uvolni z pameti

	return data;
}

void FreeStack(TStack *stack)
{
	while(stack != NULL)
		Pop(stack);
}

DEF_TCSIncrement(TOperPtr)

DEF_TCSDecrement(TOperPtr)
