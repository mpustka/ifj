//
// interpret.h
//
// Ukladani instrukci a interpretace
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	5. 10. 2013
// Autor:		Martin Roncka (xronck00)
//

#ifndef INTERPRET_H_
#define INTERPRET_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ial.h"
#include "error.h"
#include "stack.h"
#include "scanner.h"
#include "embedded.h"

/* Konstanty pro urceni velikosti poli pro blokove alokace */
#define AUX_SIZE 10
#define LAB_SIZE 10
#define MEM_SIZE 10

/* Hexadecimalni vyjadreni hodnoty potrebne pro maskovani promennych a urceni typu */
#define GET_VAR	0xF
#define GET_LEX	0x10

/* Ridici a skokove instrukce */
#define LABEL	0
#define MOVE	1
#define JMP		2
#define JMPN	3

#define CALL	4
#define RET		5

#define PUSH	6
#define POP		7

/* Aritmeticke instrukce */
#define MUL		8
#define DIV		9
#define ADD		10
#define SUB		11

/* Instrukce nad retezci */
#define CON		12


/* Relacni */
#define ISBE	13
#define ISSE	14

#define ISSM	15
#define ISBI	16

#define ISDIF	17
#define SAME	18

/* Typy operandu */
#define OP_NONE 	-1
#define OP_LABEL 	0
#define OP_TITEM	1
#define OP_VARTEMP	2
#define OP_TINSTR	3

/* Dekalrace funkci */
int Interpret();
int CodeInit();
void FreeInstructions();
void FreeLabel();
void FreeTemp();
void AddInstruction(int offset, TInstr *instruction, int *errorCode);
void ChangeInstructionPtr(int offset);


#endif /* INTERPRET_H_ */

