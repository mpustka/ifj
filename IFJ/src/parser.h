//
// parser.h
//
// Syntakticka analyza
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	5. 10. 2013
// Autor:		Michal Pustka (xpustk00)


#ifndef PARSER_H_
#define PARSER_H_

#include <stdbool.h>
#include <stdio.h>
#include "scanner.h"
#include "error.h"
#include "stack.h"
#include "ial.h"
#include "generator_instructions.h"

//Neterminaly
#define	PROG 					80
#define	STATEMENTS_LIST 		81
#define	STATEMENT 				82
#define	PRE_EXPRESSION 			84
#define	ARGUMENTS_LIST 			85
#define	ARGUMENT 				86
#define	ARGUMENT_NEXT 			87
#define	PARAMS_LIST 			88
#define	PARAM 					89
#define	PARAM_NEXT 				90
#define	EXPRESSION 				91
#define CODE					92
#define ELSE					93
#define	FOR1	 				94
#define FOR2					95
#define FOR3					96

//hlavni funkce
int PredectiveSyntaxAnalysis(FILE *file, THtable *tableSymbols);
int PrecedenceSyntaxAnalysis(FILE *file, TToken *token, THtable *tableSymbols, char **bufferLex);

//fuknce pro obsluhu zasobniku
void FreeStackExpression(TStack *stack);
void FreeStackBlocks(TStack *stack);
void PopExpression(TStack **stack);

//pomocne funkce
bool IsExpression(TStack *stack);
bool IsOperator(TStack *stack);

char* GetStringByConstant(int constant);
int CheckToken(TToken *token);
TRuleString* GetTopTerminal(TStack *stack);
char GetCharFromPrecedenceTable(int stackTerminal, int tokenTerminal, TRuleString **ptrString);
TRuleString* GetRuleByLLTable(int nonTerminal, int terminal, int *size, int *index);

#endif /* PARSER_H_ */
