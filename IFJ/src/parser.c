//
// parser.c
//
// Syntakticka analyza
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	5. 10. 2013
// Autor:		Michal Pustka (xpustk00)
//

#include "parser.h"

// Jmeno:					PredectiveSyntaxAnalysis
// Paremetr tableSymbols	ukazatel na tabulku symbolu
// Parametr file:			ukazatel na otevreny soubor
// Popis:					Predektivni syntakticka analyza (zhora-dolu), vyhodnocuje spravnost syntaxe pro tok programu
//							napr. ridici konstrukce programu. Analyza probiha pomoci LL-tabulky,
//							ktera obsahuje gramaticka pravidla.
// Nav. hodnota:			pokud je syntaxe spravna vraci RESULT_OK, pokud selze alokace vraci ERROR_INTERPRET,
//							pokud je syntakticka chyba v toku programu nebo ve vyrazu vraci ERROR_SA
int PredectiveSyntaxAnalysis(FILE *file, THtable *tableSymbols)
{
	TStack *stack = NULL; //zasobnik, ze se budou ukladat terminaly a neterminaly
	TRuleString start = {.isTerminal = false, .value = PROG}; //startujici neterminal, pro LL gramatiku
	TToken token; //Struktura tokenu, do ktere bude LA ukladat token
	int errorCode = RESULT_OK; //promenna do ktere se bude ukladat navratova hodnota
	int retLex = 0; //pomocna promenna pro ukladani navratove hodnoty z lexiklani analyzy
	char *buffer = NULL; //buffer pro lexikalni analyzator
	int length = 0; //velikost slova v bufferu LA
	TItem *item = NULL; //pomocny ukazatel pro zapis do TB
	bool stop = true; // pomocna promenna pro zastaveni cyklu

	//alokuje buffer pro LA
	if((buffer = malloc(sizeof(char)*MAX_BUFFER)) == NULL)
		return ERROR_INTERPRET;

	//Do tabulky vlozime aktualni blok: hlavni blok programu
	if(!AddBlock(tableSymbols, BLOCK_MAIN, 0, 0))
		return ERROR_INTERPRET;

	Push(&start, stack); //na zasobniku je startovaci symbol

	if((retLex = GetNextToken(file, &token, tableSymbols, &buffer, &length)) != RESULT_OK) //zavola LA, ktery varati pri uspechu token, jinak chybu
	{
		errorCode = retLex;
		stop = false;
	}

	int index = 0; //promenna do ktere se uklada index pravidla

	while(stop) //nekonecna smycka
	{
		if(stack == NULL) break; //ukoncujici podminka, pokud na zasobniku neni zaden terminal a neterminal

		//novy identifikator funkce, potreba zapsat do tabulky symbolu
		//pro pravidlo 4. <CODE> → function fun_id (<PARAMS-LIST>) { <STATEMENTS-LIST> } <CODE>
		if(index == 4 && token.type == LEX_FUN_ID)
		{
			item = NULL;
			//pokud je identifikator, id nektere z vestavenych funkci vraci error
			if(CheckEmbedded(buffer, length) != -1)
			{
				fprintf(stderr, "Semantic error: embedded is redefined.\n");
				errorCode = ERROR_SEM_NFUN;
				break; //prerus cyklus
			}

			//vlozi novy identifikator do tabulky symbolu
			item = HtableInsert(token.type, buffer, length, tableSymbols, &errorCode);

			if(errorCode != RESULT_OK)
				break;

			if(item == NULL || item->data.iValue != 0)
			{
				fprintf(stderr, "Semantic error: function is redefined.\n");
				errorCode = ERROR_SEM_NFUN;
				break; //prerus cyklus
			}

			//vrati syntakticke analyze prislysny Titem z tabulky symbolu
			token.attr = item;

			//predej pravido generatoru instrukci, ktery ma definovanou sematiku pro dane pravidlo
			if(!GenerateInstructions(tableSymbols, COMMAND_DECLAREFUN, token.attr, 0, 0, false, &errorCode))
				break; //prerus cyklus
		}

		TRuleString *head = (TRuleString *)Pop(stack); //ziskej vrchol zasobniku a odstran ho ze zasobniku

		if(head == NULL) //jina chyba
		{
			fprintf(stderr, "Syntax error: unexpected '%s'.\n", GetStringByConstant(token.type));
			errorCode = ERROR_SA; //nahrej navratovou hodnotu
			break; //prerus cyklus
		}else if(head->isTerminal) { //navrcholu zasobniku je terminal
			int terminal = head->value; //uloz hodnotu terminalu

			if(terminal == token.type) //pokud je terminal stejny jako na vstupni pasce (tokenu)
			{
				if((retLex = GetNextToken(file, &token, tableSymbols, &buffer, &length)) != RESULT_OK) //zavolej LA, ziskej dalsi token
				{
					errorCode = retLex; 					//nahrej navratovou hodnotu
					break; 									//prerus cyklus
				}
			}else if(terminal == SA_EPSILON){ 				//terminal je roven Epsilonu (prazny retezec)

			}else{
				fprintf(stderr, "Syntax error: unexpected '%s'.\n", GetStringByConstant(token.type));
				errorCode = ERROR_SA; 						//syntakticka chyba... na vstupni pasce je neocekavany terminal
				break; 										//prerus cyklus
			}

		}else{ 												//na vrcholu zasobniku je neterminal
			int nonTerminal = head->value;					//nahraj hodnotu neterminalu
			int size = 0; 									//promenna, kde bude ukladat kolik ma dane pravidlo terminalu a neterminalu

			//ziskej pravidlo z LL tabulky
			TRuleString *rule = GetRuleByLLTable(nonTerminal, token.type, &size, &index);

			//novy identifikator promenne, potreba zapsat do tabulky symbolu
			//Pro pravidla:
			// 18 <STATEMENT> → id = <PRE-EXPRESSION>;
			// 15 <FOR1> → id = <EXPRESSION>
			// 25 <PARAM> → id
			// 17 <FOR3> → id = <EXPRESSION>
			if((index == 18 || index == 15 || index == 25|| index == 17) && token.type == LEX_ID)
			{
				item = HtableInsert(token.type, buffer, length, tableSymbols, &errorCode);

				if(errorCode != RESULT_OK)
					break;

				token.attr = item;
			}

			//predej pravido generatoru instrukci, ktery ma definovanou sematiku pro dane pravidlo
			if(rule != NULL && !GenerateInstructions(tableSymbols, index, token.attr, 0, 0, false, &errorCode))
				break; //prerus cyklus

			if(rule == NULL) //LL-tabulka nenalezla pravidlo
			{
				if(head->value != EXPRESSION) //neni ocekavan vyraz, nastava syntakticka chyba
				{
					fprintf(stderr, "Syntax error: unexpected '%s'.\n", GetStringByConstant(token.type));
					errorCode = ERROR_SA; //uloz navratovou hodnotu
					break; //prerus cyklus
				}

				int retVal = 0; // promenna pro ulozeni navratove hodnoty z precedecni analyzy (analyza vyrazu)
				if((retVal = PrecedenceSyntaxAnalysis(file, &token, tableSymbols, &buffer)) != 0) //vyraz, vyhodnocuje jina analyza.. predani
				{
					errorCode = retVal; //syntakticka chyba ve vyrazu
					break; //prerus cyklus
				}

				//zavola generator instrukci, preda informaci o prechodu z analyzy vyhodnocujici vyraz (zdola-nahoru)
				//analyzy vyhodnocujici tok programu (zhora-dolu)
				if(!GenerateInstructions(tableSymbols, COMMAND_JOIN, NULL, 0, 0, false, &errorCode))
					break; //prerus cyklus
			}

			for(int i = 0; i < size; i++) //pushni na zasobnik cele pravidlo (vsechny terminaly a neterminaly)
			{
				if(!Push(rule++, stack)) //pusni terminal ci neterminal na zasobnik
				{
					errorCode = ERROR_INTERPRET; //nezdarila se alokace v zasobniku
					break; //prerusi cyklus for
				}
			}

			//nastala chyba v cyklu for
			if(errorCode != RESULT_OK)
				break; //prerusi hlavni cyklus
		}
	}

	free(buffer); //uvolnim buffer pro LA
	FreeStack(stack); //uvolni vsechny terminaly a neterminaly v zasobniku
	FreeStackBlocks(tableSymbols->actualBlock); //uvolni zasobnik bloku
	tableSymbols->actualBlock = NULL;

	return errorCode; //vrat navratovou hodnotu
}

// Jmeno:					PrecedenceSyntaxAnalysis
// Paremetr tableSymbols	ukazatel na tabulku symbolu
// Parametr file:			ukazatel na otevreny soubor
// Parametr token:			ukazatel na token z predektivni syn. analyzy
// Paremetr bufferLex:		ukazatel na ukazatel na buffer pro lexikalni analyzator
// Popis:					Precedecni syntakticka analyza (zdola-nahoru), vyhodnocuje spravnost syntaxe pro vyraz.
// 							Analyza probiha pomoci precedecni tabulky, ktera obsahuje ridici znaky. Znak '>' uplatni
//							na terminaly a neterminaly dane gramaticke pravidlo a nahrad ho neterminalem (Expression).
//							Znak '<' a '=' prida na vrchol zasobniku novy token ze vstupni pasky. Pokud je znak '1',
//							nastala syntakticka chyba ve vyrazu.
// Nav. hodnota: 			pokud je syntaxe spravna vraci RESULT_OK, pokud selze alokace vraci ERROR_INTERPRET,
// 							pokud ve vyrazu vraci ERROR_SA.
int PrecedenceSyntaxAnalysis(FILE *file, TToken *token, THtable *tableSymbols, char **bufferLex)
{
	TStack *stack = NULL; //zasobnik, ze se budou ukladat terminaly a neterminaly
	int errorCode = RESULT_OK; //promenna do ktere se bude ukladat navratova hodnota
	int retLex = 0; //pomocna promenna pro ukladani navratove hodnoty z lexiklani analyzy
	bool empty = true; //promenna, ktera bude identifikovat, jestli je vyraz prazdny
	static TRuleString end = {.isTerminal = true, .value = SA_END_EXP}; //ukoncujici terminal
	TItem *buffer = NULL;
	int length = 0; //velikost slova v bufferu LA
	bool isSimply = true; //jedna-li se o jednoduchy vyraz napr. $var = 5;

	Push(&end, stack); //vloz ukoncujici terminal na zasobnik

	//pokud je token int, double... prelozi jako konstantu, pokud je token strednik
	//ci carka prelozi jako ukoncuji znak
	int terminalToken = CheckToken(token);

	while(1) //nekonecna smycka
	{
		TRuleString *terminal = NULL; //ukazatel na terminal

		if((terminal = GetTopTerminal(stack)) == NULL) //vrati terminal, ktery je nejvyse v zasobniku
		{
			fprintf(stderr, "Syntax error: unexpected '%s'.\n", GetStringByConstant(terminalToken));
			errorCode = ERROR_SA; //neocekavany znak
			break; //prerusi cyklus
		}

		TRuleString *pushTerminal = NULL; //slouzi k prelozeni tokenu na terminal, ukazuje na pamet pro terminal
		int terminalStack = terminal->value; //ulozi hodnotu terminalu, ktery byl nejvyse na zasobniku

		//na vstupni pasce je ukoncovaci znak, nejvyssi terminal je dolar
		//nema smysl pokracovat dale
		if(terminalToken == SA_END_EXP && terminalToken == terminalStack) //ukoncujici podminka
			break; // prerusi cyklus

		//Zavola precedecni tabulku, ktera vrati ridici znak, pushTerminal prelozi token na terminal
		char controlChar = GetCharFromPrecedenceTable(terminalStack, terminalToken, &pushTerminal);

		if(controlChar == '>') //budeme uplatnovat nektere gramaticke pravidlo
		{
			//na vrcholu zasobniku je identifikator nebo konstanta
			if(terminalStack == LEX_ID || terminalStack == LEX_CON)
			{
				//<EXPRESSION> → id | con
				TRuleString *expression = (TRuleString *)malloc(sizeof(TRuleString));

				if(expression == NULL)
				{
					errorCode = ERROR_INTERPRET; //nepodarilo se alokovat pamet
					break; //prerusi cyklus
				}

				PopExpression(&stack); //odstran z vrcholu zasobniku identidikator nebo konstatnu

				//novy neterminal typu expression
				expression->isTerminal = false;
				expression->value = EXPRESSION;
				expression->attr = 0;
				expression->item = buffer;

				if(!Push(expression, stack)) //vloz novy neterminal na vrchol zasobniku
				{
					errorCode = ERROR_INTERPRET; //nezdarila se alokace
					break; //prerusi cyklus
				}

			}else{ //navrcholu zasobniku je bud terminal operator nebo zavorka

				isSimply = false;

				//na zasobniku musi byt, nejmene tri terminaly nebo neterminaly
				if(stack->next == NULL || stack->next->next == NULL)
				{
					fprintf(stderr, "Syntax error: unexpected '%s'.\n", GetStringByConstant(terminalStack));
					errorCode = ERROR_SA; //chyba ve vyrazu
					break; //prerusi cyklus
				}

				TRuleString *expression =  NULL; // ukazatel na novy neterminal typu expression

				if(terminalStack == LEX_RBRACKET) //na vrchlou zasobniku je prava zavorka
				{
					int rb = ((TRuleString *)stack->data)->value; //hodnota, prava zavorka
					int lb = ((TRuleString *)stack->next->next->data)->value; //hodnota, leva zavorka

					//proveri jest-li vyhovuje tomu to pravidlu: <EXPRESSION> → (<EXPRESSION>)
					if(rb != LEX_RBRACKET || !IsExpression(stack->next) || lb != LEX_LBRACKET)
					{
						fprintf(stderr, "Syntax error: unexpected '%s'.\n", GetStringByConstant(terminalStack));
						errorCode = ERROR_SA; //nevyhovuje vyse zminenemu pravidlu, chyba ve vyrazu
						break; //prerusi cyklus
					}

					PopExpression(&stack); // odstarani z vrcholu zasobniku ')'
					expression = (TRuleString *)Pop(stack); // odstarani z vrcholu zasobniku <EXPRESSION>
					PopExpression(&stack); // odstarani z vrcholu zasobniku '('

				}else{ //na vrcholu zasobniku je operator

					//proveri jest-li vyhovuje tomu to pravidlu: <EXPRESSION> → <EXPRESSION> operator <EXPRESSION>
					if(!IsExpression(stack) || !IsOperator(stack->next) || !IsExpression(stack->next->next))
					{
						fprintf(stderr, "Syntax error: unexpected '%s'.\n", GetStringByConstant(terminalStack));
						errorCode = ERROR_SA; //nevyhovuje vyse zminenemu pravidlu, chyba ve vyrazu
						break; //prerusi cyklus
					}

					//V atributu neterminalu jsou ulozeny mezi-promenne, ktere budou slouzit interpretu
					int operator = ((TRuleString *)stack->next->data)->value; //hodnota, operator
					TRuleString opA = *((TRuleString *)stack->data);
					TRuleString opB = *((TRuleString *)stack->next->next->data);

					PopExpression(&stack); //odstarani z vrcholu <EXPRESSION>
					PopExpression(&stack); //odstarani z vrcholu operator
					PopExpression(&stack); //odstarani z vrcholu <EXPRESSION>

					//***optimalizace***
					TRuleString *lastTerm = GetTopTerminal(stack); //nejvysi terminal na zasobniku
					bool last = false; //prepokladame, ze operace ve vyrazu neni posledni

					//na zasobniku je terminal a zaroven je terminal ukokoncujici
					if(lastTerm != NULL && lastTerm->value == SA_END_EXP)
					{
						//ziskame ridici znak z precedecni tabulky
						char tempControl = GetCharFromPrecedenceTable(lastTerm->value, terminalToken, &pushTerminal);

						//token je ukoncujici znak nebo (terminal neni legalni ve vyrazu nebo prazdny ridici znak '0' zarovne token je prava zavorka)
						if(lastTerm->value == terminalToken || ((tempControl == 1 || tempControl == '0') && terminalToken == LEX_RBRACKET))
							last = true;
					}
					//***

					//preda informaci o tom, ze operator a operandy byly nahrazeny netermnaly, generator
					//instrukci vygeneruje instrukci na zaklade sematiky. Jednez parametru je hodnota operatoru,
					//ke ktere je pricteno OFFSET_OP, proto abychom ho prevedli na konstantu typu LEX_XXXX
					//Vraci hodnotu mezi-promenne, ktera bude prirazena danemu neterminalu.
					//Hodnota mezi-promenne slouzi nasledne interpretu.
					int tempVar = GenerateInstructions(tableSymbols, operator + OFFSET_OP, NULL, &opA, &opB, last, &errorCode);

					if(errorCode != RESULT_OK)
						break; //prus cyklus

					expression = (TRuleString *)malloc(sizeof(TRuleString)); //alokuje misto pro novy neterminal

					if(expression == NULL)
					{
						errorCode = ERROR_INTERPRET; //nepodarilo se alokovat pamet
						break; //prerusi cyklus
					}

					//novy neterminal typu expression
					expression->isTerminal = false;
					expression->value = EXPRESSION;
					expression->attr = tempVar; //hodnota mezi-promenne
					expression->item = NULL;
				}

				if(!Push(expression, stack)) //vlozi novy neterminal na vrchol zasobniku
				{
					errorCode = ERROR_INTERPRET; //chyba ve vyrazu
					break; //prerusi cyklus
				}
			}

		}else if(controlChar == '<' || controlChar == '='){

			if(!Push(pushTerminal, stack)) //vlozi prelozeny token na vrchol zasobniku
			{
				errorCode = ERROR_INTERPRET; //chyba alokace pameti
				break; //prerusi cyklus
			}

			buffer = token->attr;

			//nacte token, pro dalsi iteraci
			if((retLex = GetNextToken(file, token, tableSymbols, bufferLex, &length)) != RESULT_OK)
			{
				errorCode = retLex; //lexikalni chyba
				break; //prerusi cyklus
			}

			//pokud je token int, double... prelozi jako konstantu, pokud je token strednik
			//ci carka prelozi jako ukoncuji znak
			terminalToken = CheckToken(token);

		}else{//precedecni tabulka nemohla najit znak ze vstupni pasky (tokenu), nebo nastala chyba

			//Pokud je na vrcholu zasobniku ukoncujici terminal, na vstupni pasce
			//(tokenu) je prava zavorka, jedna se o konec podminky, cyklu nebo funkce.
			//Ukoncujeme analyzu vyrazu.
			if(terminalStack == SA_END_EXP && terminalToken == LEX_RBRACKET)
				break; //prerusi cyklus

			if(terminalStack == LEX_LBRACKET) //token je prava zavorka
			{
				fprintf(stderr, "Syntax error: missing '%s'.\n", GetStringByConstant(LEX_RBRACKET));
				errorCode = ERROR_SA; //chybi leva zavorka
				break; //prerusi cyklus
			}

			fprintf(stderr, "Syntax error: unexpected '%s'.\n", GetStringByConstant(terminalToken));
			errorCode = ERROR_SA; //jina syntakticka chyba ve vyrazu
			break; //prerusi cyklus
		}

		empty = false; //vyraz neni prazdy
	}

	if(errorCode == RESULT_OK && IsExpression(stack) && isSimply) //jednoduchy vyraz napr.: $var = 5;
		GenerateInstructions(tableSymbols, TERMINAL_TO_EXPRESSION, buffer, 0, 0, false, &errorCode);

	FreeStackExpression(stack); //uvolni zasobnik

	//vyraz je prazdny napr: '$var=;', 'if()'
	if(errorCode == RESULT_OK && empty)
	{
		fprintf(stderr, "Syntax error: unexpected '%s'.\n", GetStringByConstant(token->type));
		return ERROR_SA;
	}

	return errorCode; //vrati navratovou hodnotu
}

// Jmeno: 			FreeStackBlocks
// Parametr stack: 	ukazatel na vrchol zasobniku
// Popis: 			Uvolni zasobnik a vsechny bloky
// Nav. hodnota: 	Zadna.
void FreeStackBlocks(TStack *stack)
{
	while(stack != NULL) //dokud neni vrchol zasobniku NULL "popuj".
	{
		TBlock *block = (TBlock*)StackPop(&stack);
		free(block);
	}
}

// Jmeno: 			FreeStackExpression
// Parametr stack: 	ukazatel na vrchol zasobniku
// Popis: 			Uvolni zasobnik, musime volat specialni funkci PopExpression, protoze
//					nektere prvky na zasobniku jsou alokovane dynamicky.
// Nav. hodnota: 	Zadna.
void FreeStackExpression(TStack *stack)
{
	while(stack != NULL) //dokud neni vrchol zasobniku NULL "popuj".
		PopExpression(&stack);
}

// Jmeno: 			PopExpression
// Parametr stack: 	ukazatel na ukazatel na vrchol zasobniku
// Popis: 			Uvolni dynamicky alokovane prvky typu expression
// Nav. hodnota: 	Zadna.
void PopExpression(TStack **stack)
{
	TRuleString *item = (TRuleString *)Pop(*stack);

	//Zkotroluje jestli je prvek neterminal typu expression
	if(item->isTerminal != false || item->value != EXPRESSION)
		return;

	free(item);
}

// Jmeno: 			IsOperator
// Parametr stack: 	ukazatel na prvek v zasobniku
// Popis: 			Zkontroluje jestli je prvek na zasobniku operator.
// Nav. hodnota: 	Pokud je prvek operator vraci hodnotu true jinak false.
bool IsOperator(TStack *stack)
{
	TRuleString *terminal = (TRuleString *)stack->data;
	int type = terminal->value;

	if
	(
		terminal->isTerminal == true //je terminal
		&& type != LEX_LBRACKET //neni leva zavorka
		&& type != LEX_RBRACKET //neni prava zavorka
		&& type != LEX_ID // neni indentifikator
		&& type != LEX_FUN_ID // neni identifikator funkce
		&& type != SA_END_EXP // neni ukoncujici znak
		&& type != LEX_CON  //neni konstanta
	)
		return true;

	return false;
}

// Jmeno: 			IsExpression
// Parametr stack: 	ukazatel na prvek v zasobniku
// Popis: 			Zkontroluje jestli je prvek na zasobniku neterminal typu expression.
// Nav. hodnota: 	Pokud je prvek neterminal typu expression vraci hodnotu true jinak false.
bool IsExpression(TStack *stack)
{
	TRuleString *nonTerminal = (TRuleString *)stack->data;
	int type = nonTerminal->value;

	if(nonTerminal->isTerminal == false && type == EXPRESSION)
		return true;

	return false;
}

// Jmeno: 			CheckToken
// Parametr token: 	ukazatel na token ze vstupni pasky
// Popis: 			Pokud je token strednik ci carka prelozi jako ukoncuji znak.
//					Jinak nepreklada nechava puvodni token.
// Nav. hodnota: 	Vraci hodnotu prelozeneho tokenu.
int CheckToken(TToken *token)
{
	int returnToken = 0;

	if(token->type == LEX_SEMI /*|| token->type == LEX_COMMA*/)
	{
		returnToken = SA_END_EXP; //nahradi ukoncovaci znak za dolar
	}else{
		returnToken = token->type; //nechava puvodni nezmeneny token
	}

	return returnToken;
}

// Jmeno: 			GetTopTerminal
// Parametr stack: 	ukazatel na vrchol zasobniku
// Popis: 			Nalezne na zasobniku nejblizsi terminal od vrcholu zasobniku.
// Nav. hodnota: 	Vraci terminal, pokud nenalezne vraci NULL.
TRuleString* GetTopTerminal(TStack *stack)
{
	TStack *item = stack;

	if(stack == NULL) //zasobnik je prazdny
		return NULL;

	//Hleda odkud nenarazi na terminal
	while(!((TRuleString *)item->data)->isTerminal && (item = item->next) != NULL);
	//<--- zde cyklus nepokracuje --->
	return (TRuleString *)item->data;
}

// Jmeno: 					GetCharFromPrecedenceTable
// Parametr stackTerminal: 	terminal na vrcholu zasobniku
// Parametr tokenTerminal: 	terminal na vstupni pasce (token)
// Parametr ptrString: 		ukazatel na ukazatel na pamet prelozeneho tokenu na TRuleString
// Popis: 					Nalezne ridici znak v precedecni tabulce.
// Nav. hodnota: 			Vraci ridici znak, pokud nenalezne v tabulce(rozsah mimo tabulku)
//							vraci '1'.
char GetCharFromPrecedenceTable(int stackTerminal, int tokenTerminal, TRuleString **ptrString)
{
	/*** Defince precedecni tabulky ***/

	//radky a sloupce v tabulce jsou v poradi, podle pole terminalu 'terminals'.
	static char precedenceTable[16][16] =
	{
		{'>',	'>',	'<',	'<',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'<',	'>',	'>',	'<',	'<'},
		{'>',	'>',	'<',	'<',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'<',	'>',	'>',	'<',	'<'},
		{'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'<',	'>',	'>',	'<',	'<'},
		{'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'<',	'>',	'>',	'<',	'<'},
		{'>',	'>',	'<',	'<',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'<',	'>',	'>',	'<',	'<'},
		{'<',	'<',	'<',	'<',	'<',	'>',	'>',	'>',	'>',	'>',	'>',	'<',	'>',	'>',	'<',	'<'},
		{'<',	'<',	'<',	'<',	'<',	'>',	'>',	'>',	'>',	'>',	'>',	'<',	'>',	'>',	'<',	'<'},
		{'<',	'<',	'<',	'<',	'<',	'>',	'>',	'>',	'>',	'>',	'>',	'<',	'>',	'>',	'<',	'<'},
		{'<',	'<',	'<',	'<',	'<',	'>',	'>',	'>',	'>',	'>',	'>',	'<',	'>',	'>',	'<',	'<'},
		{'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'>',	'>',	'<',	'>',	'>',	'<',	'<'},
		{'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'>',	'>',	'<',	'>',	'>',	'<',	'<'},
		{'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'=',	'0',	'<',	'<'},
		{'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'0',	'>',	'>',	'0',	'0'},
		{'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'<',	'0',	'0',	'<',	'<'},
		{'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'0',	'>',	'>',	'0',	'0'},
		{'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'>',	'0',	'>',	'>',	'0',	'0'}
	};

	/*** Konec ***/

	//Vsechny mozne terminaly, ktere se mohou vyskytovat ve vyrazech
	static TRuleString terminals[] =
	{
		{.isTerminal = true, .value = LEX_PLUS},
		{.isTerminal = true, .value = LEX_MINUS},
		{.isTerminal = true, .value = LEX_STAR},
		{.isTerminal = true, .value = LEX_SLASH},
		{.isTerminal = true, .value = LEX_DOT},
		{.isTerminal = true, .value = LEX_LESS},
		{.isTerminal = true, .value = LEX_GREATER},
		{.isTerminal = true, .value = LEX_LESS_EQ},
		{.isTerminal = true, .value = LEX_GREATER_EQ},
		{.isTerminal = true, .value = LEX_EQUAL},
		{.isTerminal = true, .value = LEX_NOTEQUAL},
		{.isTerminal = true, .value = LEX_LBRACKET},
		{.isTerminal = true, .value = LEX_RBRACKET},
		{.isTerminal = true, .value = SA_END_EXP},
		{.isTerminal = true, .value = LEX_CON},
		{.isTerminal = true, .value = LEX_ID}
	};

	//zkontroluje jesli nektery z terminalu neni mimo tabulku.
	if(stackTerminal < 0 || stackTerminal > 15  || tokenTerminal < 0 || tokenTerminal > 15)
		return 1;

	//preda ukazatel na pamet, kde je pozadovany terminal
	*ptrString = &terminals[tokenTerminal];

	//vraci nalezeny ridici znak
	return precedenceTable[stackTerminal][tokenTerminal];
}

// Jmeno: 					GetRuleByLLTable
// Parametr nonTerminal: 	neterminal na vrcholu zasobniku
// Parametr terminal: 		terminal na vstupni pasce (token)
// Parametr size: 			ukazatel na int, kde se preda velikost gramatickeho pravidla
// Parametr ruleIndex: 		ukazatel na int, kde se preda index gramatickeho pravidla
// Popis: 					Podle vstupu terminalu a neterminalu nalezne gramaticke pravidlo,
//							ktere se uplatni.
// Nav. hodnota: 			Vraci ukazatel na prvni terminal ci netermial gramatickeho pravidla.
TRuleString* GetRuleByLLTable(int nonTerminal, int terminal, int *size, int *ruleIndex)
{
	/*** Defince pravidel ***/

	//<PROG> → start_mark <CODE> eof
	static TRuleString rule1[] =
	{
		{.isTerminal = true, .value = LEX_EOF},
		{.isTerminal = false, .value = CODE},
		{.isTerminal = true, .value = LEX_START_MARK}
	};

	//<CODE> → <STATEMENT><CODE>
	static TRuleString rule2[] =
	{
		{.isTerminal = false, .value = CODE},
		{.isTerminal = false, .value = STATEMENT}
	};

	//<CODE> → ε
	//<ELSE> → ε
	//<STATEMENTS-LIST> → ε
	//<PARAMS-LIST> → ε
	//<ARGUMENTS-LIST> → ε
	//<ARGUMENT-NEXT> → ε
	static TRuleString ruleEpsilon[] =
	{
		{.isTerminal = true, .value = SA_EPSILON}
	};

	//<CODE> → function fun_id (<PARAMS-LIST>) { <STATEMENTS-LIST> } <CODE>
	static TRuleString rule4[] =
	{
		{.isTerminal = false, .value = CODE},
		{.isTerminal = true, .value = LEX_CRBRACKET},
		{.isTerminal = false, .value = STATEMENTS_LIST},
		{.isTerminal = true, .value = LEX_CLBRACKET},
		{.isTerminal = true, .value = LEX_RBRACKET},
		{.isTerminal = false, .value = PARAMS_LIST},
		{.isTerminal = true, .value = LEX_LBRACKET},
		{.isTerminal = true, .value = LEX_FUN_ID},
		{.isTerminal = true, .value = LEX_FUNC}
	};

	//<STATEMENTS-LIST> → <STATEMENT><STATEMENTS-LIST>
	static TRuleString rule5[] =
	{
		{.isTerminal = false, .value = STATEMENTS_LIST},
		{.isTerminal = false, .value = STATEMENT}
	};

	//<STATEMENT> → return <EXPRESSION>;
	static TRuleString rule7[] =
	{
		{.isTerminal = true, .value = LEX_SEMI},
		{.isTerminal = false, .value = EXPRESSION},
		{.isTerminal = true, .value = LEX_RETURN}
	};

	//<STATEMENT> → fun_id (<ARGUMENTS-LIST>);
	/*static TRuleString rule8[] =
	{
		{.isTerminal = true, .value = LEX_SEMI},
		{.isTerminal = true, .value = LEX_RBRACKET},
		{.isTerminal = false, .value = ARGUMENTS_LIST},
		{.isTerminal = true, .value = LEX_LBRACKET},
		{.isTerminal = true, .value = LEX_FUN_ID}
	};*/

	//<STATEMENT> → if (<EXPRESSION>) { <STATEMENTS-LIST-FUN> } <ELSE>
	static TRuleString rule9[] =
	{
		{.isTerminal = false, .value = ELSE},
		{.isTerminal = true, .value = LEX_CRBRACKET},
		{.isTerminal = false, .value = STATEMENTS_LIST},
		{.isTerminal = true, .value = LEX_CLBRACKET},
		{.isTerminal = true, .value = LEX_RBRACKET},
		{.isTerminal = false, .value = EXPRESSION},
		{.isTerminal = true, .value = LEX_LBRACKET},
		{.isTerminal = true, .value = LEX_IF}
	};

	//<ELSE> → else { <STATEMENTS-LIST> }
	static TRuleString rule11[] =
	{
		{.isTerminal = true, .value = LEX_CRBRACKET},
		{.isTerminal = false, .value = STATEMENTS_LIST},
		{.isTerminal = true, .value = LEX_CLBRACKET},
		{.isTerminal = true, .value = LEX_ELSE}
	};

	//<ELSE> → elsif ( <EXPRESSION> )  { <STATEMENTS-LIST> } <ELSE>
	static TRuleString rule12[] =
	{
		{.isTerminal = false, .value = ELSE},
		{.isTerminal = true, .value = LEX_CRBRACKET},
		{.isTerminal = false, .value = STATEMENTS_LIST},
		{.isTerminal = true, .value = LEX_CLBRACKET},
		{.isTerminal = true, .value = LEX_RBRACKET},
		{.isTerminal = false, .value = EXPRESSION},
		{.isTerminal = true, .value = LEX_LBRACKET},
		{.isTerminal = true, .value = LEX_ELSIF}
	};

	//<STATEMENT> → while( <EXPRESSION> ) { <STATEMENTS-LIST> }
	static TRuleString rule13[] =
	{
		{.isTerminal = true, .value = LEX_CRBRACKET},
		{.isTerminal = false, .value = STATEMENTS_LIST},
		{.isTerminal = true, .value = LEX_CLBRACKET},
		{.isTerminal = true, .value = LEX_RBRACKET},
		{.isTerminal = false, .value = EXPRESSION},
		{.isTerminal = true, .value = LEX_LBRACKET},
		{.isTerminal = true, .value = LEX_WHILE}
	};

	//<STATEMENT> → for(<FOR1>;<FOR2>;<FOR3>) { <STATEMENTS-LIST> }
	static TRuleString rule14[] =
	{
		{.isTerminal = true, .value = LEX_CRBRACKET},
		{.isTerminal = false, .value = STATEMENTS_LIST},
		{.isTerminal = true, .value = LEX_CLBRACKET},
		{.isTerminal = true, .value = LEX_RBRACKET},
		{.isTerminal = false, .value = FOR3},
		{.isTerminal = true, .value = LEX_SEMI},
		{.isTerminal = false, .value = FOR2},
		{.isTerminal = true, .value = LEX_SEMI},
		{.isTerminal = false, .value = FOR1},
		{.isTerminal = true, .value = LEX_LBRACKET},
		{.isTerminal = true, .value = LEX_FOR}
	};

	//<FOR1> → id = <EXPRESSION>
	static TRuleString rule15[] =
	{
		{.isTerminal = false, .value = EXPRESSION},
		{.isTerminal = true, .value = LEX_ASSIGN},
		{.isTerminal = true, .value = LEX_ID}
	};

	//<FOR2> → <EXPRESSION>
	static TRuleString rule16[] =
	{
		{.isTerminal = false, .value = EXPRESSION},
	};

	//<FOR3> → id = <EXPRESSION>
	static TRuleString rule17[] =
	{
		{.isTerminal = false, .value = EXPRESSION},
		{.isTerminal = true, .value = LEX_ASSIGN},
		{.isTerminal = true, .value = LEX_ID}
	};


	//<STATEMENT> → id = <PRE-EXPRESSION>;
	static TRuleString rule18[] =
	{
		{.isTerminal = true, .value = LEX_SEMI},
		{.isTerminal = false, .value = PRE_EXPRESSION},
		{.isTerminal = true, .value = LEX_ASSIGN},
		{.isTerminal = true, .value = LEX_ID}
	};

	//<STATEMENT> → break;
	static TRuleString rule19[] =
	{
		{.isTerminal = true, .value = LEX_SEMI},
		{.isTerminal = true, .value = LEX_BREAK}
	};

	//<STATEMENT> → continue;
	static TRuleString rule20[] =
	{
		{.isTerminal = true, .value = LEX_SEMI},
		{.isTerminal = true, .value = LEX_CONTINUE}
	};

	//<PRE-EXPRESSION> → <EXPRESSION>
	static TRuleString rule21[] =
	{
		{.isTerminal = false, .value = EXPRESSION},
	};

	//<PRE-EXPRESSION> → fun_id (<ARGUMENTS-LIST>)
	static TRuleString rule22[] =
	{
		{.isTerminal = true, .value = LEX_RBRACKET},
		{.isTerminal = false, .value = ARGUMENTS_LIST},
		{.isTerminal = true, .value = LEX_LBRACKET},
		{.isTerminal = true, .value = LEX_FUN_ID}
	};

	//<PARAMS-LIST> → <PARAM><PARAM-NEXT>
	static TRuleString rule24[] =
	{
		{.isTerminal = false, .value = PARAM_NEXT},
		{.isTerminal = false, .value = PARAM}
	};

	//<PARAM> → id
	static TRuleString rule25[] =
	{
		{.isTerminal = true, .value = LEX_ID}
	};

	//<PARAM-NEXT> → ,<PARAM><PARAM-NEXT>
	static TRuleString rule27[] =
	{
		{.isTerminal = false, .value = PARAM_NEXT},
		{.isTerminal = false, .value = PARAM},
		{.isTerminal = true, .value = LEX_COMMA}
	};

	//<ARGUMENTS-LIST> → <ARGUMENT><ARGUMENT-NEXT>
	static TRuleString rule29[] =
	{
		{.isTerminal = false, .value = ARGUMENT_NEXT},
		{.isTerminal = false, .value = ARGUMENT}
	};

	//<ARGUMENT> → id
	static TRuleString rule30[] =
	{
		{.isTerminal = true, .value = LEX_ID}
	};

	//<ARGUMENT> → con
	static TRuleString rule31[] =
	{
		{.isTerminal = true, .value = LEX_CON}
	};

	//<ARGUMENT-NEXT> → , <ARGUMENT><ARGUMENT-NEXT>
	static TRuleString rule33[] =
	{
		{.isTerminal = false, .value = ARGUMENT_NEXT},
		{.isTerminal = false, .value = ARGUMENT},
		{.isTerminal = true, .value = LEX_COMMA}
	};

	/*** Konec ***/

	/*** LL-Tabulka ***/

	//pomocne marko pro navraceni a predani potrebnych informacib
	#define ret(rule, index) { *size = sizeof(rule) / sizeof(TRuleString); *ruleIndex = index; return &rule[0]; }

	switch(nonTerminal)
	{
		case PROG:

			if(terminal == LEX_START_MARK)
				ret(rule1, 1);

			return NULL;

		break;

		case CODE:

			if
			(
					terminal == LEX_RETURN
				|| 	terminal == LEX_ID
				|| 	terminal == LEX_FUN_ID
				|| 	terminal == LEX_IF
				|| 	terminal == LEX_WHILE
				|| 	terminal == LEX_FOR
				|| 	terminal == LEX_BREAK
				|| 	terminal == LEX_CONTINUE
			)
				ret(rule2, 2);

			if(terminal == LEX_EOF)
				ret(ruleEpsilon, 3);

			if(terminal == LEX_FUNC)
				ret(rule4, 4);

			return NULL;

		break;

		case STATEMENTS_LIST:

			if
			(
					terminal == LEX_RETURN
				|| 	terminal == LEX_ID
				|| 	terminal == LEX_FUN_ID
				|| 	terminal == LEX_IF
				|| 	terminal == LEX_WHILE
				|| 	terminal == LEX_FOR
				|| 	terminal == LEX_BREAK
				|| 	terminal == LEX_CONTINUE
			)
				ret(rule5, 5);

			if(terminal == LEX_CRBRACKET)
				ret(ruleEpsilon, 6);

			return NULL;

		break;

		case STATEMENT:

			if(terminal == LEX_RETURN)
				ret(rule7, 7);

			if(terminal == LEX_ID)
				ret(rule18, 18);

		/*	if(terminal == LEX_FUN_ID)
				ret(rule8, 8);*/

			if(terminal == LEX_IF)
				ret(rule9, 9);

			if(terminal == LEX_WHILE)
				ret(rule13, 13);

			if(terminal == LEX_FOR)
				ret(rule14, 14);

			if(terminal == LEX_BREAK)
				ret(rule19, 19);

			if(terminal == LEX_CONTINUE)
				ret(rule20, 20);


			return NULL;

		break;

		case ELSE:

			if(
					terminal == LEX_EOF
				|| 	terminal == LEX_FUNC
				|| 	terminal == LEX_CRBRACKET
				||	terminal == LEX_RETURN
				|| 	terminal == LEX_ID
				|| 	terminal == LEX_FUN_ID
				|| 	terminal == LEX_IF
				|| 	terminal == LEX_WHILE
				|| 	terminal == LEX_FOR
				|| 	terminal == LEX_BREAK
				|| 	terminal == LEX_CONTINUE

			)
				ret(ruleEpsilon, 10);

			if(terminal == LEX_ELSE)
				ret(rule11, 11);

			if(terminal == LEX_ELSIF)
				ret(rule12, 12);

			return NULL;

		break;

		case FOR1:

			if(terminal == LEX_ID)
				ret(rule15, 15);

			if(terminal == LEX_SEMI)
				ret(ruleEpsilon, 34);

			return NULL;

		break;

		case FOR2:

			if(terminal == LEX_SEMI)
				ret(ruleEpsilon, 35);

			ret(rule16, 16);

		break;

		case FOR3:

			if(terminal == LEX_ID)
				ret(rule17, 17);

			if(terminal == LEX_RBRACKET)
				ret(ruleEpsilon, 36);

			return NULL;

		break;

		case PRE_EXPRESSION:

			if(terminal == LEX_FUN_ID)
				ret(rule22, 22);

			ret(rule21, 21);

		break;

		case ARGUMENTS_LIST:

			if(terminal == LEX_ID || terminal == LEX_CON)
				ret(rule29, 29);

			if(terminal == LEX_RBRACKET)
				ret(ruleEpsilon, 28);

			return NULL;

		break;

		case ARGUMENT:

			if(terminal == LEX_ID)
				ret(rule30, 30);

			if(terminal == LEX_CON)
				ret(rule31, 31);

			return NULL;

		break;

		case ARGUMENT_NEXT:

			if(terminal == LEX_RBRACKET)
				ret(ruleEpsilon, 32);

			if(terminal == LEX_COMMA)
				ret(rule33, 33);

			return NULL;

		break;

		case PARAMS_LIST:

			if(terminal == LEX_ID)
				ret(rule24, 24);

			if(terminal == LEX_RBRACKET)
				ret(ruleEpsilon, 23);

			return NULL;

		break;

		case PARAM:

			if(terminal == LEX_ID)
				ret(rule25, 25);

			return NULL;

		break;

		case PARAM_NEXT:

			if(terminal == LEX_RBRACKET)
				ret(ruleEpsilon, 26);

			if(terminal == LEX_COMMA)
				ret(rule27, 27);

			return NULL;

		break;
	}

	return NULL;

	/*** Konec ***/
}

// Jmeno: 				GetStringByConstant
// Parametr constatnt: 	ukazatel na token ze vstupni pasky
// Popis: 				Slozi pro preklad konstant na realne znaky resp. retezec, pouziva se v chybovem hlaseni.
// Nav. hodnota: 		Vraci znak respektive retezec.
char* GetStringByConstant(int constant)
{
	switch(constant)
	{
		case LEX_PLUS:
			return "+";
		case LEX_MINUS:
			return "-";
		case LEX_STAR:
			return "*";
		case LEX_SLASH:
			return "/";
		case LEX_DOT:
			return ".";
		case LEX_LESS:
			return "<";
		case LEX_GREATER:
			return ">";
		case LEX_LESS_EQ:
			return "<=";
		case LEX_GREATER_EQ:
			return ">=";
		case LEX_EQUAL:
			return "===";
		case LEX_NOTEQUAL:
			return "!==";
		case LEX_LBRACKET:
			return "(";
		case LEX_RBRACKET:
			return ")";
		case LEX_CLBRACKET:
			return "{";
		case LEX_CRBRACKET:
			return "}";
		case LEX_ASSIGN:
			return "=";
		case LEX_COMMA:
			return ",";
		case LEX_SEMI:
			return ";";
		case LEX_IF:
			return "if";
		case LEX_WHILE:
			return "while";
		case LEX_FUNC:
			return "function";
		case LEX_RETURN:
			return "return";
		case LEX_ID:
			return "variable ...";
		case LEX_FUN_ID:
			return "function id ...";
		case LEX_CON:
			return "constant";
	}

	return "symbol";
}
