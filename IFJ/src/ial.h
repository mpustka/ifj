//
// ial.h
//
// Tabulka symbolu, Mergesort
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	19. 10. 2013
// Autor:		Dominika Regeciova (xregec00) | Adam Ruzicka (xruzic43)


#ifndef IAL_H_
#define IAL_H_

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>
#include "stack.h"
#include "data_structures.h"


#define VALUE_SIZE 100000


// Struktura tabulky
typedef struct htable
{
    unsigned int size; // Velikost tabulky
    int actualMainBlock; // Aktulani hlavni blok (funkce, main).
    TStack *actualBlock; // Ukazatel na blok, ze ktereho budu ukladat tokeny
    Tlist array[]; // Ukazatel na data
}THtable;

typedef struct ruleString
{
	bool isTerminal;
	int value;			//hodnota terminal ci neterminalu
	unsigned int attr;	//atribut pro ukladani mezi-promennych
	TItem *item;	//ukazatel do tabulky symbolu
}TRuleString;


// Funkce

// Rozptylovaci funkce
unsigned int HashFunction (const char *element, unsigned int length, unsigned hTableSize);

// Inicializace tabulky
THtable * HtableInit (unsigned int size);

// Uvolneni pameti tabulky
void HtableFree (THtable *hash);

// Najdi prvek v tabulce
TItem * HtableFindItem(int sType, char *sValue, unsigned int length, THtable *hash);

// Vyhledani v tabulce
TItem * HtableLookUp (int sType, char *sValue, unsigned int length, unsigned int hashKey, THtable *hash);

// Vlozeni zaznamu do tabulky
TItem * HtableInsert (int sType, char *sValue, unsigned int length, THtable *hash, int *errorCode);

// Inicializace tabulky Value
int ValueInit();

// Funkce na zpracovani hodnoty tokenu
void ValueStore(int sType, char *sValue, unsigned int length, THTableItem *list, int *errorCode);

// Funkce na prevod z charu na double
double StringToDouble (char *number);

// Funkce na prevod z charu na integer
int StringToInt (char *number);

int BlockCheck(TStack *actual, unsigned int block, unsigned int tmp);

char *Mergesort(char * list,int *errCode);

#endif /* IAL_H_ */


