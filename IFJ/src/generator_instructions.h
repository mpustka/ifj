//
// generator_instructions.h
//
// Generator instrukci
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	20. 10. 2013
// Autor:		Michal Pustka (xpustk00)


#ifndef GENERATOR_INSTRUCTIONS_H_
#define GENERATOR_INSTRUCTIONS_H_

#include <stdbool.h>
#include "interpret.h"
#include "stack.h"
#include "ial.h"

//typy bloku
#define	BLOCK_MAIN		-1
#define	BLOCK_IF		0
#define	BLOCK_ELSE		1
#define	BLOCK_ELSIF		2
#define	BLOCK_WHILE		3
#define	BLOCK_FOR		4
#define	BLOCK_FUNCTION	5

//ofset aritmetickych operaci
#define OFFSET_OP		50

//specialni pravidla pro generator instrukci
#define	TERMINAL_TO_EXPRESSION 	43
#define	COMMAND_JOIN 			41
#define	COMMAND_DECLAREFUN		42

//stavy mikro automatu
#define	NOP			0
#define	RETURN		1
#define	POSTRETURN  2
#define	SIMPLYEXP  	3
#define	LASTEXP  	4
#define	TITEMPTR  	5

typedef struct block
{
	int type; //typ bloku
	unsigned int id;
	unsigned int label1, label2; //index navesti
}TBlock;

bool AddBlock(THtable *tableSymbols, int typeBlock, unsigned int label1, unsigned int label2);
int GenerateInstructions(THtable *tableSymbols, int indexRule, TItem *tItem, TRuleString *tempA, TRuleString *tempB, bool lastExp, int *errorCode);
void PrepareInstruciton(TInstr *instruction);

#endif /* GENERATOR_INSTRUCTIONS_H_ */
