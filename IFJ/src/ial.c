//
// ial.c
//
// Tabulka symbolu, Mergesort
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	19. 10. 2013
// Autor:		Dominika Regeciova (xregec00) | Adam Ruzicka (xruzic43)

#include "ial.h"
#include "scanner.h"
#include "generator_instructions.h"

void *valueMemory[10]={NULL}; // Pole ukazatelu na Value
void *valueWorking=NULL; // Ukazatel pro praci s polem Value
int blok=-1; // Pocet doalokovanych bloku value
int growingCounter=1; // Pri opakovane prealokaci budu zvaetsovat alokovanou pamet value
unsigned int emptySpace=0; // Kotrola, zda mam dost mista na ukladani do value


// Jmeno:					HashFunction
// Paremetr element:		Ukazatel na hashovany prvek, retezec
// Parametr hTableSize:		Velikost tabulky
// Popis:					Funkce zpracovava prvek, znak po znaku, dokud nedojde na konec �etezce,
//							kde za kazd� znak z�sk� ur�itou hodnotu, kterou postupne scita.
//							Nakonec vraci modulo teto hodnoty, aby byl index mensi nez velikost tabulky.
// Nav. hodnota:			Funkce vraci pozici (index) v tabulce symbol�
unsigned int HashFunction (const char *element, unsigned int length, unsigned hTableSize)
{
    unsigned int position=0;
    unsigned char *hashElement=NULL;

    for(hashElement=(unsigned char*)element; length != 0; hashElement++, length--)
    {
        position = 31*position + *hashElement;
    }

    return position % hTableSize;
}


// Jmeno:					HtableInit
// Parametr size:			Velikost tabulky
// Popis:					Funkce alokuje pamet pro tabulku symbolu o dane velikosti
//							a inicializuje jeji pocatecni hodnoty.
// Nav. hodnota:			Funkce vraci ukazatel na inicializovanou tabulku symbolu,
//							nebo NULL, pripade nezdarene alokace pameti
THtable * HtableInit (unsigned int size)
{
	THtable *tmp=NULL;
	int initHelp=0;
	unsigned int toalloc = sizeof(THtable) + size * sizeof(Tlist); // Alokace pameti
	if ((tmp = malloc(toalloc)) == NULL) // V pripade nespravne alokace
		return NULL;

	initHelp=ValueInit(); // Inicializace tabulky value
	if (initHelp==-1) // Funkce vratila chybovou hodnotu
	{
		free(tmp);
		return NULL;
	}

	tmp->size = size; // Ulozim si velikost tabulky
	tmp->actualBlock=NULL;

	for (unsigned int i = 0; i < size; i++) // V opacnem pripade vytvorim pole odkazu na polozky tabulky
	{
		tmp->array[i] = NULL;
	}

	return tmp;
}


// Jmeno:					HtableFree
// Parametr hash:			Ukazatel na tabulku symbolu
// Popis:					Funkce uvolnuje pamet tabulky symbolu
// Nav. hodnota:			Funkce nevraci zadnou hodnotu
void HtableFree (THtable *hash)
{
	THTableItem *tmp=NULL;
	THTableItem *help=NULL;

	for (unsigned int i = 0; i < hash->size; i++) // Prochazim celou tabulku
	{
		tmp = hash->array[i];
	    while (tmp != NULL) // Uvolnuji vsechny prvky - muze jich byt vic za sebou
	    {
	    	help = tmp;
	        tmp = tmp->next;
	        free(help); // Uvolneni jedne polozky
	        help=NULL;
	    }
	    hash->array[i] = NULL; // Uvolnim tabulku odkazu
	}
	free(hash); // Uvolnim TS
	hash=NULL;

	// Projdu tabulku ukazatelu na value a uvolnim ji
	for (int i=0;valueMemory[i]!=NULL;i++)
	{
		free(valueMemory[i]);
	}
}

// Jmeno:					ValueInit
// Popis:					Funkce alokuje pamet ukladan� hodnot tokenu,
//							pop��pad� doalokuje pot�ebnou pam�t - po blocich
// Nav. hodnota:			Funkce vraci 0, pokud se alokace pameti zadarila,
//							jinak vrac� hodnotu -1
int ValueInit()
{
	int sizeValue=0; // Velikost pridavane pameti
	blok++; // Nastavim "poradove" cislo alokace tabulky value
	sizeValue=VALUE_SIZE*growingCounter; // Velikost alokace
	growingCounter++; // Priste budu alokovat o to vetsi blok pameti
	emptySpace=sizeValue; // Cely alokovany blok bude prazdny

	valueMemory[blok]=malloc(sizeValue);
	if(valueMemory[blok]==NULL) // Alokace se nezdarila
	{
		return -1;
	}

	valueWorking=valueMemory[blok];
	return 0;
}


// Jmeno:					ValueStore
// Parametr sType:			Typ tokenu
// Parametr sValue:			Ukazatel na hodnotu tokenu, retezec
// Parametr length:			Delka retezce sValue
// Parametr list:			Ukazatel na polozku v tabulce symbolu, ke ktere mam pridat Value
// Popis:					Funkce ulozi hodnotu tokenu do pole Value,
//							podle typu - int, double, jinak uklada jako char
// Nav. hodnota:			Funkce nevraci zadnou hodnotu
void ValueStore (int sType, char *sValue, unsigned int length, THTableItem *list, int *errorCode)
{
	/*for (int i = 0; i < length; i++)
	{
		putchar(sValue[i]);
	}
	putchar('\n');*/
	// valueWorking je ukazatel na prvni volne misto
	// eptySpace je pocitadlo, kolik mista volne pameti jeste mam
	switch (sType)
	{
		case (LEX_INT):	// V pripade integeru zmenim char na int a ulozim si ho

			list->data.iValue = StringToInt(sValue);
			break;

		case (LEX_DOUBLE):	// V pripade doublu zmenim char na double a ulozim si ho

			list->data.dValue = StringToDouble(sValue);
			break;

		case (LEX_BOOL):	// V pripade doublu zmenim char na double a ulozim si ho

			if (strncmp(sValue, "true", length) == 0)
				list->data.iValue = 1;
			else
				list->data.iValue = 0;


			break;

		case (LEX_STRING): // String ukladam jako string - je to nutne???

			list->id = NULL;
			if (emptySpace<(length*sizeof(char)))
			{
				if(ValueInit() == -1)
				{
					*errorCode = ERROR_INTERPRET;
					return;
				}
			}

			strncpy(valueWorking, sValue, length);

			list->data.sValue = valueWorking;
			valueWorking=(char *)valueWorking+(length*sizeof(char)) ;
			(*((char*)valueWorking)) = '\0'; //ukoncujici nula
			valueWorking=1+(char *) valueWorking;

			emptySpace=emptySpace-(length*sizeof(char)+1); // Doplneni 1

			break;
		default:
//		case (LEX_ID):			 // Jinak ukladam posloupnost charu
			if (emptySpace<(length*sizeof(char)))
			{
				if(ValueInit() == -1)
				{
					*errorCode = ERROR_INTERPRET;
					return;
				}
			}

			strncpy(valueWorking, sValue, length);
			list->id=valueWorking;
			list->data.iValue = 0;

			valueWorking = ((char *) valueWorking) + (length * sizeof(char));
			*((char*)valueWorking) = '\0'; //ukoncujici nula

			//printf("Ukladam %s do bloku %d\n", list->id, list->blok);
			valueWorking=1+(char *) valueWorking;

			emptySpace=emptySpace-(length*sizeof(char)+1); // Doplneni 1

		break;
	}

	list->type = sType;
	list->blok = -1;
	return;
}


// Jmeno:					HtableFindItem
// Parametr sType:			Typ tokenu
// Parametr sValue:			Ukazatel na hodnotu tokenu, retezec
// Parametr hash:			Ukazatel na tabulku symbolu
// Popis:					Funkce hleda polozku, nejdriv vsak zavola hashovaci funkci,
//							aby zjistila, kde by se polozka mela nachazet
// Nav. hodnota:			Funkce vraci ukazatel hledanou polozku, nebo NULL
TItem * HtableFindItem(int sType, char *sValue, unsigned int length, THtable *hash)
{
	//char key=(char)sType+sValue[0]+'\0'; // Key pro hashovani je tvoren typem a prvnim znakem nazvu/hodnoty
	//printf("\t%d + %d = %d %s\n", sType, sValue[0] key, sValue);
	unsigned int hashKey=HashFunction(sValue, length, hash->size); // Ziskam pozici v array
	return HtableLookUp(sType, sValue, length, hashKey, hash);
}

// Jmeno:					HtableLookUp
// Parametr sType:			Typ tokenu
// Parametr sValue:			Ukazatel na hodnotu tokenu, retezec
// Parametr hashKey:		Ukazatel do tabulky symbolu, klic polozky
// Parametr hash:			Ukazatel na tabulku symbolu
// Popis:					Funkce hleda polozku, porovnava vsechyn hodnoty, vcetne bloku
// Nav. hodnota:			Funkce vraci ukazatel hledanou polozku, nebo NULL
 TItem * HtableLookUp (int sType, char *sValue, unsigned int length, unsigned int hashKey, THtable *hash)
 {
	sValue[length] = '\0';

	/*for (int i = 0; i < length; i++)
		putchar(sValue[i]);
	putchar('\t');*/

	THTableItem *item = hash->array[hashKey];

	 while (item != NULL)
	 {
		 if (item->type != sType)
		 {
			item = item->next;
			continue;
		 }

		 switch (item->type)
		 {
		 	 case (LEX_INT):
				if (item->data.iValue == StringToInt(sValue))
					return (TItem *) item;
				break;

		 	 case (LEX_DOUBLE):
				if (item->data.dValue == StringToDouble(sValue))
					return (TItem *) item;
				break;

		 	 case (LEX_STRING):
				if (strlen(item->data.sValue) ==  length && strncmp(item->data.sValue, sValue, length) == 0)
					return (TItem *) item;
				break;

		 	 case (LEX_ID):

				if (strlen(item->id) ==  length && strncmp(item->id, sValue, length) == 0)// && item->blok == hash->actualMainBlock)
					return (TItem *) item;
				break;

		 	 case (LEX_FUN_ID):
				if (strlen(item->id) ==  length && strncmp(item->id, sValue, length) == 0)
					return (TItem *) item;
				break;
		 }

		 item = item->next;
	 }

	 return NULL;
}

 // Jmeno:					HtableInsert
 // Parametr sType:			Typ tokenu
 // Parametr sValue:		Ukazatel na hodnotu tokenu, retezec
 // Parametr hash:			Ukazatel na tabulku symbolu
 // Parametr length:		Delka retezce sValue
 // Popis:					Funkce vlozi polozku, nebo vraci odkaz na polozku, pokud tu uz je
 // Nav. hodnota:			Funkce vraci ukazatel vlozenou polozku, nebo NULL v pripade chyby
TItem * HtableInsert (int sType, char *sValue, unsigned int length, THtable *hash, int *errorCode)
{
	sValue[length] = '\0';

	unsigned int hashKey = HashFunction(sValue, length, hash->size); // Ziskam pozi	ci v array

	THTableItem *item = NULL;
	THTableItem *tmp = NULL; // Pomocni ukazatel, pro provazani prvku

	if ((item = hash->array[hashKey]) == NULL) 			// Pozice je volna
	{
		if ((tmp = malloc(sizeof(THTableItem))) == NULL)
		{
			*errorCode = ERROR_INTERPRET;
			return NULL;
		}
	}
	else // Pozice neni volna = prvek zde uz muze byt
	{
		item = (THTableItem *) HtableLookUp(sType, sValue, length, hashKey, hash);
		if (item != NULL)
		{
			return (TItem *) (item); // Prvek zde uz je, vratim na nej odkaz
		}
		else
		{
			if ((tmp = malloc(sizeof(THTableItem))) == NULL)
			{
				*errorCode = ERROR_INTERPRET;
				return NULL;
			}
		}
	}

	tmp->id = NULL;
	tmp->next = hash->array[hashKey];
	hash->array[hashKey] = tmp;
	ValueStore(sType, sValue, length, tmp, errorCode);

	return (TItem *) tmp;
}

// Jmeno:					StringToDouble
// Parametr number:			Ukazatel na retezec charu
// Popis:					Funkce prijme retezec, ktery zmeni na double
// Nav. hodnota:			Double
double StringToDouble (char *number)
{
	double result=0.0;
	sscanf(number,"%lf",&result);
	return result;
}

// Jmeno:					StringToInt
// Parametr number:			Ukazatel na retezec charu
// Popis:					Funkce prijme retezec, ktery zmeni na integer
// Nav. hodnota:			Int
int StringToInt (char *number)
{
	int result=0;
 	sscanf(number,"%d",&result);
 	return result;
}

char *Mergesort(char *list, int *errorCode) {
	*errorCode=RESULT_OK;
	TStack *stringStack;
    char *sorted=NULL;
    int length=strlen(list);
    if((sorted=malloc(length*sizeof(char)+1))==NULL) {
		*errorCode = ERROR_INTERPRET;
		return NULL;
    }
    memcpy(sorted,list,length+1); // kopirovani retezce do nove alokovaneho mista v pameti
    int step=1; // delka kroku 1
    int left, right, index;
    int leftListStart, rightListStart, leftListEnd, rightListEnd;
    int leftListLength, rightListLength;
    while(step<length) {
        for(leftListStart=0; leftListStart<length; leftListStart+=step*2) { // prochazime retezec po kroku
            leftListEnd = leftListStart + step; // konec leveho je zacatek + delka kroku
            rightListStart = leftListEnd; // zacatek praveho je konec leveho
            rightListEnd = rightListStart + step; // konec praveho je zacatek + delka kroku
            if(leftListEnd > length) leftListEnd = length; // pokud levy konci za retezcem, nastavime konec na posledni znak retezce
            if(rightListStart > length) rightListStart = length; // pokud pravy zacina za retezcem, nastavime jeho zacatek na posledni znak
            if(rightListEnd > length) rightListEnd = length; // pokud pravy konci za retezcem, nastavime jeho konec na posledni znak
            leftListLength = leftListEnd - leftListStart; // delka podretezce je rozdilem jeho konce a zacatku
            rightListLength = rightListEnd - rightListStart;
            char ra[rightListLength]; // pole pro podretezce
            char la[leftListLength];
            left = right = 0; // indexy podretezcu
            memcpy(&la, &sorted[leftListStart], leftListLength); // kopirovani z retezce do podretezcu
            memcpy(&ra, &sorted[rightListStart], rightListLength);
            for(index=leftListStart, right = 0, left = 0; left<leftListLength && right<rightListLength; index++) { // dokud jsme nedosli na konec jednoho z podretezcu
                if(la[left] < ra[right]) {
                    sorted[index] = la[left++];
                } else {
                    sorted[index] = ra[right++];
                }
            }
            for( ; left<leftListLength; left++, index++) { // dokopirovani podretezce ktery nedosel na konec
                sorted[index] = la[left];
            }
            for( ; right<rightListLength; right++, index++) {
                sorted[index] = ra[right];
            }
        }
        step*=2; // zdvojnasobeni kroku
    }
	StackPush((void *)&sorted, &stringStack); // pridani noveho retezce na zasobnik retezcu
    return sorted;
}

