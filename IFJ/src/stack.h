//
// stack.h
//
// Datova struktura zasobniku
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	15. 10. 2013
// Autor:		Martin Roncka (xronck00) | Michal Pustka (xpustk00)

#ifndef STACK_H_
#define STACK_H_

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>
#include "error.h"
#include "data_structures.h"

typedef struct stack
{
	void **data;
	struct stack *next;
}TStack;

bool StackPush(void **data, TStack **stack);
void** StackPop(TStack **stack);
void FreeStack(TStack *stack);

#define Pop(x) StackPop(&x)
#define Push(x, y) StackPush((void **)x, &y)

extern TStack *stringStack;

/* Kontrinualni zasobnik */
#define STACK_SIZE 	10				// velikost tabulky bloku
#define FAST_SIZE	16
#define STACK_INIT 	1024			// pocatecni velikost bloku
#define FAST_INIT	65536

/* Makro pro generickou definici kontinualniho zasobniku */
#define DEF_contStack(TYPE)									\
															\
	typedef struct TYPE##contStack							\
	{														\
		TYPE *table[STACK_SIZE];							\
		TYPE *memory;										\
		unsigned int sector;								\
		int top;											\
		int max;											\
	}TYPE##ContStack;										\

/* Makro pro inicializaci zasobniku */
#define TCSInit(STACK, TYPE, RETVAL, SIZE)					\
															\
	for (int i = 0; i < STACK_SIZE; i++)					\
		STACK.table[i] = NULL;								\
															\
	STACK.sector = -1;										\
	STACK.max = SIZE;										\
	++STACK.sector;											\
															\
	if ((STACK.table[STACK.sector] = malloc(STACK.max * sizeof(TYPE))) == NULL)			\
	{														\
		return ERROR_INTERPRET;								\
	}														\
															\
	STACK.memory = STACK.table[STACK.sector];				\
	STACK.top = -1;											\

/* Makro pro uvolneni pameti alokovane v blocich zasobniku */
#define TCSFree(STACK)										\
															\
	for (int i = 0; i < STACK_SIZE; i++)					\
	{														\
		free(STACK.table[i]);								\
		STACK.table[i] = NULL;								\
	}														\
															\
	STACK.sector = -1;										\
	STACK.top = STACK_INIT;									\
	STACK.max = STACK_INIT;									\

/* Makro vlozi na zasobnik hodnotu DATA typu TYPE */
#define TCSPush(STACK, DATA, RETVAL, TYPE)					\
															\
	++((STACK).top);										\
	if (((STACK).top) >= (STACK).max)						\
		TCSIncrement(STACK, TYPE, RETVAL);					\
	(STACK).memory[(STACK).top] = DATA;						\

/* Kontrola zda muzeme popnout zasobnik */
#define TCSTopCheck(STACK, TYPE, RETVAL)					\
															\
	++((STACK).top);										\
	if (((STACK).top) >= (STACK).max)						\
		TCSIncrement(STACK, TYPE, RETVAL);					\

/* Mkro pro ziskani ukazatele z vrcholu zasobniku */
#define TCSGetTopPtr(STACK, PTR)							\
															\
	PTR = (STACK).memory + (STACK).top;						\
	PTR = (STACK).memory + (STACK).top--;					\

/* Makro pro nacteni hodnoty z vrcholu zasobniku a pop */
#define TCSPop(STACK, DATA, TYPE)							\
															\
	if ((STACK).top < 0)									\
		TCSDecrement(STACK, TYPE);							\
	DATA = (TYPE) (STACK).memory[(STACK).top--];			\

/* ziskame ukazatel na prvek vrcholu zasobniku a popneme */
#define TCSGetTopPop(STACK, PTR, TYPE)						\
															\
	if ((STACK).top < 0)									\
		TCSDecrement(STACK, TYPE);							\
	PTR = (STACK).memory + (STACK).top--;					\

/* Makro pro deklaraci genericke funkce pro inkrementaci CS */
#define DEF_TCSIncrement_DEC(TYPE)							\
															\
	int TYPE##TCSIncrement(TYPE##ContStack *stack);			\

/* Makro pro definici genericke funkce TCSIncrement */
#define DEF_TCSIncrement(TYPE)								\
															\
	int TYPE##TCSIncrement(TYPE##ContStack *stack)			\
	{														\
		++stack->sector;									\
		stack->max <<= 1;									\
															\
		if (stack->table[stack->sector] == NULL)			\
		{													\
			if ((stack->table[stack->sector] = malloc(stack->max * sizeof(TYPE))) == NULL)			\
			{												\
				return ERROR_INTERPRET;						\
			}												\
		}													\
															\
		stack->memory = stack->table[stack->sector];		\
		stack->top = 0;										\
		return 0;											\
	}														\

/* Makro pro deklaraci genericke funkce pro dekrementaci CS */
#define DEF_TCSDecrement_DEC(TYPE)							\
															\
	void TYPE##TCSDecrement(TYPE##ContStack *stack);		\

/* Makro pro definici genericke funkce TCSDecrement */
#define DEF_TCSDecrement(TYPE)								\
															\
	void TYPE##TCSDecrement(TYPE##ContStack *stack)			\
	{														\
		--(stack->sector);									\
		stack->memory = stack->table[stack->sector];		\
		stack->max >>= 1;									\
		stack->top = stack->max - 1;						\
	}														\

/* Makro vraci typ kontinualniho zasobniku */
#define GET_CONT_STACK_TYPE(TYPE)	TYPE##ContStack

/* Obecne makro por TCSIncrement nahrazuje genericke funkce */
#define TCSIncrement(STACK, TYPE, RETVAL)					\
{															\
	++(STACK).sector;										\
	(STACK).max <<= 1;										\
															\
	if ((STACK).table[(STACK).sector] == NULL)				\
	{														\
		if (((STACK).table[(STACK).sector] = malloc((STACK).max * sizeof(TYPE))) == NULL)			\
		{													\
			RETVAL = ERROR_INTERPRET;						\
		}													\
	}														\
															\
	(STACK).memory = (STACK).table[(STACK).sector];			\
	(STACK).top = 0;										\
}															\

/* Obecne makro por TCSDecrement nahrazuje genericke funkce */
#define TCSDecrement(STACK, TYPE)							\
{															\
	--((STACK).sector);										\
	(STACK).memory = (STACK).table[(STACK).sector];			\
	(STACK).max >>= 1;										\
	(STACK).top = (STACK).max - 1;							\
}															\


/* Definujeme kontinualni zasonik typu TOperPtr i funkce nad nim */
DEF_contStack(TOperPtr)
DEF_TCSIncrement_DEC(TOperPtr)
DEF_TCSDecrement_DEC(TOperPtr)



/* Makra pro zasobniku typu fast je pro konkretni implementaci jednoho
 * zasobniku s konkretnimi nazvy jeho prvku, moznost flexibilne zadat
 * o ulozeni casti zasobniku do registru a pracovat tak rychleji
 *
 * Operace nad timto zasobnikem jsou obdobne k TCStack
 */
#define FastInit(SECTOR, TABLE, STACK, TOP, MAX, RETVAL, TYPE)	\
																\
	for (int i = 0; i < FAST_SIZE; i++)							\
		TABLE[i] = NULL;										\
																\
	SECTOR = 0;													\
	MAX = FAST_INIT;											\
																\
	if ((TABLE[0] = malloc(FAST_INIT * sizeof(TYPE))) == NULL)	\
	{															\
		RETVAL = ERROR_INTERPRET;								\
	}															\
																\
	STACK = TABLE[0];											\
	TOP = 0;													\

#define FastFree(TABLE)											\
																\
	for (int i = 0; TABLE[i] != NULL && i < FAST_SIZE; i++)		\
	{															\
		free(TABLE[i]);											\
		TABLE[i] = NULL;										\
	}															\

#define FastCheckPush()											\
																\
	if (top >= max)												\
	{															\
		++stackSector;											\
		max <<= 1;												\
																\
		if (stackTable[stackSector] == NULL)					\
		{														\
			if ((stackTable[stackSector] = malloc(max * sizeof(TStore))) == NULL)	\
			{													\
				fprintf(stderr, "Error alocating memory");		\
				retVal = ERROR_INTERPRET;						\
				break;											\
			}													\
		}														\
																\
		stack = stackTable[stackSector];						\
		top = 0;												\
	}															\

#define FastPopTop(SECTOR, TABLE, STACK, TOP, MAX, RETVAL, TYPE)\
																\
    FastCheckPop(SECTOR, TABLE, STACK, TOP, MAX);				\
	DATA = TABLE[--TOP];										\*/

#define FastCheckPop(SECTOR, TABLE, STACK, TOP, MAX)			\
																\
	if (TOP == 0)												\
	{															\
		--SECTOR;												\
		STACK = TABLE[SECTOR];									\
		MAX >>= 1;												\
		TOP = MAX-1;											\
	}															\


#endif /* STACK_H_ */
