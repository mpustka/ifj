//
// embedded.h
//
// Vestavene funkce
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	28. 10. 2013
// Autor:		Adam Ruzicka (xruzic43)

#ifndef EMBED
#define EMBED
#include <stdio.h>
#include <stdlib.h>
#include <string.h> //strlen
#include <math.h> //pow
#include "error.h"
#include <stdarg.h>
#include <ctype.h>
#include <stdbool.h>
#include "stack.h"
#include "interpret.h"
#include "scanner.h"
#include "ial.h"
#include "stack.h"

#define DCS_START 0
#define DCS_INTS 1
#define DCS_INT 2
#define DCS_DEC0 3
#define DCS_DEC1 4
#define DCS_EXPS 5
#define DCS_EXP 6

#endif

int IntVal(void *data, char typ, int *errorCode);
bool BoolVal(void *data, char typ);
double DoubleVal(void *data, char typ, int *errorCode);
char *StrVal(void *data, char typ, int *errorCode);
int PutString(TOperPtrContStack *varStack, int *parCount);
char *GetSubString(char *inputString,int start, int end,int *errorCode);
char *GetString(int *errorCode);
int FindSubString(char *inputString, char *pattern);
int calculateOffset(char, char *);
char *StripWS(char *inputString,int *errorCode);
double DoubleParse(char *data,int *errorCode);
char *ReallocString(char *inputString, int size,int *errorSize);
