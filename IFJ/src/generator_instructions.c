// generator_instructions.h
//
// Generator instrukci
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	20. 10. 2013
// Autor:		Michal Pustka (xpustk00)

#include "generator_instructions.h"

// Jmeno:					AddBlock
// Parametr blocks:			ukazatel na ukazatel bloku, jak jdou chronologicky zasebou
// Parametr tableSymbols:	ukazatel na tabulku symbolu
// Parametr label1:			hodnota navesti1, slouzi pozdeji pro generovani instrukci
// Parametr label2:			hodnota navesti2, slouzi pozdeji pro generovani instrukci
// Popis:					Prida novy blok do posloupnosti bloku toku programu.
// Nav. hodnota:			Vrati false pokud nastala chyba pri alokaci, jinak vraci true.
bool AddBlock(THtable *tableSymbols, int typeBlock, unsigned int label1, unsigned int label2)
{
	TBlock *block = NULL; //ukazatel na novy blok
	static int id = 0;

	if((block = (TBlock*)malloc(sizeof(TBlock))) == NULL) //alokace pameti pro novy blok
		return false; //alokace selhala

	block->label1 = label1; //ulozeni navesti, pro pozdejsi generovani insrukci
	block->label2 = label2;
	block->type = typeBlock; //ulozeni typu bloku napr. Typ IF, WHILE, ELSE...
	block->id = (typeBlock == BLOCK_MAIN) ? BLOCK_MAIN : id++;

	if(!StackPush((void **)block, &tableSymbols->actualBlock))
		return false; //alokace selhala

	//ulozime aktulani hlavni blok
	if(typeBlock == BLOCK_MAIN || typeBlock == BLOCK_FUNCTION)
		tableSymbols->actualMainBlock = block->id;

	return true;
}

// Jmeno:					GenerateInstructions
// Parametr tableSymbols:	ukazatel na tabulku symbolu
// Parametr indexRule:		index pravidla
// Parametr tItem:			ukazatel do tabulky symbolu: promenna, konstanta ci funkce
// Parametr tempA:			index mezi-promenneA
// Parametr tempB:			index mezi-promenneB
// Parametr lastExp:		indikuje posledni generovani ve vyrazu, zduvodu optimalizace
// Parametr errorCode:		predava typ chyby odkazem
// Popis:					Nazakladne indexu gramatickych pravidel ci jinych pravidel se provedou sematicke akce,
//							ktere maji za ukol vygenerovat instrukce. Generovani instrukci se provadi tak, ze
//							se posle ukazatel na strukturu TInstr, ktera se posle interpretu. Interpret nasledne
//							alokuje misto pro instrukci a ulozi si ji.
// Nav. hodnota:			Pokud je funkce volana z predektivni analyzy(analyza toku programu) vraci true pokud probehla,
//							alokace bloku uspecne, jinak false.
//							Pokud je funkce volana z precedecni analyzy(analyza vyrazu) vraci index mezi-promenne,
//							ktera poslouzi pro nasledne generovani instrukci.
int GenerateInstructions(THtable *tableSymbols, int indexRule, TItem *tItem, TRuleString *tempA, TRuleString *tempB, bool lastExp, int *errorCode)
{
	//promenna label slouzi pro generovani indexu navesti
	//promenna temp slouzi pro generovani indexu mezi-promennych
	//promenne fromJoin, toJoin slouzi pro spojeni dvou instrukci respektive jejich hodnoty
	//napr. navratova hodnota, ktera se ma ulozit do dane promenne
	//promenna args slouzi pro generovani instrukci, ktere volaji podprogram(funkci), jako pocitadlo argumentu
	//promenne tempLabelX slouzi jako promenne pro ukaladani indexu labelu, spojeni vice sementickych akci, kdy jedna akce nastavi index, druha vygeneruje instrukci
	//promenna offset slouzi pro pozdejsi posouvani se v zasobniku kodu
	//promenna state mikro konecny automat, slozi pro optimalizovane generovani nekterych instrukci
	//promenna tempItem ukazatel do tabulky symbolu, vyrovnavaci pamet mezi dvema akcemi
	static unsigned int label = 1, temp = 1, fromJoin = 1, toJoin = 1, args = 0, tempLabel1 = 0, tempLabel2 = 0, tempLabel3 = 0, offset = 1, state = NOP;
	static TItem *tempItem = NULL;
	TInstr instruction; //pamet pro instrukci, ktera bude predavana interpretu

	//inicializujeme prazdnou instrukci
	PrepareInstruciton(&instruction);

	switch(indexRule)
	{
		//<STATEMENTS-LIST> → ε
		//Pokud nastava tohle pravidlo, vime, ze preachazime do jineho bloku,
		//nebo-li vychazime pryc z predesleho bloku
		case 6:
		{
			//vytahni ze zasobniku nejvyssi blok
			TBlock *block = (TBlock*)StackPop(&tableSymbols->actualBlock);

			//generuje konce techto bloku, prozor nikoliv jejich zacatky
			switch(block->type)
			{
				case BLOCK_IF:
				case BLOCK_ELSE:

					instruction.instr = LABEL;

					break;

				case BLOCK_ELSIF:


					instruction.instr = LABEL;
					instruction.operA.type = OP_LABEL; //typ operadnu navesti
					instruction.operA.data = block->label2; //prirazeni indexu navesti

					//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
					//pod minulou instrukci
					AddInstruction(1, &instruction, errorCode);

					//zacneme generovat novou instrukci navesti
					PrepareInstruciton(&instruction);
					instruction.instr = LABEL;

					break;

				case BLOCK_FOR:
				case BLOCK_WHILE:

					instruction.instr = JMP; //generujeme jump
					instruction.operA.type = OP_LABEL; //typ operandu, navesti
					//navesti, kde program skocit, kdyz se splni podminka
					instruction.operA.data = block->label2;

					//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
					//pod minulou instrukci
					AddInstruction(1, &instruction, errorCode);

					//zacneme generovat novou instrukci navesti
					PrepareInstruciton(&instruction);
					instruction.instr = LABEL;

					break;

				case BLOCK_FUNCTION:

					//popli jsme blokc funkce, aktulani hlavni blok musi byt main
					tableSymbols->actualMainBlock = BLOCK_MAIN;

					//tato instukce bude generovana vzdy, ikdyz funkce neco vraci
					instruction.instr = RET; //generujeme ret, navraci hodnotu
					instruction.operA.type = OP_NONE; //typ operandu

					//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
					//pod minulou instrukci
					AddInstruction(1, &instruction, errorCode);

					//pripravime se na generovani nove instrukce
					PrepareInstruciton(&instruction);
					instruction.instr = LABEL;
					break;

				case BLOCK_MAIN:
					//na zasobniku nejsou zadne bloky, jsme v hlavnim bloku programu
					return true;
			}

			//generujem instrukci navesti
			instruction.operA.type = OP_LABEL; //typ operandu, navesti
			instruction.operA.data = block->label1;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//uvolni blok z pameti
			free(block);
			break;
		}

		//<STATEMENT> → return <EXPRESSION>;
		case 7:

			state = RETURN; //nastavime konecny automat do stavu Return

		break;

		//<STATEMENT> → fun_id (<ARGUMENTS-LIST>);
		/*case 8:

			//budeme volat fuknci, generujeme instrukci call
			instruction.instr = CALL;
			instruction.operB.type = OP_TITEM; //typ operadu, promenna z TS
			instruction.operB.ptr = tItem;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			break;*/

		//<STATEMENT> → if (<EXPRESSION>) { <STATEMENTS-LIST> } <ELSE>
		case 9:
		{
			//generuje instrukci jump not, pokud se podminka nesplni skaceme
			//na navesti vetve else
			instruction.instr = JMPN;

			//generuje jednotlive indexy navesti pro bloky podminky
			int lElse = label++;

			instruction.operA.type = OP_VARTEMP; //typ operady, mezi-promenna
			instruction.operA.data = temp++; //generujem index mezipromenne
			instruction.operB.type = OP_LABEL; //typ operadu, navesti
			instruction.operB.data = lElse; //index navesti pro vetve else

			toJoin = temp-1; //ocekavame vyraz, musime hodnotu vyrazku spojit

			if(!AddBlock(tableSymbols, BLOCK_IF, lElse, 0)) //prida novy blok IF do zasobnku bloku
			{
				*errorCode = ERROR_INTERPRET;
				return false;
			}

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//pristi instrukce se zaradi pred touto instrukci, protoze ocekavame vyraz
			ChangeInstructionPtr(-1);

			break;
		}


		//<ELSE> → elseif(<EXPRESSION>){<STATEMENTS-LIST>}<ELSE>
		case 12:
		{

			int lExit = label++, lTotalExit = label++; //vygeneruje navesti
			instruction.instr = JMP; //instrukce skoku

			instruction.operA.type = OP_LABEL; //typ operadu navesti
			instruction.operA.data = lTotalExit; //prirazeni id navesti

			//pridame instrukci do kodoveho zasobniku pred minulou instrukci
			AddInstruction(-1, &instruction, errorCode);
			//vratime ukazatel zpet na posledni instrukci
			ChangeInstructionPtr(1);

			//pripravime se na generovani nove instrukce
			PrepareInstruciton(&instruction);
			instruction.instr = JMPN;
			instruction.operA.type = OP_VARTEMP; //typ operady, mezi-promenna
			instruction.operA.data = temp++; //generujem index mezipromenne
			instruction.operB.type = OP_LABEL; //typ operadu, navesti
			instruction.operB.data = lExit; //index navesti pro navest, kde konci elseif blok

			toJoin = temp-1; //ocekavame vyraz, musime hodnotu vyrazku spojit

			//prida novy blok ELSEIF do zasobnku bloku
			if(!AddBlock(tableSymbols, BLOCK_ELSIF, lExit, lTotalExit))
			{
				*errorCode = ERROR_INTERPRET;
				return false;
			}

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//pristi instrukce se zaradi pred touto instrukci, protoze ocekavame vyraz
			ChangeInstructionPtr(-1);

			break;
		}

		//<ELSE> → else {<STATEMENTS-LIST>}
		case 11:
		{
			//generuje jednotlive indexy navesti pro bloky podminky
			int lExit = label++;

			//generuje instrukci jump, nepodmineny skok,
			//ktery obskoci vetev else
			instruction.instr = JMP;

			instruction.operA.type = OP_LABEL; //typ operadu, navesti
			instruction.operA.data = lExit; //index navesti pro vetve else

			//prida novy blok ELSE do zasobnku bloku
			if(!AddBlock(tableSymbols, BLOCK_ELSE, lExit, 0))
			{
				*errorCode = ERROR_INTERPRET;
				return false;
			}

			//vlozi instrukci JMP za label konce bloku IF tak aby obskocil ELSE blok
			AddInstruction(-1, &instruction, errorCode);

			//ukazatel zpatky na posledni instrukci
			ChangeInstructionPtr(1);

			break;
		}


		//<STATEMENT> → continue;
		//<STATEMENT> → break;
		case 20:
		case 19:
		{
			TStack *item = tableSymbols->actualBlock; // aktualni blok
			TBlock* block = NULL;

			while(item != NULL) //dokud se nedostane na spod zasobniku, pokracuje
			{
				block = (TBlock*)item->data; //ziskej blok

				//jestli-ze se nachazi na stejne ci na vyssi urovni blok while resp. for
				if(block->type == BLOCK_FOR || block->type == BLOCK_WHILE)
					break; //preruseni cyklu

				block = NULL;
				item = item->next;
			}

			if(block != NULL)
			{
				instruction.instr = JMP;
				instruction.operA.type = OP_LABEL;

				//<STATEMENT> → break;
				if(indexRule == 20)
				{
					instruction.operA.data = block->label2;
				}else{ //<STATEMENT> → continue;
					instruction.operA.data = block->label1;
				}

				//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
				//pod minulou instrukci
				AddInstruction(1, &instruction, errorCode);
			}else{
				//continue resp. break nema sematincky vyznam, vypisujeme chybove hlaseni
				fprintf(stderr, "Semantic error: wrong application continue or break.\n");
				*errorCode = ERROR_SEM_RUNT;
				return false;
			}

			break;
		}

		//<STATEMENT> → while (<EXPRESSION>) {<STATEMENTS-LIST>}
		case 13:
		{
			//generuje jednotlive indexy navesti: pokracovani v cyklu, konec cyklu
			int lContinue = label++, lExit = label++;

			//generuje instrukci navesti
			instruction.instr = LABEL;
			instruction.operA.type = OP_LABEL; //typ operadu, navesti
			instruction.operA.data = lContinue; //navesti pro pokracovani v cyklu

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//generuje instrukci jump not
			PrepareInstruciton(&instruction);
			instruction.instr = JMPN;
			instruction.operA.type = OP_VARTEMP; //typ operandu, mezi-promenna
			instruction.operA.data = temp++; //generuje index mezi-promenne
			instruction.operB.type = OP_LABEL; //typ operandu, navesti
			instruction.operB.data = lExit; //pokud podminka cyklu neplati skoci na navesti s indexem lExit
			toJoin = temp-1; //ocekavame vyraz, spojovaci promenna

			//pridame blok while
			if(!AddBlock(tableSymbols, BLOCK_WHILE, lExit, lContinue))
			{
				*errorCode = ERROR_INTERPRET;
				return false;
			}

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//pristi instrukce se zaradi pred touto instrukci, protoze ocekavame vyraz
			ChangeInstructionPtr(-1);

			break;
		}

		//<STATEMENT> → for( <FOR1>;  <FOR2>; <FOR3>) { <STATEMENTS-LIST> }
		case 14:
		{
			break;
		}

		//<FOR1> → id = <EXPRESSION>
		//<STATEMENT> → id = <PRE-EXPRESSION>;
		case 18:
		case 15:
		{
			tempItem = tItem;
			state = TITEMPTR;

			break;
		}

		//<FOR2> → <EXPRESSION>
		case 16:
		{
			//generuje jednotlive indexy navesti: pokracovani v cyklu, konec cyklu
			int lContinue = label++, lExit = label++;
			tempLabel1 = label++, tempLabel2 = label++, tempLabel3 = lContinue;

			//generuje instrukci navesti
			instruction.instr = LABEL;
			instruction.operA.type = OP_LABEL; //typ operadu, navesti
			instruction.operA.data = tempLabel2;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//generuje instrukci jump not
			PrepareInstruciton(&instruction);
			instruction.instr = JMPN;
			instruction.operA.type = OP_VARTEMP; //typ operandu, mezi-promenna
			instruction.operA.data = temp++; //generuje index mezi-promenne
			instruction.operB.type = OP_LABEL; //typ operandu, navesti
			instruction.operB.data = lExit; //pokud podminka cyklu neplati skoci na navesti s indexem lExit
			toJoin = temp-1; //ocekavame vyraz, spojovaci promenna

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			PrepareInstruciton(&instruction);
			instruction.instr = JMP;
			instruction.operA.type = OP_LABEL;
			instruction.operA.data = tempLabel1;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//vyraz az za instrukci JMPN
			ChangeInstructionPtr(-2);
			offset = 2;

			//pridame blok for
			if(!AddBlock(tableSymbols, BLOCK_FOR, lExit, lContinue))
			{
				*errorCode = ERROR_INTERPRET;
				return false;
			}

			break;
		}

		//<FOR2> → ε
		case 35:
		{
			//generuje jednotlive indexy navesti: pokracovani v cyklu, konec cyklu
			int lContinue = label++, lExit = label++;
			tempLabel1 = label++, tempLabel2 = label++, tempLabel3 = lContinue;

			//generuje instrukci navesti
			instruction.instr = LABEL;
			instruction.operA.type = OP_LABEL; //typ operadu, navesti
			instruction.operA.data = tempLabel2;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//generuje instrukci navesti
			instruction.instr = LABEL;
			instruction.operA.type = OP_LABEL; //typ operadu, navesti
			instruction.operA.data = tempLabel1;

			PrepareInstruciton(&instruction);
			instruction.instr = JMP;
			instruction.operA.type = OP_LABEL;
			instruction.operA.data = tempLabel1;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//pridame blok for
			if(!AddBlock(tableSymbols, BLOCK_FOR, lExit, lContinue))
			{
				*errorCode = ERROR_INTERPRET;
				return false;
			}

			break;
		}

		//<FOR3> → id = <EXPRESSION>
		case 17:
		{
			instruction.instr = LABEL;
			instruction.operA.type = OP_LABEL; //typ operadu, navesti
			instruction.operA.data = tempLabel3;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//MOV
			tempItem = tItem;
			state = TITEMPTR;

			PrepareInstruciton(&instruction);
			instruction.instr = JMP;
			instruction.operA.type = OP_LABEL;
			instruction.operA.data = tempLabel2;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//generuje instrukci navesti
			PrepareInstruciton(&instruction);
			instruction.instr = LABEL;
			instruction.operA.type = OP_LABEL; //typ operadu, navesti
			instruction.operA.data = tempLabel1;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//pred movem mame 2 instrukce
			ChangeInstructionPtr(-2);
			offset = 2;

			break;
		}

		//<FOR3> → ε
		case 36:
		{
			instruction.instr = LABEL;
			instruction.operA.type = OP_LABEL; //typ operadu, navesti
			instruction.operA.data = tempLabel3;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			PrepareInstruciton(&instruction);
			instruction.instr = JMP;
			instruction.operA.type = OP_LABEL; //typ operadu, navesti
			instruction.operA.data = tempLabel2;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//generuje instrukci navesti
			PrepareInstruciton(&instruction);
			instruction.instr = LABEL;
			instruction.operA.type = OP_LABEL; //typ operadu, navesti
			instruction.operA.data = tempLabel1;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			break;
		}

		//<CODE> → function fun_id (<PARAMS-LIST>) { <STATEMENTS-LIST> } <CODE>
		case COMMAND_DECLAREFUN:
		{
			//vytvorime jump pro interpret aby pri vykovanani kodu mohl preskocit, deklaraci
			unsigned int endLabel = label++;
			instruction.instr = JMP;
			instruction.operA.type = OP_LABEL; //typ operadu, navesti
			instruction.operA.data = endLabel;

			AddInstruction(1, &instruction, errorCode);

			//generuje instukci navesti
			PrepareInstruciton(&instruction);
			instruction.instr = LABEL;
			instruction.operA.type = OP_TITEM; //typ operandu, promenna z TB
			instruction.operA.ptr = tItem;
			instruction.operA.data = label++;

			//pridani bloku funkce
			if(!AddBlock(tableSymbols, BLOCK_FUNCTION, endLabel, 0))
			{
				*errorCode = ERROR_INTERPRET;
				return false;
			}

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			break;
		}

		//<PRE-EXPRESSION> → fun_id (<ARGUMENTS-LIST>)
		case 22:

			//generuje instrukci call, budeme volat podprogram(funkci)
			instruction.instr = CALL;
			instruction.operA.type = OP_TITEM; //typ operadnu, mezi-promenna

			instruction.operA.ptr = tempItem;
			instruction.operB.type = OP_TITEM; // typ operandu, promenna z TB
			instruction.operB.ptr = tItem;

			//posleme instrukci interpretu s ofsetem -1 instrukce se bude vkladat
			//za minulou instrukci
			AddInstruction(1, &instruction, errorCode);
			state = NOP;

			break;

		//<PARAM> → id
		case 25:

			//parametr funkce, generujeme insrukci pop, ktera vyjme parametr ze zasobniku
			//a vlozi ho do dane promenne
			instruction.instr = POP;
			instruction.operA.type = OP_TITEM; //type operandu, promenna z TB
			instruction.operA.ptr = tItem;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			break;

		//<ARGUMENT> → id
		//<ARGUMENT> → con
		case 31:
		case 30:

			//argument funkce, vlozime argument na zasobnik pred volanim dane funkce
			instruction.instr = PUSH;
			instruction.operA.type = OP_TITEM; //type operandu, promenna z TB
			instruction.operA.ptr = tItem;
			args++;

			//posleme instrukci interpretu s ofsetem -1 instrukce se bude vkladat
			//pred minulou instrukci
			AddInstruction(-1, &instruction, errorCode);

			break;

		//<ARGUMENT-LIST> → ε
		//<ARGUMENT-NEXT> → ε
		case 28:
		case 32:

			//zname pocet argumentu + 1, posunem zasobnik instrukci na instrukci call
			ChangeInstructionPtr(args+1);
			args = 0;

			break;

		//operace nad vyrazama
		//<EXPRESSION> → (neco)
		case TERMINAL_TO_EXPRESSION:


			if(state == RETURN)
			{
				instruction.instr = RET; //instrukce return
			}else{
				instruction.instr = MOVE; //instrukce presunu
			}

			if(state == RETURN)
			{
				state = POSTRETURN;
				instruction.operA.type = OP_TITEM; //type operandu, promenna z TB
				instruction.operA.ptr = tItem;
			}else if(state == NOP){
				instruction.operA.type = OP_VARTEMP;
				instruction.operA.data = toJoin;

			}else{ //jednoduchy vyraz napr.: $var = 5;
				state = SIMPLYEXP;
				instruction.operA.type = OP_TITEM; //type operandu, promenna z TB
				instruction.operA.ptr = tempItem;
			}

			if(state != POSTRETURN)
			{
				instruction.operB.type = OP_TITEM;
				instruction.operB.ptr = tItem;
			}

			AddInstruction(1, &instruction, errorCode);

			break;

		//<EXPRESSION> → <EXPRESSION> (operator) <EXPRESSION>
		case (OFFSET_OP + 0):
		case (OFFSET_OP + 1):
		case (OFFSET_OP + 2):
		case (OFFSET_OP + 3):
		case (OFFSET_OP + 4):
		case (OFFSET_OP + 5):
		case (OFFSET_OP + 6):
		case (OFFSET_OP + 7):
		case (OFFSET_OP + 8):
		case (OFFSET_OP + 9):
		case (OFFSET_OP + 10):

			switch(indexRule) //vyber typu instrukce
			{
				case (OFFSET_OP + 0): instruction.instr = ADD; break;
				case (OFFSET_OP + 1): instruction.instr = SUB; break;
				case (OFFSET_OP + 2): instruction.instr = MUL; break;
				case (OFFSET_OP + 3): instruction.instr = DIV; break;
				case (OFFSET_OP + 4): instruction.instr = CON; break;
				case (OFFSET_OP + 5): instruction.instr = ISSM; break;
				case (OFFSET_OP + 6): instruction.instr = ISBI; break;
				case (OFFSET_OP + 7): instruction.instr = ISSE; break;
				case (OFFSET_OP + 8): instruction.instr = ISBE; break;
				case (OFFSET_OP + 9): instruction.instr = SAME; break;
				case (OFFSET_OP + 10): instruction.instr = ISDIF; break;
			}

			//posledni instrukce ve vyrazu a zaroven je ve vyrovanvaci pameti ukazatel do TS
			if(state == TITEMPTR && lastExp)
			{
				instruction.operA.type = OP_TITEM; //type operandu, promenna z TB
				instruction.operA.ptr = tempItem;
			}else{
				instruction.operA.type = OP_VARTEMP; //typ operandu, mezi-promenna
				instruction.operA.data = lastExp ? toJoin : temp++; //generujeme index mezi-promenne
				fromJoin = temp-1; //mezi-prommnena slouzici ke spojeni vyrazu s prikazem
			}

			if(tempB->attr != 0)
			{
				instruction.operB.type = OP_VARTEMP; //typ operandu, mezi-promenna
				instruction.operB.data = tempB->attr; //operand
			}else{
				instruction.operB.type = OP_TITEM; //typ operandu, mezi-promenna
				instruction.operB.ptr = tempB->item; //operand
			}

			if(tempA->attr != 0)
			{
				instruction.operC.type = OP_VARTEMP; //typ operandu, mezi-promenna
				instruction.operC.data = tempA->attr; //operand
			}else{
				instruction.operC.type = OP_TITEM; //typ operandu, mezi-promenna
				instruction.operC.ptr = tempA->item; //operand
			}

			if(lastExp && state != RETURN)
				state =  LASTEXP;

			//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
			//pod minulou instrukci
			AddInstruction(1, &instruction, errorCode);

			//vraci index mezi-promenne
			return temp-1;

		case COMMAND_JOIN:

			if(state == TITEMPTR)
			{
				instruction.instr = MOVE;
				instruction.operA.type = OP_TITEM; //typ operandu, mezi-promenna
				instruction.operA.ptr = tempItem; //cilova mezi-promenna
				instruction.operB.type = OP_VARTEMP; //typ operandu, mezi-promenna
				instruction.operB.data = fromJoin; //mezi-promenna z vyrazu

				//posleme instrukci interpretu s ofsetem 1 instrukce se bude vkladat
				//pod minulou instrukci
				AddInstruction(1, &instruction, errorCode);
				tempItem = NULL;
				ChangeInstructionPtr(offset);
				state = NOP;
				break;

			}else if(state == RETURN){

				instruction.instr = RET;
				instruction.operA.type = OP_VARTEMP; //typ operandu, mezi-promenna
				instruction.operA.data = toJoin; //mezi-promenna z vyrazu
				AddInstruction(1, &instruction, errorCode);

				state = NOP;
				break;
			}else if(state == POSTRETURN){
				state = NOP;
				break;
			}else if(state == SIMPLYEXP || state == LASTEXP){
				ChangeInstructionPtr(offset);
				state = NOP;
				break;
			}

			//nasledujici instrukce se bude vklada az za predminulou instrukci
			ChangeInstructionPtr(offset);

			state = NOP;

			break;
	}

	return true;
}

// Jmeno:					PrepareInstruciton
// Parametr instruction:	Ukazatel na instrukci.
// Popis:					Pripravi novou instrukci, nastavi prazdne operandy.
// Nav. hodnota:
inline void PrepareInstruciton(TInstr *instruction)
{
	instruction->operA.type = OP_NONE;
	instruction->operB.type = OP_NONE;
	instruction->operC.type = OP_NONE;
	instruction->next = NULL;
	instruction->previous = NULL;
}
