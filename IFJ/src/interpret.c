//
// interpret.c
//
// Ukladani instrukci a interpretace
// Projekt:		Projekt do predmetu IFJ (Interpret jazyka IFJ13)
// Vytvoreno:	5. 10. 2013
// Autor:		Martin Roncka (xronck00)
//

#include "interpret.h"

/* Retezcova reprezentace instrukci pro ladici vypisy 3AD kodu */
char *instruct[] = {"LABEL", "MOVE", "JMP", "JMPN", "CALL", "RET", "PUSH",
					"POP", "MUL", "DIV", "ADD", "SUB", "CON", "ISBE", "ISSE",
					"ISSM", "ISBI", "ISDIF", "SAME"};

TLabel *labelTable[LAB_SIZE] = {NULL};			// Taulka s primym pristupem pro zretezene labely

TTmp *tempTable[AUX_SIZE] = {NULL};				// Pole ukazatelu na pomocne promenne typu TTmp
TTmp *auxTemp = NULL;							// Ukazatel na pole pomocnych promennych TTmp
int tempSector = -1;							// Ukazatel sektoru pomocnych promennych

unsigned int regSize = 500;						// pocet pomocnych promennych pri prvnim spusteni interpretu - v pripade nedostatku se zvysi na dvojnasobek
int retVal = 0;									// pro kontrolu chyboveho stavu interpretu
int block = 0;									// pro sledovani vnoreni a vynoreni z bloku programu - z funkci

/* Promenne pro praci s instrukcemi */
int sector = -1;								// ukazatel do pole alokvanych sektoru pameti pro instrukce
int stackN = 500;								// promenna pro multiplikativni zvetsovani alokovane pameti pro instrukce
TInstr *memory[MEM_SIZE] = {NULL};				// pole ukazatelu do pameti na kontinualni bloky pro ukladani instrukci
TInstr *code = NULL;							// ukazatel pro pamet kde se budou ukladat instrukce k interpretaci
TInstr *current = NULL;							// ukazatel do kodu na instrukci ve jejimz okoli pracujeme
TInstr *previous = NULL;						// ukazatel na predeslou instrukci instrukce current
TInstr *start = NULL;							// pro ulozeni pocatecni adresy kde se budou ukladat instrukce

/* Makro pro ulozeni stavu promenne */
#define FastItemStore(DATA)																\
																						\
	FastCheckPush();																	\
	stack[top].U.val = *((TTmp *)DATA);													\
	stack[top].ptr = DATA;																\
	++top;																				\

/* Makro pro ulozeni zarazky do zasobniku */
#define FastStopStore(TOPOP, RETVAR, RETINSTR)											\
																						\
	FastCheckPush();																	\
	stack[top].U.TBackStop.retInstr = RETINSTR;											\
	stack[top].U.TBackStop.toPop = TOPOP;												\
	stack[top].ptr = RETVAR;															\
	++top;																				\

/* Makro pro nacteni zarazky ze zasobniku */
#define FastStopRestore(TOPOP, RETVAR, RETINSTR)										\
																						\
	if (top == 0)																		\
	{																					\
		--stackSector;																	\
		stack = stackTable[stackSector];												\
		max >>= 1;																		\
		top = max;																		\
	}																					\
																						\
	--top;																				\
	RETINSTR = stack[top].U.TBackStop.retInstr;											\
	TOPOP = stack[top].U.TBackStop.toPop;												\
	RETVAR = stack[top].ptr;															\

/* Makro pro obnoveni promennych do stavu z puvodniho bloku */
#define FastItemRestore(TOPOP)															\
																						\
	for (int i = 0; i < TOPOP ; i++)													\
	{																					\
		if (top == 0)																	\
		{																				\
			--stackSector;																\
			stack = stackTable[stackSector];											\
			max >>= 1;																	\
			top = max;																	\
		}																				\
		--top;																			\
		*((TTmp *)(stack[top].ptr)) = stack[top].U.val;									\
	}																					\

/* Makro pro nalezeni Labelu v tabulce */
#define GetLabel(labeln, current)														\
{																						\
	TLabel *tmp = labelTable[labeln % LAB_SIZE];										\
																						\
	while (tmp != NULL)																	\
	{																					\
		if (tmp->label == (unsigned int) labeln)										\
		{																				\
			break;																		\
		}																				\
																						\
		tmp = tmp->next;																\
	}																					\
	current = (TInstr *) tmp->ptr;														\
}																						\

/* funcke pro pridani Labelu do tabulky */
int AddLabel(int label, TTmp *instr)
{
	TLabel *alptr;

	if ((alptr = malloc(sizeof(TLabel))) == NULL)
		return -1;

	alptr->label = label;
	alptr->ptr = (void *) instr;
	alptr->next = NULL;

	TLabel *ptr = labelTable[label % LAB_SIZE];

	if (ptr == NULL)
	{
		labelTable[label % LAB_SIZE] = alptr;
	}
	else
	{
		while (ptr != NULL)
		{
			if (ptr->next == NULL)
			{
				ptr->next = alptr;
				break;
			}
			ptr = ptr->next;
		}
	}

	return 0;
}

/* Funkce pro uvolneni tabulky Labelu */
void FreeLabel()
{
	TLabel *ptr = NULL;

	TLabel *alptr;
	for (int i = 0; i < LAB_SIZE; i++)
	{
		ptr = labelTable[i];
		while (ptr != NULL)
		{
			alptr = ptr;
			ptr = ptr->next;
			free(alptr);
		}
	}
}

/* Funkce pro Kontinualni alokaci pameti pro docasne promenne */
int TempInit()
{
	tempSector++;
	regSize <<= 1;
	if ((auxTemp = malloc(regSize * sizeof(TTmp))) == NULL)
		return -1;

	tempTable[tempSector] = auxTemp;

	return 0;
}

/* Makro pro uvolneni pameti alokavane pro docasne promenne */
void FreeTemp()
{
	for (int i = 0; i <= tempSector; i++)
	free(tempTable[i]);
}

/* Makro pro ziskani ukazatele docasne promenne */
#define GetTempPtr(n) (auxTemp + n)

/* Funkce pro uvolneni bloku pameti pro instrukce */
void FreeInstructions()
{
	for (int i = 0; memory[i] != NULL; i++)		// cyklus ktery projde ukazatele na bloky pameti alokovane pro instrukce
		free(memory[i]);						// a postupne uvolni pamet, dokud nenarazi na NULL pointer
}

/* Funkce AddInstruction
 *	je volana pro ulozeni instrukce instruction do dvousmerne vazaneho seznamu intrukci
 *	definovaneho pametovym prostorem code a pocatecni instrukci LABEL_START jako prvni se
 *	vyhodnoti offset se kterym je instrukce spjata pro spravne zarazeni semanticke zarazeni
 *	do seznamu instrukci instrukci
 *
 * offset:			posuv se kterym se instrukce zaradi do celkove logiky programu
 * instruction:		ukazatel na strukturu kde jsou ulozeny hodnoty pro novou instrukci
 */
void AddInstruction(int offset,  TInstr *instruction, int *errorCode)
{
	/* Tento blok instrukci projde operandy instrukci a ulozi do nich ukazatele do pameti na
	 * TTmp. Pripadne zkontroluje validnost ukazatelu do tabulky symbolu
	 */
	TOperant *operants = &(instruction->operA);
	int checkIndex = 0;

	TOperant *operant = NULL;
	for(; checkIndex < 3; checkIndex++)
	{
		operant = operants + checkIndex;
		if(operant->type == OP_VARTEMP)
		{
			static unsigned int offset = 0;					// slouzi pro pripadny navrat do predchoziho bloku
			if (operant->data >= regSize)
			{
				offset = operant->data;
				if (TempInit() == -1)
				{
					fprintf(stderr, "Chyba pri alokaci\n");
					*errorCode = ERROR_INTERPRET;
					return;
				}
			}

			operant->ptr = (TItem *) GetTempPtr(operant->data) - offset; //TODO
			if (operant->data < offset)
			{
				operant->ptr = (TItem *) (tempTable[tempSector - 1] + operant->data);
			}
		}
		else if(operant->type == OP_TITEM)
		{
			if (operant->ptr == NULL)
			{
				fprintf(stderr, "Pouziti nedeklarovane promenne\n");
				*errorCode = ERROR_SEM_NVAR;
				return;
			}
		}
	}

	/* Zkontroluje se zda je potrea pro instrukci alokovat dodatecne pamet */
	if (code - memory[sector] >= stackN)
	{
		if (CodeInit() != 0)
		{
			fprintf(stderr, "Error alocating memory\n");
			*errorCode = ERROR_INTERPRET;
			return;
		}
	}

	if (offset != 1)							// pokud je offset jiny nez +1
		ChangeInstructionPtr(offset);			// volame funkci pro posuv v seznamu instrukci

	*current = *instruction;					// a na toto misto ulozime instrukci nam predanou odkazem
    current->next = previous->next;				// ukazatel na nasledujici instrukci bude prozatim NULL
	current->previous = previous;				// ukazatel na predchozi instrukci bude adresa instrukce kterou jsme zpracovavali naposledy
	previous->next = current;					// nastavime ukazatel next predesle instrukce na soucasnou instrukci

	if (current->next != NULL)					// zkontrolujeme zda nebyla nova instrukce vlozena mezi dve jine
		current->next->previous = current;		// pokud ano nastavime ukazatel previous nasledujici instrukce na current

	/* Prida se label do tabulky symbolu */
	if (current->instr == LABEL)
	{
		if (current->operA.type == OP_TITEM)
			current->operA.ptr->data.iValue = current->operA.data;

		if (AddLabel(current->operA.data, (TTmp *) current) == -1)
		{
			fprintf(stderr, "Error alocating memory\n");
			*errorCode = ERROR_INTERPRET;
			return;
		}
	}

	code++;										// inkrementujeme codeCount, abychom pri dalsim volani funkce neprepisovali stejne misto v pameti
	previous = current;							// pro pristi volani teto funkce bude prave pridana instrukce brana jako previous
	current = code;								// do promenne current nyni ulozime adresu kde ulozime nasledujici instrukci
}

/* Funkce ChangeInstructionPtr
 * 	slouzi pro manipulaci s pozici pro ulozeni nove instrukce
 *
 * offset:		udava posuv v ramci ulozenych instrukci
 */
void ChangeInstructionPtr(int offset)
{
	while (offset != 0)							// cyklus ridici posuv v ramci instrukci
	{
		if (offset >= 1)						// pokud je offset kladny, instrukce se bude radit dopredu
		{
			if (previous->next != NULL)			// kontrola validnosti ukazatele na nasledujici instrukci
				previous = previous->next;
			offset--;
		}
		else									// naopak pokud je zaporny, bude instrukce razena dozadu
		{
			if (previous->previous != NULL)		// kontrola validnosti ukazatele na predeslou instrukci
				previous = previous->previous;
			offset++;
		}										// nulovy offset nenastane - znamenal by prepis soucasne instrukce
	}
}

/* Funkce codeInit
 *	pro inicializaci pameti pro ukladani seznamu instrukci. Funkce je schopna prealokovat ulozena data do vetsiho prostoru v pripade jejiho
 *	vicenasobneho volani
 */
int CodeInit()
{
	sector++;									// pridavame novy sektor
	stackN *= 2;								// inkrementujeme n�sobi� pro alokaci

	if ((memory[sector] = malloc(sizeof(TInstr) * stackN)) == NULL)// alokujeme misto v pameti pro instrukce
		return ERROR_INTERPRET;					// v pripade neuspechu alokace ukoncujeme funkci s chybovym kodem

	start = code = memory[sector];				// nastavime ukazatel do pro ukaldani instrukci na zacatek nove alokovaneho bloku

	if (sector == 0)							// pokud vytvarime prvni blok instrukci
	{
		start = memory[0];						// ulozime pocatecni adresu pro zacatek programu - neni nutne, avsak prehlednejsi
		previous = start;						// vytvorime prvni instrukci LABEL_START, zapichneme prvni ukazatel do pameti
		memset(start, 0, sizeof(TInstr));		// pamet vynulujeme
		code++;									// inkrementujeme ukazatel do kodu abychom nove vytvorenou instrukci neprepsali
		TempInit();
	}
												// code a current by mohl byt jeden ukazatel, kod je ale takto prehlednejsi
	current = code;								// nastavime soucasnou instrukci na zacatek kodoveho bloku

	return 0;									// po uspesne inicializaci vracime 0
}

/* Makro pro ulozeni hodnoty operandu do promenne */
#define SetVar(item, operB) 															\
																						\
	item->data = operB->ptr->data;														\
	item->type = operB->ptr->type & GET_VAR;											\
																						\
/* Makro pro nastaveni bloku promenne */
#define SetBlock(item, block)															\
																						\
	item->blok = block;																	\

/* Makro pro ulozeni hodnoty operandu do docasne promenne */
#define SetTemp(tmp, operB) 															\
																						\
	tmp->data = operB->ptr->data;														\
	tmp->type = operB->ptr->type | GET_LEX;												\

/* Funkce pro Ladici vypisy operandu typu TItem jednotlivych instrukci */
void TitemProfile(TItem *ptr)
{
	switch(ptr->type)
	{
		case(LEX_INT):

			printf("(int) %d", ptr->data.iValue);
			break;

		case(LEX_DOUBLE):

			printf("(dbl) %g", ptr->data.dValue);
			break;

		case(LEX_STRING):

			printf("\"%s\"", (char *)ptr->data.sValue);
			break;
		case (LEX_BOOL):
			printf("%s", (ptr->data.iValue == 0) ? "false" : "true");
			break;
		case(INTR_VAR_INT):
		case(INTR_VAR_DOUBLE):
		case(INTR_VAR_STRING):
		case(INTR_VAR_BOOL):
		case(INTR_VAR_NULL):
		case(LEX_ID):

			printf("%s", (char *)ptr->id);
			break;

		case(LEX_FUN_ID_EM):
		case(LEX_FUN_ID):


			printf("%s:", (char *)ptr->id);
			break;

		case(LEX_NULL):

			printf("(null)");
			break;

		default:

			printf("Unknown TItem type %d\n", ptr->type);
			retVal = ERROR_SEM_RUNT;
			return;
			break;
	}
	return;
}

/* Funkce pro Ladici vypisy operandu typu TTmp jednotlivych instrukci */
void TempProfile(TTmp *ptr)
{
	switch (ptr->type) {
	case (LEX_INT):
		printf(" %d", ptr->data.iValue);
		break;
	case (LEX_DOUBLE):
	{
		printf(" %.3g", ptr->data.dValue);
		break;
	}
	case (LEX_STRING):
		printf(" %s", ptr->data.sValue);
		break;
	case (LEX_BOOL):
		printf(" %s", (ptr->data.iValue == 0) ? "false" : "true");
		break;
	case (LEX_NULL):
		printf(" (null)");
		break;
	default:
#if DEBUG_INTERPRET
		printf(" new");
#endif
		break;
	}

	return;
}

/* Funkce pro Ladici vypisy operandu jednotlivych instrukci */
void PrintOper(TInstr *ptr)
{
	TOperant *tmp = &ptr->operA;

	for (int i = 0; i < 3; i++)							// jelikoz vime ze jsou pouze 3 operandy na instrukci muzeme pouzit for s tremi iteracemi
	{
		if (tmp->type == OP_TITEM && tmp->ptr != NULL)  // zkontrolujeme typ operandu a validnost ukazatele
		{
			TitemProfile(tmp->ptr);
		}
		else if (tmp->type == OP_VARTEMP)  				// zkontrolujeme typ operandu a validnost ukazatele
		{
			printf("T%d", tmp->data);

			if (i != 0)
				TempProfile((TTmp *)tmp->ptr);

		}
		else if (tmp->type == OP_LABEL)
		{
			printf("L%u", tmp->data);
		}
		else
		{
			tmp++;
			continue;
		}

		tmp++;											// prejdeme na dalsi operand
		if (tmp->type != OP_NONE && i < 2)
		{
			printf(", ");

			putchar('\t');
		}
		else break;
	}
	if (ptr->instr == CALL && ptr->operB.type == OP_TINSTR)
	{
		printf("%s\n",  ((TInstr *) ptr->operB.ptr)->operC.ptr->id);
	}
}

/* Funkce pro vyhodnoce volani vestavenych funkci */
int CallEmbedded(TItem *ptr, int *params, TItem * rets, TOperPtrContStack *varStack)
{
	TItem *ret = (TItem *) rets;					// nastavime ukazatel navratove hodnoty

	/* Vychozi chybovy stav je ERROR_SEM_MISS, pokud vse projde vporadku, nastavi se na 0 */
	int errorCode = ERROR_SEM_MISS;

	/* Porovna */
	switch (ptr->data.iValue)
	{
		case (PUT_STRING):
		{
			ret->type = LEX_INT;
			ret->data.iValue = PutString(varStack,params);
			errorCode = RESULT_OK;
			break;
		}
		case (GET_STRING):
		{
			ret->type = LEX_STRING;
			ret->data.sValue = GetString(&errorCode);
			break;
		}
		case (STRLEN):
		{
			if (*params >= 1)
			{
				TOperant *par1;
				TCSPop(*varStack, par1, TOperPtr);

				(*params)--;

				switch(par1->ptr->type) {
					case INTR_VAR_BOOL:
					case LEX_BOOL:
						ret->data.iValue = strlen(StrVal(&par1->ptr->data.iValue, par1->ptr->type, &errorCode));
						break;
					case INTR_VAR_INT:
					case LEX_INT:
						ret->data.iValue = strlen(StrVal(&par1->ptr->data.iValue, par1->ptr->type, &errorCode));
						break;
					case INTR_VAR_DOUBLE:
					case LEX_DOUBLE:
						ret->data.iValue = strlen(StrVal(&par1->ptr->data.dValue, par1->ptr->type, &errorCode));
						break;
					case INTR_VAR_STRING:
					case LEX_STRING:
						ret->data.iValue = strlen(par1->ptr->data.sValue);
						errorCode = RESULT_OK;
						break;
					case INTR_VAR_NULL:
					case LEX_NULL:
						ret->data.iValue = 0;
						errorCode = RESULT_OK;
						break;
				}
				ret->type = LEX_INT;
			}
			break;
		}
		case (INTVAL):
		{
			if (*params >= 1)
			{
				TOperant *par1;
				TCSPop(*varStack, par1, TOperPtr);

				(*params)--;
				ret->type = LEX_INT;
					switch(par1->ptr->type) {
					case INTR_VAR_BOOL:
					case LEX_BOOL:
						ret->data.iValue = IntVal(&par1->ptr->data.iValue, par1->ptr->type,&errorCode);
						break;
					case INTR_VAR_INT:
					case LEX_INT:
						ret->data.iValue = IntVal(&par1->ptr->data.iValue, par1->ptr->type,&errorCode);
						break;
					case INTR_VAR_DOUBLE:
					case LEX_DOUBLE:
						ret->data.iValue = IntVal(&par1->ptr->data.dValue, par1->ptr->type,&errorCode);
						break;
					case INTR_VAR_STRING:
					case LEX_STRING:
						ret->data.iValue = IntVal(par1->ptr->data.sValue, par1->ptr->type,&errorCode);
						//printf("\n\n\n%s\t%d\n\n\n",par1->ptr->data.sValue, IntVal(par1->ptr->data.sValue, par1->ptr->type,&res));
						break;
					case INTR_VAR_NULL:
					case LEX_NULL:
						ret->data.iValue = 0;
						errorCode = RESULT_OK;
				}
			}
			break;
		}
		case (DOUBLEVAL):
		{
			if (*params >= 1)
			{
				TOperant *par1;
				TCSPop(*varStack, par1, TOperPtr);

				(*params)--;
				ret->type = LEX_DOUBLE;
				switch(par1->ptr->type) {
					case INTR_VAR_BOOL:
					case LEX_BOOL:
						ret->data.dValue = DoubleVal(&par1->ptr->data.iValue, par1->ptr->type,&errorCode);
						break;
					case INTR_VAR_INT:
					case LEX_INT:
						ret->data.dValue = DoubleVal(&par1->ptr->data.iValue, par1->ptr->type,&errorCode);
						break;
					case INTR_VAR_DOUBLE:
					case LEX_DOUBLE:
						ret->data.dValue = DoubleVal(&par1->ptr->data.dValue, par1->ptr->type,&errorCode);
						break;
					case INTR_VAR_STRING:
					case LEX_STRING:
						ret->data.dValue = DoubleVal(par1->ptr->data.sValue, par1->ptr->type,&errorCode);
						break;
					case INTR_VAR_NULL:
					case LEX_NULL:
						ret->data.dValue = 0.0;
						errorCode = RESULT_OK;
				}
			}
			break;
		}
		case (BOOLVAL):
		{
			if (*params >= 1)
			{
				TOperant *par1;
				TCSPop(*varStack, par1, TOperPtr);

				(*params)--;
				ret->type = LEX_BOOL;
				switch(par1->ptr->type) {
					case INTR_VAR_BOOL:
					case LEX_BOOL:
						ret->data.iValue = BoolVal(&par1->ptr->data.iValue, par1->ptr->type);
						break;
					case INTR_VAR_INT:
					case LEX_INT:
						ret->data.iValue  = BoolVal(&(par1->ptr->data.iValue), par1->ptr->type);
						break;
					case INTR_VAR_DOUBLE:
					case LEX_DOUBLE:
						ret->data.iValue = BoolVal(&par1->ptr->data.dValue, par1->ptr->type);
						break;
					case INTR_VAR_STRING:
					case LEX_STRING:
						ret->data.iValue = BoolVal(par1->ptr->data.sValue, par1->ptr->type);
						break;
					case INTR_VAR_NULL:
					case LEX_NULL:
						ret->data.iValue = 0;
						break;
				}
				errorCode = RESULT_OK;
			}
			break;
		}
		case (STRVAL):
		{
			if (*params >= 1)
			{
				TOperant *par1;
				TCSPop(*varStack, par1, TOperPtr);

				(*params)--;
				ret->type = LEX_STRING;
				switch(par1->ptr->type) {
					case INTR_VAR_BOOL:
					case LEX_BOOL:
						ret->data.sValue = StrVal(&par1->ptr->data.iValue, par1->ptr->type,&errorCode);
						break;
					case INTR_VAR_INT:
					case LEX_INT:
						ret->data.sValue = StrVal(&par1->ptr->data.iValue, par1->ptr->type,&errorCode);
						break;
					case INTR_VAR_DOUBLE:
					case LEX_DOUBLE:
						ret->data.sValue = StrVal(&par1->ptr->data.dValue, par1->ptr->type,&errorCode);
						break;
					case INTR_VAR_STRING:
					case LEX_STRING:
						ret->data.sValue = StrVal(par1->ptr->data.sValue, par1->ptr->type,&errorCode);
						break;
					case INTR_VAR_NULL:
					case LEX_NULL:
						ret->data.sValue = "";
						errorCode = RESULT_OK;
				}
			}
			break;
		}
		case (GET_SUBSTRING):
		{
			if (*params >= 3)
			{
				TOperant *par1;
				TCSPop(*varStack, par1, TOperPtr);
				(*params)--;
				TOperant *par2;
				TCSPop(*varStack, par2, TOperPtr);
				(*params)--;
				TOperant *par3;
				TCSPop(*varStack, par3, TOperPtr);
				(*params)--;

				int type1 = par1->ptr->type;
				int type2 = par2->ptr->type;
				int type3 = par3->ptr->type;

				if ((type1 == LEX_STRING || type1 == INTR_VAR_STRING) && (type2 == LEX_INT || type2 == INTR_VAR_INT) && (type3 == LEX_INT || type3 == INTR_VAR_INT))
				{
					ret->type = LEX_STRING;
					ret->data.sValue = GetSubString(par1->ptr->data.sValue, par2->ptr->data.iValue, par3->ptr->data.iValue, &errorCode);
				}
			}
			break;
		}
		case (FIND_STRING):
		{
			if (*params >= 2)
			{
				TOperant *par1;
				TCSPop(*varStack, par1, TOperPtr);
				(*params)--;
				TOperant *par2;
				TCSPop(*varStack, par2, TOperPtr);
				(*params)--;

				int type1 = par1->ptr->type;
				int type2 = par2->ptr->type;

				if ((type1 == LEX_STRING || type1 == INTR_VAR_STRING) && (type2 == LEX_STRING || type2 == INTR_VAR_STRING))
				{
					ret->type = LEX_INT; // find_string vraci index zacatku podretezce
					ret->data.iValue = FindSubString(par1->ptr->data.sValue, par2->ptr->data.sValue);
					errorCode = RESULT_OK;
				}
			}
			break;
		}
		case (SORT_STRING):
		{
			if (*params >= 1)
			{
				TOperant *par1;
				TCSPop(*varStack, par1, TOperPtr);
				(*params)--;

				int type1 = par1->ptr->type;

				if ((type1 == LEX_STRING || type1 == INTR_VAR_STRING))
				{
					ret->type = LEX_STRING;
					ret->data.sValue = Mergesort(par1->ptr->data.sValue,&errorCode);
				}
			}
			break;
		}
	}

	*params = 0;

	return errorCode;
}

/* Funkce Interpret
 *	nastavi ukazatel na prvni instrukci prelozeneho programu, tyto instrukce zacne postupne vyhodnocovat a linearne se pohybovat na nasledujici
 *	instrukce v podobe seznamu.
 */
int Interpret()
{
	TOperPtrContStack varStack;
	//TStoreContStack storeStack;
	TCSInit(varStack, TOperPtr, retVal, 1000);

	register TInstr *cur = memory[0]->next;			// ukazatel pomoci ktereho se budeme pohybovat v seznamu instrukci
	int params = 0;
	int toPop = 0;

#if DEBUG_PARSER | DEBUG_INTERPRET
	printf("\t.start:\n");
#endif

	TOperant *opA;
	TOperant *opB;

	TItem *tmpA;
	TItem *tmpB;
	TItem *tmpC;

	/* Inicializace Zasobniku												*/
	/************************************************************************/
																			//
	unsigned int stackSector = 0;											//
	TStore *stackTable[FAST_SIZE] = {NULL};									//
	TStore *stack = NULL;													//
	unsigned int top = 0;													//
	unsigned int max = FAST_INIT;											//
																			//
	if ((stackTable[0] = malloc(FAST_INIT * sizeof(TStore))) == NULL)       //
	{  																		//
		retVal= ERROR_INTERPRET; 											//
	}																		//
																			//
	stack = stackTable[0];													//
																			//
	//FastInit(sector, sectorTable, stackSector, top, max, retVal, TStore); //
	/************************************************************************/

	while (cur != NULL && retVal == 0)
	{
		opA = &(cur->operA);			// nastavime ukazatel tmp na operand A soucasne zpracovavane instrukce
		opB = &(cur->operB);			// nastavime ukazatel tmp na operand B soucasne zpracovavane instrukce

/* Prepinac pro ladici vypisy 3ad kodu */
#if DEBUG_INTERPRET | DEBUG_PARSER
		if (cur->instr == LABEL)
			putchar('\n');

		printf("\t",(void *) cur);

		if (cur->instr != LABEL)
			printf("  %s\t", instruct[cur->instr]);

		PrintOper(cur);
		putchar('\n');
#endif

#ifndef DEBUG_PARSER

		switch (cur->instr)
		{
			case (PUSH):///////////////////////////////////////////////////////
			{
				++params;								// inkrementujeme pocet parametru pro funkci

				/* V pripade pushovani promenne zkontrolujeme blok */
				if (((opA->ptr->type & GET_LEX) == 0) && opA->ptr->blok != block)
				{
					fprintf(stderr, "Use of undeclared variable\n");
					retVal = ERROR_SEM_NVAR;
					break;
				}

				TCSPush(varStack, opA, retVal, TOperPtr); // vlozime promennou na zasobnik

				break;
			}
			case (POP):////////////////////////////////////////////////////////
			{
				tmpA = cur->operA.ptr;					// nastavime ukazatel tmp na operand A

				/* Kontrolujeme zda na zasobniku je dostatecny pocet promennych */
				if (params == 0)
				{
					fprintf(stderr, "Function call expects more arguments!\n");
					retVal = ERROR_SEM_MISS;
					break;
				}

				params--;								// dekrementujeme pocet parametru pro funkci

				/*     Zkontrolujeme spravny pocet parametru funkce     */
				if (cur->next->instr == POP && params == 0)
				{
					fprintf(stderr, "Nedostatecny pocet parametru pri volani funkce!\n");
					retVal = ERROR_SEM_MISS;
					break;
				}
				else if (cur->next->instr != POP)
				{
					params = 0;
				}

				/* Zkontrolujeme zda nedoslo k popnuti promenne vicekrat */
				if (tmpA->blok == block)
				{
					fprintf(stderr, "Function definition has invalid arguments!\n");
					retVal = ERROR_SEM_RUNT;
					break;
				}

				toPop++;								// inkrementujeme pocet promennych pro obnoveni

				/*     Ulozeni puvodni hodnoty promenne na zasobnik     */
				/********************************************************/
				FastItemStore(tmpA);
				/********************************************************/

				/*  	 Nacteme hodnotu promenne ze zasoniku 		    */
				/********************************************************/
				TCSPop(varStack, opB, TOperPtr);
				SetVar(tmpA, opB);
				SetBlock(tmpA, block);
				/********************************************************/

				break;
			}
			case (JMPN): /*****************************************************/
			{
				/*  Zkontrolujeme hodnotu promenne zda se data lisi od nuly */
				if (opA->ptr->data.iValue != 0)
				{
					/* Pokud ano, je jeste moznost ze se jedna o retezec */
					if (opA->ptr->type == LEX_STRING)
					{
						 if (opA->ptr->data.sValue[0] != '\0')
							 break;
					}
					/* Zkontrolujeme jeste zda se nejedna o null - nemusi byt vynulovan */
					else
					{
						if (opA->ptr->type != LEX_NULL)
							break;
					}
				}

				/*  Skocime na instrukci s Labelem JMPNu */
				if (opB->type == OP_TINSTR)
				{
					cur = (TInstr *) opB->ptr;
				}
				else
				{
					/* Nacteme label a ulozime ho do operandu B instrucke JMPN */
					GetLabel(opB->data, cur);
					opB->ptr = (TItem *) cur;
					opB->type = OP_TINSTR;
				}

				break;
			}
			case (ADD):////////////////////////////////////////////////////////
			{
				/* Nacteme promenne se kterymi budeme pracovat a jejich typy */
				tmpA = cur->operA.ptr;
				tmpB = cur->operB.ptr;
				tmpC = cur->operC.ptr;

				int typeB = tmpB->type | GET_LEX;
				int typeC = tmpC->type | GET_LEX;

				/* Pokud ukladame do promenne, je potreba uchovat jeji hodnotu v pripade vnoreni */
				if (opA->type == OP_TITEM) {
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
					}
				}

				/* Zkonrtolujeme zda jsou pouzite promenne definovany v danem bloku */
				if ((((tmpB->type & GET_LEX) == 0) && tmpB->blok != block) ||
					(((tmpC->type & GET_LEX) == 0) && tmpC->blok != block)) {
					fprintf(stderr, "Pouziti nedeklarovane promenne\n");
					retVal = ERROR_SEM_NVAR;
					break;
				}
				/* Zkontrolujeme zda je operace provadena s vhodnymi operandy */
				if ((typeB != LEX_INT && typeB != LEX_DOUBLE)
						|| (typeC != LEX_INT && typeC != LEX_DOUBLE)) {
					fprintf(stderr,
							"Operator + je definovan pouze nad typy double a int!\n");
					retVal = ERROR_SEM_ARRE;
					break;
				}

				/* Na zaklade typu operandu vypocitame hodnotu a nastavime typ promenne */
				if (typeB == LEX_DOUBLE) {
					tmpA->type = LEX_DOUBLE;

					if (typeC == LEX_DOUBLE)
						tmpA->data.dValue = tmpB->data.dValue + tmpC->data.dValue;
					else
						tmpA->data.dValue = tmpB->data.dValue + tmpC->data.iValue;
				} else if (typeC == LEX_DOUBLE) {
					tmpA->type = LEX_DOUBLE;

					tmpA->data.dValue = tmpB->data.iValue + tmpC->data.dValue;
				} else {
					tmpA->type = LEX_INT;
					tmpA->data.iValue = tmpB->data.iValue + tmpC->data.iValue;
				}

				/* Pokud se jedna o TItem, nastavime spravne take typ a blok */
				if (opA->type == OP_TITEM) {
					tmpA->type &= GET_VAR;
					tmpA->blok = block;
				}

				break;
			}
			case (SUB):		////////////////////////////////////////////////////////
			{
				/* Nacteme promenne se kterymi budeme pracovat a jejich typy */
				tmpA = cur->operA.ptr;
				tmpB = cur->operB.ptr;
				tmpC = cur->operC.ptr;

				int typeB = tmpB->type | GET_LEX;
				int typeC = tmpC->type | GET_LEX;

				/* Pokud ukladame do promenne, je potreba uchovat jeji hodnotu v pripade vnoreni */
				if (opA->type == OP_TITEM) {
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
					}
				}

				/* Zkonrtolujeme zda jsou pouzite promenne definovany v danem bloku */
				if ((((tmpB->type & GET_LEX) == 0) && tmpB->blok != block) ||
					(((tmpC->type & GET_LEX) == 0) && tmpC->blok != block)) {
					fprintf(stderr, "Pouziti nedeklarovane promenne\n");
					retVal = ERROR_SEM_NVAR;
					break;
				}
				/* Zkontrolujeme zda je operace provadena s vhodnymi operandy */
				if ((typeB != LEX_INT && typeB != LEX_DOUBLE)
						|| (typeC != LEX_INT && typeC != LEX_DOUBLE)) {
					fprintf(stderr,
							"Operator - je definovan pouze nad typy double a int %d %d!\n",
							typeB, typeC);
					retVal = ERROR_SEM_ARRE;
					break;
				}

				if (typeB == LEX_DOUBLE) {
					tmpA->type = LEX_DOUBLE;

					if (typeC == LEX_DOUBLE)
						tmpA->data.dValue = tmpB->data.dValue - tmpC->data.dValue;
					else
						tmpA->data.dValue = tmpB->data.dValue - tmpC->data.iValue;
				} else if (typeC == LEX_DOUBLE) {
					tmpA->type = LEX_DOUBLE;

					tmpA->data.dValue = tmpB->data.iValue - tmpC->data.dValue;
				} else {
					tmpA->type = LEX_INT;
					tmpA->data.iValue = tmpB->data.iValue - tmpC->data.iValue;
				}

				if (opA->type == OP_TITEM)
				{
					tmpA->type &= GET_VAR;
					tmpA->blok = block;
				}

				break;
			}
			case (MOVE):////////////////////////////////////////////////////////
			{
				tmpA = cur->operA.ptr;
				tmpB = cur->operB.ptr;


				/* Zkonrtolujeme zda je pouzita promenne definovana v danem bloku */
				if ((((opB->ptr->type & GET_LEX) == 0) && opB->ptr->blok != block))
				{
					fprintf(stderr, "Pouziti nedeklarovane promenne\n");
					retVal = ERROR_SEM_NVAR;
					break;
				}

				/* Nastavime promennou podle toho zda se jedna o docasnou nebo trvalou */
				if (opA->type == OP_VARTEMP)
				{
					SetTemp(tmpA, opB);
				}
				else
				{
					/* Pokud ukladame do promenne, je potreba uchovat jeji hodnotu v pripade vnoreni */
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
					}

					/* Nastavime hodnotu promenne a blok */
					SetVar(tmpA, opB);
					SetBlock(tmpA, block);
				}

				break;
			}
			case (CALL):///////////////////////////////////////////////////////
			{
				tmpA = cur->operA.ptr;
				tmpB = cur->operB.ptr;

				/* Pokud je navratova promenna nedefinovana v danem bloku, ulozime ji na zasobnik */
				if (tmpA->blok != block && block > 0)
				{
					++toPop;
					FastItemStore(tmpA);
				}

				/* Inkrementujeme blok ve kterem se nachazime */
				block++;

				/* Pokud uz byla funkce volana jednou, mame ukazatel primo v operandu */
				if (opB->type == OP_TINSTR)
				{
					FastStopStore(toPop, opA->ptr, cur->next);

					toPop = 0;

					cur = (TInstr *) opB->ptr;
					break;
				}
				/* Pokud ne, je potreba zkontrolovat zda byla definovana a nacist ukazatel */
				else if (opB->ptr->type == LEX_FUN_ID)
				{
					FastStopStore(toPop, opA->ptr, cur->next);

					toPop = 0;

					/* Zkontrolujeme zda byla funkce definovana */
					if (opB->ptr->data.iValue == 0)
					{
						fprintf(stderr, "Volani nedeklarovane funkce! Koncim program\n"); //NULL PTR");
						retVal = ERROR_SEM_NFUN;
						break;
					}

					/* Ulozime si ukazatel na funkci do operandu instrukce */
					GetLabel(opB->ptr->data.iValue, cur);

#if DEBUG_INTERPRET
					cur->operC.ptr = opB->ptr;
#endif

					opB->ptr = (TItem *) cur;
					opB->type = OP_TINSTR;

					break;
				}
				/* Pokud se jedna o vestavenou funkci, provede se jina rutina */
				else if (opB->ptr->type == LEX_FUN_ID_EM)
				{
					/* Dekrementujeme blok */
					block--;

					TItem ret;

					if ((retVal = CallEmbedded(opB->ptr, &params, &ret, &varStack)) != 0)
					{
						if (retVal == ERROR_SEM_MISS)
							fprintf(stderr, "Nevhodne parametry pri volani vestavene funkce! %s\n", opB->ptr->id);
						else
							fprintf(stderr, "Doslo k chybe pri volani vestavene funkce!\n");

						break;
					}

					/* Nastavime blok a typ promenne */
					tmpA->data = ret.data;
					tmpA->blok = block;
					tmpA->type = ret.type & GET_VAR;
				}
				break;
			}
			case (RET)	:////////////////////////////////////////////////////////
			{
				TTmp ret = *((TTmp *)opA->ptr);

				/* Kontrolujeme zda je promenna kterou vracime validni - tzn deklarovana s definovanou hodnotou v danem bloku */
				if (opA->type == OP_TITEM && ((opA->ptr->type & GET_LEX) == 0) && opA->ptr->blok != block)
				{
					fprintf(stderr, "Pouziti nedeklarovane promenne\n");
					retVal = ERROR_SEM_NVAR;
					break;
				}

				if (block == 0)
				{
					cur = NULL;
					continue;
				}

				block--;

				FastItemRestore(toPop);
				FastStopRestore(toPop, tmpB, cur);

				tmpB->blok = block;

				/* Vratime uchovanou navratovou hodnotu a typ */
				if (opA->type == OP_TITEM || opA->type == OP_VARTEMP)
				{
					((TTmp *)tmpB)->data = ret.data;
					tmpB->type = ret.type & GET_VAR;
				}
				/* Pokud return nic nevraci, nastavim promennou jako NULL */
				else
				{
					tmpB->type = INTR_VAR_NULL;
				}

				continue;
			}
			case (JMP):////////////////////////////////////////////////////////
			{
				/* Pokud je hodnota promenne nulova nebo se jedna o NULL */
				if (opB->type == OP_VARTEMP && (opB->ptr->data.iValue == 0 || opB->ptr->type == LEX_NULL))
					break;

				/* Skocime bud na jiz prednactenou instrukci nebo instrukci nacteme do operandu a pote skocime */
				if (opA->type == OP_TINSTR)
				{
					cur = ((TInstr *) opA->ptr)->next;
					continue;
				}
				else
				{
					GetLabel(opA->data, cur);
					opA->ptr = (TItem *) cur;
					opA->type = OP_TINSTR;
				}

				break;
			}
			case (MUL):////////////////////////////////////////////////////////
			{
				/* Nacteme promenne a jejich typy */
				tmpA = cur->operA.ptr;
				tmpB = cur->operB.ptr;
				tmpC = cur->operC.ptr;

				int typeB = tmpB->type | GET_LEX;
				int typeC = tmpC->type | GET_LEX;

				/* Pokud ukladame do promenne, je potreba uchovat jeji hodnotu v pripade vnoreni */
				if (opA->type == OP_TITEM) {
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
					}
				}

				if ((((tmpB->type & GET_LEX) == 0) && tmpB->blok != block) ||
					(((tmpC->type & GET_LEX) == 0) && tmpC->blok != block))
				{
					fprintf(stderr, "Pouziti nedeklarovane promenne\n");
					retVal = ERROR_SEM_NVAR;
					break;
				}
				if ((typeB != LEX_INT && typeB != LEX_DOUBLE) ||
					(typeC != LEX_INT && typeC != LEX_DOUBLE))
				{
					fprintf(stderr, "Operator * je definovan pouze nad typy double a int! %d %d!\n", typeB, typeC);
					retVal = ERROR_SEM_ARRE;
					break;
				}

				if (typeB == LEX_DOUBLE)
				{
					tmpA->type = LEX_DOUBLE;

					if (typeC == LEX_DOUBLE)
						tmpA->data.dValue = tmpB->data.dValue * tmpC->data.dValue;
					else
						tmpA->data.dValue = tmpB->data.dValue * tmpC->data.iValue;
				}
				else if (typeC == LEX_DOUBLE)
				{
					tmpA->type = LEX_DOUBLE;

					tmpA->data.dValue = tmpB->data.iValue * tmpC->data.dValue;
				}
				else
				{
					tmpA->type = LEX_INT;
					tmpA->data.iValue = tmpB->data.iValue * tmpC->data.iValue;
				}

				if (opA->type == OP_TITEM)
				{
					tmpA->type &= GET_VAR;
					tmpA->blok = block;
				}

				break;
			}
			case (DIV):////////////////////////////////////////////////////////
			{
				tmpA = cur->operA.ptr;			// nastavime ukazatel tmp na operand A soucasne zpracovavane instrukce
				tmpB = cur->operB.ptr;			// nastavime ukazatel tmp na operand B soucasne zpracovavane instrukce
				tmpC = cur->operC.ptr;

				int typeB = tmpB->type | GET_LEX;
				int typeC = tmpC->type | GET_LEX;


				/* Pokud ukladame do promenne, je potreba uchovat jeji hodnotu v pripade vnoreni */
				if (opA->type == OP_TITEM) {
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
					}
				}

				if ((((tmpB->type & GET_LEX) == 0) && tmpB->blok != block) ||
					(((tmpC->type & GET_LEX) == 0) && tmpC->blok != block))
				{
					fprintf(stderr, "Pouziti nedeklarovane promenne\n");
					retVal = ERROR_SEM_NVAR;
					break;
				}
				if ((typeB != LEX_INT && typeB != LEX_DOUBLE) ||
					(typeC != LEX_INT && typeC != LEX_DOUBLE))
				{
					fprintf(stderr, "Operator / je definovan pouze nad typy double a int!\n");
					retVal = ERROR_SEM_ARRE;
					break;
				}

				if (((typeC == LEX_DOUBLE) ? tmpC->data.dValue : tmpC->data.iValue) == 0)
				{
					fprintf(stderr, "Doslo k deleni nulou!\n");
					retVal = ERROR_SEM_ZERO;
					break;
				}

				if (opA->type == OP_TITEM)
				{
					tmpA->type = INTR_VAR_DOUBLE;
					tmpA->blok = block;
				}
				else
					tmpA->type = LEX_DOUBLE;

				tmpA->data.dValue = ((typeB == LEX_DOUBLE) ? tmpB->data.dValue : (double) tmpB->data.iValue) /
								   ((typeC == LEX_DOUBLE) ? tmpC->data.dValue : (double) tmpC->data.iValue);

				break;
			}

			case (SAME):///////////////////////////////////////////////////////
			{
				tmpA = cur->operA.ptr;			// nastavime ukazatel tmp na operand A soucasne zpracovavane instrukce
				tmpB = cur->operB.ptr;			// nastavime ukazatel tmp na operand B soucasne zpracovavane instrukce
				tmpC = cur->operC.ptr;

				int typeB = tmpB->type | GET_LEX;
				int typeC = tmpC->type | GET_LEX;

				/* Pokud ukladame do promenne, je potreba uchovat jeji hodnotu v pripade vnoreni */
				if (opA->type == OP_TITEM) {
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
						tmpA->blok = block;
					}
					tmpA->type = INTR_VAR_BOOL;
				}
				else
					tmpA->type = LEX_BOOL;

				if (typeB == typeC)
				{
					if (typeB == LEX_INT || typeB == LEX_DOUBLE || typeB == LEX_BOOL)
						tmpA->data.iValue = (typeB != LEX_DOUBLE) ? (tmpB->data.iValue == tmpC->data.iValue) : (tmpB->data.dValue == tmpC->data.dValue);
					else if (typeB == LEX_STRING)
						tmpA->data.iValue = (strcmp(tmpB->data.sValue, tmpC->data.sValue) == 0) ? true : false;
					else
						tmpA->data.iValue = true;
				}
				else
					tmpA->data.iValue = false;

				break;
			}
			case (ISDIF)://////////////////////////////////////////////////////
			{
				tmpA = cur->operA.ptr;			// nastavime ukazatel tmp na operand A soucasne zpracovavane instrukce
				tmpB = cur->operB.ptr;			// nastavime ukazatel tmp na operand B soucasne zpracovavane instrukce
				tmpC = cur->operC.ptr;

				int typeB = tmpB->type | GET_LEX;
				int typeC = tmpC->type | GET_LEX;

				if (opA->type == OP_TITEM) {
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
						tmpA->blok = block;
					}
					tmpA->type = INTR_VAR_BOOL;
				}
				else
					tmpA->type = LEX_BOOL;

				if (typeB == typeC)
				{
					if (typeB == LEX_INT || typeB == LEX_DOUBLE || typeB == LEX_BOOL)
						tmpA->data.iValue = (typeB != LEX_DOUBLE) ? (tmpB->data.iValue != tmpC->data.iValue) : (tmpB->data.dValue != tmpC->data.dValue);
					else if (typeB == LEX_STRING)
						tmpA->data.iValue = (strcmp(tmpB->data.sValue, tmpC->data.sValue) != 0) ? true : false;
					else
						tmpA->data.iValue = false;
				}
				else
				{
					tmpA->data.iValue = true;
				}

				break;
			}
			case (ISBE):///////////////////////////////////////////////////////
			{
				tmpA = cur->operA.ptr;			// nastavime ukazatel tmp na operand A soucasne zpracovavane instrukce
				tmpB = cur->operB.ptr;			// nastavime ukazatel tmp na operand B soucasne zpracovavane instrukce
				tmpC = cur->operC.ptr;

				int typeB = tmpB->type | GET_LEX;
				int typeC = tmpC->type | GET_LEX;

				if (opA->type == OP_TITEM) {
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
						tmpA->blok = block;
					}
					tmpA->type = INTR_VAR_BOOL;
				}
				else
					tmpA->type = LEX_BOOL;

				if (typeB == typeC)
				{
					if (typeB == LEX_INT || typeB == LEX_DOUBLE || typeB == LEX_BOOL)
						tmpA->data.iValue = (typeB != LEX_DOUBLE) ? (tmpB->data.iValue >= tmpC->data.iValue) : (tmpB->data.dValue >= tmpC->data.dValue);
					else if (typeB == LEX_STRING)
						tmpA->data.iValue = (strcmp(tmpB->data.sValue, tmpC->data.sValue) >= 0) ? true : false;
					else
						tmpA->data.iValue = true;
				}
				else
				{
					fprintf(stderr, "Relation operators are defined only at two values of the same type!\n");
					retVal = ERROR_SEM_ARRE;
					break;
				}

				break;
			}
			case (ISSE):///////////////////////////////////////////////////////
			{
				tmpA = cur->operA.ptr;			// nastavime ukazatel tmp na operand A soucasne zpracovavane instrukce
				tmpB = cur->operB.ptr;			// nastavime ukazatel tmp na operand B soucasne zpracovavane instrukce
				tmpC = cur->operC.ptr;


				int typeB = tmpB->type | GET_LEX;
				int typeC = tmpC->type | GET_LEX;

				if (opA->type == OP_TITEM) {
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
						tmpA->blok = block;
					}
					tmpA->type = INTR_VAR_BOOL;
				}
				else
					tmpA->type = LEX_BOOL;

				if (typeB == typeC)
				{
					if (typeB == LEX_INT || typeB == LEX_DOUBLE || typeB == LEX_BOOL)
						tmpA->data.iValue = (typeB != LEX_DOUBLE) ? (tmpB->data.iValue <= tmpC->data.iValue) : (tmpB->data.dValue <= tmpC->data.dValue);
					else if (typeB == LEX_STRING)
						tmpA->data.iValue = (strcmp(tmpB->data.sValue, tmpC->data.sValue) <= 0) ? true : false;
					else
						tmpA->data.iValue = true;
				}
				else
				{
					fprintf(stderr, "Relation operators are defined only at two values of the same type!\n");
					retVal = ERROR_SEM_ARRE;
					break;
				}


				break;
			}
			case (ISSM):///////////////////////////////////////////////////////
			{
				tmpA = cur->operA.ptr;			// nastavime ukazatel tmp na operand A soucasne zpracovavane instrukce
				tmpB = cur->operB.ptr;			// nastavime ukazatel tmp na operand B soucasne zpracovavane instrukce
				tmpC = cur->operC.ptr;

				int typeB = tmpB->type | GET_LEX;
				int typeC = tmpC->type | GET_LEX;


				if (opA->type == OP_TITEM) {
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
						tmpA->blok = block;
					}
					tmpA->type = INTR_VAR_BOOL;
				}
				else
					tmpA->type = LEX_BOOL;


				if (typeB == typeC)
				{
					if (typeB == LEX_INT || typeB == LEX_DOUBLE || typeB == LEX_BOOL)
						tmpA->data.iValue = (typeB != LEX_DOUBLE) ? (tmpB->data.iValue < tmpC->data.iValue) : (tmpB->data.dValue < tmpC->data.dValue);
					else if (typeB == LEX_STRING)
						tmpA->data.iValue = (strcmp(tmpB->data.sValue, tmpC->data.sValue) < 0) ? true : false;
					else
						tmpA->data.iValue = false;
				}
				else
				{
					fprintf(stderr, "Relation operators are defined only at two values of the same type!\n");
					retVal = ERROR_SEM_ARRE;
					break;
				}

				break;
			}
			case (ISBI):///////////////////////////////////////////////////////
			{
				tmpA = cur->operA.ptr;			// nastavime ukazatel tmp na operand A soucasne zpracovavane instrukce
				tmpB = cur->operB.ptr;			// nastavime ukazatel tmp na operand B soucasne zpracovavane instrukce
				tmpC = cur->operC.ptr;


				int typeB = tmpB->type | GET_LEX;
				int typeC = tmpC->type | GET_LEX;

				if (opA->type == OP_TITEM) {
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
						tmpA->blok = block;
					}
					tmpA->type = INTR_VAR_BOOL;
				}
				else
					tmpA->type = LEX_BOOL;


				if (typeB == typeC)
				{
					if (typeB == LEX_INT || typeB == LEX_DOUBLE || typeB == LEX_BOOL)
						tmpA->data.iValue = (typeB != LEX_DOUBLE) ? (tmpB->data.iValue > tmpC->data.iValue) : (tmpB->data.dValue > tmpC->data.dValue);
					else if (typeB == LEX_STRING)
						tmpA->data.iValue = (strcmp(tmpB->data.sValue, tmpC->data.sValue) > 0) ? true : false;
					else
						tmpA->data.iValue = false;
				}
				else
				{
					fprintf(stderr, "Relation operators are defined only at two values of the same type!\n");
					retVal = ERROR_SEM_ARRE;
					break;
				}

				break;
			}
			case (CON):////////////////////////////////////////////////////////
			{
				tmpA = cur->operA.ptr;			// nastavime ukazatel tmp na operand A soucasne zpracovavane instrukce
				tmpB = cur->operB.ptr;			// nastavime ukazatel tmp na operand B soucasne zpracovavane instrukce
				tmpC = cur->operC.ptr;


				int typeB = tmpB->type |= GET_LEX;
				int typeC = tmpC->type |= GET_LEX;

				if (opA->type == OP_TITEM) {
					if (tmpA->blok != block && block > 0)
					{
						++toPop;
						FastItemStore(tmpA);
						tmpA->blok = block;
					}
					tmpA->type = INTR_VAR_STRING;
				}
				else
					tmpA->type = LEX_STRING;

				if (typeB != LEX_STRING && typeC != LEX_STRING)
				{
					fprintf(stderr, "Concatenation must have at least one string operand!\n");
					retVal = ERROR_SEM_ARRE;
					break;
				}

				char *bString = StrVal((typeB == LEX_STRING) ? (void *)(tmpB->data.sValue) : (void *)&(tmpB->data), tmpB->type, &retVal);
				char *cString = StrVal((typeC == LEX_STRING) ? (void *)(tmpC->data.sValue) : (void *)&(tmpC->data), tmpC->type, &retVal);

				int bLen=strlen(bString); // ulozeni delky retezece do promenne, bude pouzita nekolikrat
				int cLen=strlen(cString);

				if((tmpA->data.sValue = malloc((bLen + cLen + 1)*sizeof(char)))==NULL)
				{
					fprintf(stderr, "Concatenation failure!\n");
					retVal = ERROR_INTERPRET;
					break;
				}
				strncpy(tmpA->data.sValue, bString, bLen);
				strncpy(tmpA->data.sValue + bLen, cString, cLen);
				tmpA->data.sValue[bLen+cLen]='\0';

				tmpA->type = LEX_STRING;
				if (opA->type == OP_TITEM)
				{
					tmpA->type &= GET_VAR;
					tmpA->blok = block;
				}
				break;
			}
			case (LABEL)://////////////////////////////////////////////////////
			{
				break;
			}
			default:
				break;
		}
#endif

		cur = cur->next;							// prejdeme na dalsi instrukci*/
	}

#if DEBUG_INTERPRET | DEBUG_PARSER
	printf("\nUvolnuji pamet\n");
#endif

	TCSFree(varStack);

	/***************************************************************************/
	for (int i = 0; i < FAST_INIT && stackTable[i] != NULL; i++)
	{
		free(stackTable[i]);
		stackTable[i] = NULL;
	}
	/***************************************************************************/

	return retVal;									// po uspesne interpretaci vracime 0
}
