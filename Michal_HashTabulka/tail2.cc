// tail2.cc
// Řešení IJC-DU2, příklad a), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//--------------------------------------------------------
#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <string.h>

using namespace std;

bool tail(const char *file, unsigned int countLines, bool lastLines)
{   
    deque<string> container;
    
    ifstream infile(file, ios_base::in);
    string line;
    
    if(!infile.is_open() && file != NULL)
        return false;
          
    while(getline(file != NULL ? infile : cin, line))
        container.push_back(line);
   
    if(infile.is_open())
        infile.close();

    countLines = countLines > container.size() ? container.size() : countLines;
    
    if(countLines < 1)
        return true;
    
    deque<string>::iterator inter = lastLines ? container.end() - countLines : container.begin() + countLines - 1;
     
    for(;inter != container.end(); inter++)
        cout << *inter << endl;  
      
    return true;   
}

long int strToInt(const char* str)
{
    long int number = 0;
    
    while(*str != '\0')
    {
        if(isdigit(*str))
        {
          number = number * 10 + (*str - '0');
        }else if(*str != '+'){
          return -1;
        }
        
        str++;
    }
    
    return number;
}

int main(int argc, char* argv[])
{
  ios::sync_with_stdio(false);
  long int count = 10;
  bool lastLines = true;
  char *filename = NULL;
   
  for(int i = 1; (i < 4 && i < argc); i++)
  {
      if(strcmp(argv[i], "-n") == 0)
      {
          if(i+1 > 3 || i+1 >= argc || (count = strToInt(argv[++i])) == -1)
          {
            cerr << "Za paremetrem -n ocekavano cislo." << endl;
            return 1;
          }   

          lastLines = argv[i][0] == '+' ? false : true;       
      }else if(filename == NULL){
          filename = argv[i];  
      }else{
          cerr << "Neznamy parametr." << endl;
          return 1;
      }
  } 
  
  if(count < 1)
      return 0;
      
  if(!tail(filename, (int unsigned)count, lastLines))
  {
      cerr << "Neleze otevrit soubor." << endl;
      return 1;
  }
  
  return 0;
}
