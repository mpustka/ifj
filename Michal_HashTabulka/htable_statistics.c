// htable_statistics.c
// Řešení IJC-DU2, příklad b), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//--------------------------------------------------------
#include "htable.h"

// tisk průměrné a min/max. délky seznamů
void htable_statistics(struct htable_t *t)        
{
    unsigned int min = 0, max = 0, sum = 0, count = 0;
    float average = 0;
    
    for(unsigned int i = 0; i < t->size; i++)
    {
        struct htable_listitem *listitem = t->ptr[i];
        int subsum = 0;
        for(;listitem != NULL; listitem = listitem->next);
            subsum++;
        
        if(min > subsum)
            min = subsum;
            
        if(max < subsum)
            max = subsum;    
              
        count++;
        sum += subsum;
    }

    average = (float)sum / count;

    printf("\nMaximalni seznam: %d \nMimalni seznam: %d \n", max, min);
    printf("Prumerna velikost seznamu %f \n", average);
}
