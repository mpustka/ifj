// htable_free.c
// Řešení IJC-DU2, příklad b), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//--------------------------------------------------------
#include "htable.h"

//zrušení celé tabulky (volá clear)
void htable_free(struct htable_t *t)
{   
    htable_clear(t);
    
    if(t != NULL)
        free(t);
}
