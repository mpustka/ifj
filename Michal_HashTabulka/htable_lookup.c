// htable_lookup.c
// Řešení IJC-DU2, příklad a), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//--------------------------------------------------------
#include "htable.h"

// vyhledávání - viz dále
struct htable_listitem * htable_lookup(struct htable_t *t, const char *key)
{
    unsigned int index = hash_function(key, t->size);
    struct htable_listitem *listitem = t->ptr[index];
    struct htable_listitem *tempitem = NULL, *preitem = NULL;
        
    for(;(listitem != NULL && strcmp(listitem->key, key) != 0); listitem = listitem->next)
    	preitem = listitem;
    
	if(listitem != NULL)
        return listitem; 
    
    if((tempitem = malloc(sizeof(struct htable_listitem))) == NULL)
        return NULL;
             
   	if(t->ptr[index] == NULL)
    {
        listitem = tempitem;
        t->ptr[index] = listitem;
    }else{
        preitem->next = tempitem;
        listitem = tempitem;
    }   
         
    listitem->data = 0;
    listitem->key = NULL;
    listitem->next = NULL;
     
    return listitem;
}
