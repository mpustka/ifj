// htable_clear.c
// Řešení IJC-DU2, příklad b), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//--------------------------------------------------------
#include "htable.h"

// zrušení všech položek v tabulce
void htable_clear(struct htable_t *t)
{
    for(unsigned int i = 0; i < t->size; i++)
    {
        struct htable_listitem *listitem = t->ptr[i], *delete_item = NULL;
        
        while(listitem != NULL)
        {
            free(listitem->key);
            
            delete_item = listitem;
            listitem = listitem->next;
            
            free(delete_item);
        }
        
        t->ptr[i] = NULL;     
    } 
}
