// htable.h
// Řešení IJC-DU2, příklad b), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//rozhrani pro knihovnu libhtable.so a libhtable.a
//--------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>

#define COUNT_LIST 10000
#define LENGHT_WORD 255

struct htable_listitem
{
    char *key;
    unsigned int data;
    struct htable_listitem *next;
};

struct htable_t
{
    unsigned int size;
    struct htable_listitem * ptr[];
};

// pro vytvoření a inicializaci tabulky
struct htable_t * htable_init(const unsigned int size); 
// vyhledávání - viz dále
struct htable_listitem * htable_lookup(struct htable_t *t, const char *key);
// volání funkce pro každý prvek
void htable_foreach(struct htable_t *t, void (*execute)(struct htable_listitem item));
// vyhledání a zrušení zadané položky
void htable_remove(struct htable_t *t, const char *key);
// zrušení všech položek v tabulce
void htable_free(struct htable_t *t);
//zrušení celé tabulky (volá clear)
void htable_clear(struct htable_t *t);
// tisk průměrné a min/max. délky seznamů
void htable_statistics(struct htable_t *t);
//rozptylovaci funkce
unsigned int hash_function(const char *str, unsigned htable_size);

