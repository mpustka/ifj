// htable_remove.c
// Řešení IJC-DU2, příklad b), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//--------------------------------------------------------
#include "htable.h"

// vyhledání a zrušení zadané položky
void htable_remove(struct htable_t *t, const char *key)
{
    struct htable_listitem *listitem = t->ptr[hash_function(key, t->size)];
    
    for(; listitem != NULL; listitem = listitem->next)
    {
        if(strcmp(listitem->next->key, key) == 0)
        {
            struct htable_listitem *itemnext = listitem->next;
            
            if(itemnext->next != NULL)
                listitem->next = itemnext->next;
            
            free(itemnext->key);
            free(itemnext);
            
            break;
        }
    } 
}
