// htable_init.c
// Řešení IJC-DU2, příklad b), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//--------------------------------------------------------
#include "htable.h"

// pro vytvoření a inicializaci tabulky
struct htable_t * htable_init(const unsigned int size) 
{  
    struct htable_t *htable = malloc(sizeof(struct htable_t) + sizeof(struct htable_listitem[size]));
    
    if(htable == NULL)
        return NULL;
    
    //inicializeace
    for(int i = 0; i < size; i++)
    	htable->ptr[i] = NULL;
    	
    htable->size = size;   
    return htable;
}  
