// htable_lookup.c
// Řešení IJC-DU2, příklad b), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//Rozptylovaci funkce
//--------------------------------------------------------
#include "htable.h"

//rozptylovaci funkce
unsigned int hash_function(const char *str, unsigned htable_size) 
{
    unsigned int h=0;
    unsigned char *p;

    for(p=(unsigned char*)str; *p!='\0'; p++)
        h = 31*h + *p;
        
    return h % htable_size;
}
