// tail.c
// Řešení IJC-DU2, příklad a), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//Tail
//--------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>

#define CHARPERLINE 1024 // i se znakem '\n'

struct Buffer
{
    char line[CHARPERLINE+1];
    struct Buffer *next;
};

bool tail(FILE *file, unsigned int countLines, bool lastLines)
{  
    struct Buffer *last = NULL, *list = NULL, *temp = NULL;
    bool next = false, error = false;
    int alocate =  lastLines ? (countLines == 0 ? 3 : countLines) : 3;
       
    //alokace pameti    
    for(int i = 0; i < alocate; i++)
    {
        if((temp = malloc(sizeof(struct Buffer))) == NULL)
        {
            error = true;
            break;
        }
        
        temp->line[0] = '\0';
        
        if(list != NULL)   
            list->next = temp;
          
        if(i == 0)
            last = temp;
        
        list = temp;
    }
    
    if(list != NULL)
    {
        list->next = last;
    }else{
         error = true;
    }
   
    for(int i = 1; (!error && fgets(list->line, sizeof(list->line), file) != NULL); i++)
    {                    
        if(next)
        {
          next = false;
          
          if(strlen(list->line) == CHARPERLINE && list->line[CHARPERLINE-1] != '\n')
              next = true;
              
          continue;
        }
        
        if(strlen(list->line) == CHARPERLINE && list->line[CHARPERLINE-1] != '\n')
        {
          fprintf(stderr, "Varovani: radek v souboru ma vic jak 1024 znaku.\n");
          next = true;
        }
           
        if(!lastLines && i >= countLines)
          fputs(list->line, stdout);  
          
        list = list->next;
    }
   
    for(int i = 0; i < alocate; i++)
    {
        if(!error && list->line[0] != '\0' && lastLines && countLines > 0)
          fputs(list->line, stdout);
        
        temp = list;
        list = list->next;
             
        //uvoneni pameti
        free(temp);
    }
    
    return !error; 
}

long int strToInt(const char* str)
{
    long int number = 0;
    
    while(*str != '\0')
    {
        if(isdigit(*str))
        {
            number = number * 10 + (*str - '0');
        }else if(*str != '+'){
            return -1;
        }   
        str++;
    }   
    return number;
}

int main(int argc, const char *argv[])
{   
    FILE *file = NULL;
    const char *filename = NULL;
    bool lastcount = true;
    
    long int count = 10;
    
    for(int i = 1; (i < 4 && i < argc); i++)
    {
        if(strcmp(argv[i], "-n") == 0)
        {
            if(i+1 > 3 || i+1 >= argc || (count = strToInt(argv[++i])) == -1)
            {
              fprintf(stderr, "Za paremetrem -n ocekavano cislo.\n");
              return 1;
            }   

            lastcount = argv[i][0] == '+' ? false : true;       
        }else if(filename == NULL){
            filename = argv[i];  
        }else{
            fprintf(stderr, "Neznamy parametr.\n");
            return 1;
        }
    }
    
    file = stdin;
    
    if(filename != NULL && (file = fopen(filename, "r")) == NULL)
    {
      fprintf(stderr, "Nepodarilo se otevrit soubor.\n");    
      return 1;    
    }    
             
    if(!tail(file, count, lastcount))
    {
      fprintf(stderr, "Chyba pameti.\n");    
      return 1;    
    } 
     
    if(file != stdin && file != NULL)
        fclose(file);
    
    return 0;  
}
