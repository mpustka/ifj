// htable_forech.c
// Řešení IJC-DU2, příklad b), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//--------------------------------------------------------
#include "htable.h"

// volání funkce pro každý prvek
void htable_foreach(struct htable_t *t, void (*execute)(struct htable_listitem item))
{
    for(unsigned int i = 0; i < t->size; i++)
    {
        struct htable_listitem *listitem = t->ptr[i];
        
        for(; listitem != NULL; listitem = listitem->next)
              execute(*listitem);
    }
}
