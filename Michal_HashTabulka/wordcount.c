// wordcount.c
// Řešení IJC-DU2, příklad b), 10.4.2013
// Autor: Michal Pustka, FIT
// Přeloženo: gcc 4.7.2
//--------------------------------------------------------
//Pocita vyskyt slov v souboru
//--------------------------------------------------------
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <stdlib.h>

#include "htable.h"

int fgetword(char *buffer, int max, FILE *f)
{
    const int size = max-1;
    bool lastSpace = false;
    int i = 0, ch = 0;

    while((ch = fgetc(f)) != EOF)
    {
        if(!isspace(ch))
        {
            if(i >= size)
            {
              fprintf(stderr, "Slovo ma vic jak %d znaku.", size); 
              break;
            }
            
            buffer[i++] = ch;
            lastSpace = true;
        }else{
            if(lastSpace)
                break;
        }
    }
    
    buffer[i] = '\0';
    
    return ch == EOF ? ch : i;  
}

void print_table(struct htable_listitem item)
{
    printf("%s\t%d\n", item.key, item.data);
}

int main(int argc, const char *argv[])
{
    struct htable_t *table = htable_init(COUNT_LIST);
    struct htable_listitem *item = NULL;
    char *word = NULL;
    char buffer[LENGHT_WORD+1];
    bool error = false;
    int val = 0;

    while(1)
    {
    	val = fgetword(buffer, LENGHT_WORD+1, stdin);
        
        if((item = htable_lookup(table, buffer)) != NULL)
        {
			if(item->key == NULL)
			{
		        int length = strlen(buffer);
		        if((word = malloc(sizeof(char) * length+1)) == NULL)
				{
					error = true;
					break;
				}
		    
				//zkopirujeme pamet
				strncpy(word, buffer, length+1);
				item->key = word;
		    } 
		       
            item->data++;     
        }else{
            error = true;
            break;
        }
        
        if(val == EOF) break;
    }

    if(!error)
        htable_foreach(table, print_table);
     
    //uvolneni pameti    
    htable_free(table);

    if(error)
        fprintf(stderr, "Chyba pameti.\n");

    return error;
}
